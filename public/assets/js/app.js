var dt; //var to hold datatable coming from partial views
$(document).ready(function(){
  $('.side-menu a').on('click', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    $('#select_modules').val($(this).attr('id')).trigger('change');
    $('#content').html('<div class="loader"></div>');
    $.get($(this).attr('href'), function(html){
      $('#content').find('.loader').remove();
      $('#content').html(html);
      $('#offCanvasLeft').foundation('close');
    });
  });

  $('#modal_modules').on('change', '#status_heading', function(){
    if ($(this).val() === '2') {
      $.get(baseUrl + 'admin/heading/parent_dropdown.html', {id: $(this).val()}, function(html){
        $('#modal_modules').find('#parent_heading').html(html);
      });
    } else {
      $('#modal_modules').find('#parent_heading').html('');
    }
  });

  $('#content').on('click', '#add_user', function() {
    $.get('/admin/user/update.html', function(html){
      $('#modal_modules').html(html).foundation('open');
    });
  });

  $('#modal_modules').on('click', '#update', function() {
      $('.callout.alert').remove();
      var form = $(this).closest('form');
      form.find('small.error').remove();
      form.find('input').removeClass('validation-error');
      form.find('label').removeClass('error');
      $.post(baseUrl + 'admin/user/save.json', $('#user_form').serialize(), function(json) {
        if (json.status == 'OK') {
          $('#modal_modules').foundation('close');
          dt.ajax.reload();
        } else {
          $.each(json.msg, function(k,v){
            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
            $('label[for="'+k+'"]').addClass('error');
          });
        }
      });
    });

  $('#content').on('click', '.user-edit', function() {
    $.get('/admin/user/update.html', {id: $(this).attr('data-id')}, function(html){
      $('#modal_modules').html(html).foundation('open');
    });
  });

  $('#content').on('click', '.user-view', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    $.get('/admin/user/view.html', {id: $(this).attr('data-id')}, function(html){
      $('#modal_modules').html(html).foundation('open');
    });
  });

  $('#content').on('click', '.user-delete', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    $.get(baseUrl + 'admin/user/delete.html', {id: $(this).attr('data-id')}, function(html){
      $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
    });
  });

  $('#modal_modules').on('click', '#delete_user', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var form = $(this).closest('form');
    $.post(baseUrl + 'admin/user/delete.json', form.serialize(), function(data){
      if (data.status === 'OK') {
        $('#modal_modules').removeClass('small').addClass('large').foundation('close');
        dt.ajax.reload();
      }
    });
  });

  if ($('.select2').length > 0) {
    $('.select2').select2();
  }

    $(document).on('click', '.alert-box .close', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $(this).parent().addClass('hide');
    });

    //generate datatable
    $('#show').on('click', function(){
      if ($('#select_modules').val() !== '' && $('#select_modules').find(':selected').attr('data-href') !== '') {
        $('#content').html('<div class="loader"></div>');
        $.get($('#select_modules').find(':selected').attr('data-href'), function(html){
          $('#content').find('.loader').remove();
          $('#content').html(html);
        });
      }
    });

    //add or edit heading
    $('#content').on('click', '#btn_add_heading, .heading-edit', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/heading/update.html', {id: $(this).attr('data-id'), user_id: $(document).find('#company').val()}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    //preview
    $('#content').on('click', '.heading-view', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/heading/view.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '.heading-delete', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/heading/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

    $('#content').on('click', '#btn_add_supplier, .supplier-edit', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/supplier/update.html', {id: $(this).attr('data-id')}, function(html){
        console.log($('#modal_modules'));
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#save_supplier', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var btn = $(this);
      btn.prop('disabled', true);
      var form = $(this).closest('form');
      form.find('small.error').remove();
      form.find('input').removeClass('validation-error');
      form.find('label').removeClass('error');
      $.post(baseUrl + 'admin/supplier/save.json', form.serialize(), function(data){
        btn.prop('disabled', false);
        if (data.status === 'FAIL') {
          $.each(data.msg, function(k,v){
            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
            $('label[for="'+k+'"]').addClass('error');
          });
        } else {
          $('#modal_modules').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#content').on('click', '.supplier-delete', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/supplier/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#delete_supplier', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      $.post(baseUrl + 'admin/supplier/delete.json', form.serialize(), function(data){
        if (data.status === 'OK') {
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#content').on('click', '.supplier-view', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/supplier/view.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    //product
    $('#content').on('click', '.product-view', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/product/view.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '#btn_add_product', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/product/modify.html', function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '.product-edit', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/product/modify.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#save_product', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      form.find('small.error').remove();
      form.find('input').removeClass('validation-error');
      form.find('label').removeClass('error');
      var btn = $(this);
      btn.prop('disabled', true);
      $.post(baseUrl + 'admin/product/modify.json', form.serialize(), function(data){
        btn.prop('disabled', false);
        if (data.status === 'FAIL') {
          $.each(data.msg, function(k,v){
            $('#modal_modules').find('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
            $('#modal_modules').find('label[for="'+k+'"]').addClass('error');
          });
        } else {
          $('#modal_modules').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#content').on('click', '.product-delete', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/product/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#delete_product', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      $.post(baseUrl + 'admin/product/delete.json', form.serialize(), function(data){
        if (data.status === 'OK') {
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    //company start
    $('#content').on('click', '.company-view', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/company/view.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '#btn_add_company', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/company/update.html', function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '.company-edit', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/company/update.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#save_company', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      form.find('small.error').remove();
      form.find('input').removeClass('validation-error');
      form.find('label').removeClass('error');
      var btn = $(this);
      btn.prop('disabled', true);
      $.post(baseUrl + 'admin/company/update.json', form.serialize(), function(data){
        btn.prop('disabled', false);
        if (data.status === 'FAIL') {
          $.each(data.msg, function(k,v){
            $('#modal_modules').find('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
            $('#modal_modules').find('label[for="'+k+'"]').addClass('error');
          });
        } else {
          $('#modal_modules').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#content').on('click', '.company-delete', function(e){ 
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/company/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#delete_company', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      $.post(baseUrl + 'admin/company/delete.json', form.serialize(), function(data){
        if (data.status === 'OK') {
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
          dt.ajax.reload();
        }
      });
    });
    //company end

    //msds start
    $('#content').on('click', '.msds-view', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/msds/view.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '#btn_add_msds', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/msds/update.html', function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '.msds-edit', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/msds/update.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#save_msds', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      form.find('small.error').remove();
      form.find('input').removeClass('validation-error');
      form.find('label').removeClass('error');
      var btn = $(this);
      btn.prop('disabled', true);
      $.post(baseUrl + 'admin/msds/update.json', form.serialize(), function(data){
        btn.prop('disabled', false);
        if (data.status === 'FAIL') {
          $.each(data.msg, function(k,v){
            $('#modal_modules').find('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
            $('#modal_modules').find('label[for="'+k+'"]').addClass('error');
          });
        } else {
          $('#modal_modules').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#content').on('click', '.msds-delete', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/msds/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#delete_msds', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      $.post(baseUrl + 'admin/msds/delete.json', form.serialize(), function(data){
        if (data.status === 'OK') {
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#content').on('click', '.msds-qr', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/msds/qr.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
        $('#modal_modules').on('click', '[data-close=""]', function(){
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
        });
      });
    });
    //msds end

    //support start
    $('#content').on('click', '.support-view', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/support/view.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });
    $('#content').on('click', '.support-edit', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/support/update.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '#btn_add_support', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/support/update.html', function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '.support-logo', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/support/logo.html', {id : $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
        $('#modal_modules').on('click', '[data-close=""]', function(){
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
        });
      });
    });

    $('#content').on('click', '.support-qr', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/support/qr.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
        $('#modal_modules').on('click', '[data-close=""]', function(){
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
        });
      });
    });

    $('#content').on('click', '.support-delete', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/support/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '#delete_support', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      $.post(baseUrl + 'admin/support/delete.json', form.serialize(), function(data){
        if (data.status === 'OK') {
          $('#modal_modules').removeClass('small').addClass('large').foundation('close');
          dt.ajax.reload();
        }
      });
    });

    $('#modal_modules').on('click', '#save_support', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      var form = $(this).closest('form');
      form.find('small.error').remove();
      form.find('input').removeClass('validation-error');
      form.find('label').removeClass('error');
      var btn = $(this);
      btn.prop('disabled', true);
      $.post(baseUrl + 'admin/support/update.json', form.serialize(), function(data){
        btn.prop('disabled', false);
        if (data.status === 'FAIL') {
          if (data.id) {
            $('#modal_modules').find('#support_file').addClass('validation-error').after('<small class="error">' + data.msg + '</small>');
          } else {
            $.each(data.msg, function(k,v){
              $('#modal_modules').find('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
              $('#modal_modules').find('label[for="'+k+'"]').addClass('error');
            });
          }
        } else {
          $('#modal_modules').foundation('close');
          dt.ajax.reload();
        }
      });
    });
    //support end

    //fittest
    $('#content').on('click', '.fittest-view', function(){
      $.get(baseUrl + 'admin/fittest/preview.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#modal_modules').on('click', '.fittest-update', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/fittest/update.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').foundation('close');
        $('#modal_modules').html(html).foundation('open');
      });
    });

    $('#content').on('click', '.fittest-delete', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      $.get(baseUrl + 'admin/fittest/delete.html', {id: $(this).attr('data-id')}, function(html){
        $('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
      });
    });

  
});