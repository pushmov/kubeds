<?php
namespace Controller;

class Pages extends Auth {
	protected $_step;
	protected $_option;
	protected $_user_id;
	public function before() {
		parent::before();

		/*
		if ($this->auto_render) {
		$this->_user_id = $this->authlite->get_user()->id;
		$this->_step = $this->request->param('step');
		// Check user first
		$step = \Model\Step::find_one_by(array('route' => $this->_step, 'user_id' => $this->_user_id));
		if ($step == null) {
		// Check template user
		$step = \Model\Step::find_one_by(array('route' => $this->_step, 'user_id' => 1));
		}
		if ($step == null) {
		throw new HttpNotFoundException;
		}
		$this->_option = $this->request->param('option');
		// Check user option first
		$option = \Model\Option::find_one_by(array('step_id' => $step->id, 'id' => $this->_option, 'user_id' => $this->_user_id));
		if ($option == null) {
		// Check template user option
		$option = \Model\Option::find_one_by(array('step_id' => $step->id, 'id' => $this->_option, 'user_id' => 1));
		}
		if ($option == null) {
		throw new HttpNotFoundException;
		}
		}
		*/
	}

	public function action_option() {

		$this->_user_id = $this->authlite->get_user()->id;

		$this->_user_template_id = $this->authlite->get_user()->template_id;
		$this->_step = $this->request->param('step');

		// Check user first
		$step = \Model\Step::find_one_by(array('route' => $this->_step, 'user_id' => $this->_user_id));
		if ($step == null) {
			// Check template user
			$step = \Model\Step::find_one_by(array('route' => $this->_step, 'user_id' => $this->_user_template_id));
		}
		if ($step == null) {
			throw new \HttpNotFoundException;
		}
		$this->_option = $this->request->param('option');
		// Check user option first
		$option = \Model\Option::find_one_by(array('step_id' => $step->id, 'id' => $this->_option, 'user_id' => $this->_user_id));
		if ($option == null) {
			// Check template user option
			$option = \Model\Option::find_one_by(array('step_id' => $step->id, 'id' => $this->_option, 'user_id' => $this->_user_template_id));
		}
		if ($option == null) {
			throw new \HttpNotFoundException;
		}
		$buttons = \Model\Option::forge()->get_for_user($this->authlite->get_user(), $step->id);
		$title = $step->title . ' ' . $option->title;


		$contents = \Model\Content::forge()->get_for_options($this->authlite->get_user(), $option->id);
		// Check approved product content table
		$this->template->styles = array('styles2.css');
		if ($option['type_id'] == \Model\Option::T_PRODUCT) {
			$products = \Model\User\Product::forge()->get_for_user($this->authlite->get_user(), $step->id);
			usort($products, array('\Model\User\Product', 'comp'));
			$contents = \Model\User\Product::forge()->format($products);
			$this->template->styles = array_merge($this->_datatable_css, array('styles2.css'));
			$this->template->scripts = $this->_datatable_js;
		}

		//button Account SOP Pages
		if ($option->type_id == \Model\Option::T_SOP) {
			$buttons[$option->id] = $option->to_array();
		}

		$parentid = array();
		foreach ($buttons as $id => $button) {
			$parentid[] = $button['parent_id'];
			if (in_array($id, $parentid)) {
				unset($buttons[$id]);
			}
		}
		
		$data = array('title' => $title,'contents' => $contents, 'buttons' => $buttons, 'route' => $step->route, 'option_type' => $option->type_id, 'option_id' => $option->id);
		
		//msds or support view without page navigation
		if ($option['type_id'] == \Model\Option::T_MSDS) {
			$contents = \Model\Msds::forge()->get_for_user(true);
			$data['sub_content'] = \View::forge('content_msds', array('contents' => $contents));
		}

		if ($option['type_id'] == \Model\Option::T_SUPPORT) {
			$contents = \Model\Support::forge()->get_for_user($option['id']);
			$data['sub_content'] = \View::forge('content_support', array('contents' => $contents));
		}

		$data['admin'] = false;
		$data['content_id'] = 0;
		$admin = \Authlite::instance('auth_admin');
		if ($admin->logged_in()) {
			$data['admin'] = (bool) $admin->logged_in();
			$data['content_id'] = \Input::get('content_id');
		}
		$this->template->content = \View::forge('content', $data);
	}

	public function get_load() {
		if (\Input::get('type') == \Model\Option::T_PRODUCT) {
			$group = \Model\Product\Group::find_by_pk(\Input::get('id'));
			$view = $group->content;
			if ( $group->content_type == \Model\Product\Group::CT_LIST ) {
				$content = \Model\User\Product::forge()->get_for_user($this->authlite->get_user(), \Input::get('id'));
				$view = \View::forge('product', array('content' => $content), false);
			}
			return $this->response($view);
		}
		if (($content = \Model\Content::find_by_pk(\Input::get('id'))) !== null) {
			$view = \View::forge('content_partial', null, false);
			$view->content = $content;
			return $this->response($view);
		}
		return $this->response('<div class="callout alert"> Content No Found </div>');
	}
}