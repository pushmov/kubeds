<?php

namespace Controller;

class Auth extends \Controller_Hybrid
{
    public $template = 'member_base';
    public $authlite;
    public $auto_render;

    protected $_datatable_css = array('datatables.min.css', 'dataTables.foundation.min.css', 'responsive.dataTables.min.css');
    protected $_datatable_js = array('datatables.min.js', 'dataTables.foundation.min.js', 'dataTables.responsive.min.js');

    public function before()
    {
        parent::before();
        // Authlite instance

        $this->authlite = \Authlite::instance('auth_user');
        $this->auto_render = !\Input::is_ajax();
        
        // Login check
        if ((\Request::active()->controller != 'Controller\Login') && !$this->authlite->logged_in()) {
            \Response::redirect('/login');
        }

        if ($this->auto_render) {
            // Initialize empty values
            $this->template->meta_title = '';
            $this->template->meta_desc = '';
            $this->template->title = '';
            $this->template->content = '';
            $this->template->backg = '';
            $this->template->name = '';
            $this->template->logged_in = $this->authlite->logged_in();

            if ($this->authlite->logged_in()) {
                $this->template->logo = \Model\Template::forge()->get_logo($this->authlite->get_user());
            } else {
                $this->template->logo = \Model\Template::forge()->get_logo();
            }

            $this->template->styles = array();
            $this->template->scripts = array();
        }

        if ($this->authlite->logged_in()) {
            \Asset::add_path('assets/kubeds', 'img');
            \Asset::add_path('assets/kubeds/' . str_pad(1, 4, '0', STR_PAD_LEFT), 'img');
            \Asset::add_path('assets/kubeds/' . str_pad($this->authlite->get_user()->id, 4, '0', STR_PAD_LEFT), 'img');
            \Asset::add_path('assets/kubeds/company/');
        }

    }

    public function after($response)
    {
        return parent::after($response);
    }
}