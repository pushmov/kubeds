<?php
namespace Controller\Admin;

class Options extends Auth {
	public function post_enable() {
		$opt = \Model\Option::find_by_pk(\Input::post('id'));
		if ($opt) {
			switch ($opt->parent_id) {
				case 0:
					$opt->status_id = \Model\Option::S_SYSTEM;
					break;
				default:
					$opt->status_id = \Model\Option::S_CUSTOM;
					break;
			}
			if ( in_array(\Input::post('user_id'), \Model\User::user_default_template()) || $opt->parent_id > 0) {
				$opt->status_id = in_array(\Input::post('user_id'), \Model\User::user_default_template()) ? \Model\Option::S_SYSTEM : \Model\Option::S_CUSTOM;
				$opt->save(false);
				$hash = $opt->id;
			} else {
				$custom = \Model\Option::forge()->set($opt->to_array());
				$custom->id = null;
				$custom->parent_id = $opt->id;
				$custom->status_id = \Model\Option::S_CUSTOM;
				$custom->user_id = \Input::post('user_id');
				$custom->save(false);
				$hash = $custom->id;
			}
			
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-option-' . $hash));
	}

	public function post_disable() {
		$opt = \Model\Option::find_by_pk(\Input::post('id'));
		if ($opt) {
			if ($opt->parent_id > 0 || in_array(\Input::post('user_id'), \Model\User::user_default_template())) {
				$opt->status_id = \Model\Option::S_UNUSED;
				$opt->save(false);
				$hash = $opt->id;
			} else {
				$custom = \Model\Option::forge();
				$custom->set($opt->to_array());
				$custom->id = null;
				$custom->parent_id = $opt->id;
				$custom->user_id = \Input::post('user_id');
				$custom->status_id = \Model\Option::S_UNUSED;
				$custom->save(false);
				$hash = $custom->id;
			}
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-option-' . $hash));
	}

	public function get_edit() {
		$view = \View::forge('admin/options/update');
		$data = \Model\Option::find_by_pk(\Input::get('id'));
		$view->data = $data;
		$view->title = 'Add Option';
		$view->status_attr = array('id' => 'status_heading');
		$view->status_id = in_array(\Input::get('user_id'), \Model\User::user_default_template()) ? \Model\Option::S_SYSTEM : \Model\Option::S_CUSTOM;
		if ($data !== null) {
			$view->title = 'Edit Option';
			$view->status_attr['disabled'] = 'disabled';
			$data['created_name'] = \Model\Admin::find_by_pk($data['created_by'])->full_name();
			$data['updated_name'] = \Model\Admin::find_by_pk($data['updated_by'])->full_name();
		}
		return $this->response($view);
	}

	public function post_edit() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($opt = \Model\Option::find_by_pk($post['id'])) === null ) {
			$opt = \Model\Option::forge(\Model\Option::$_defaults);
		}

		$val = $opt->validation();

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));

		} else {

			//clone record with S_CUSTOM when title or route has different value from system template. and the status_id is system template
			if ( $post['title'] != $opt->title && $opt->status_id == \Model\Option::S_SYSTEM && !in_array($post['user_id'], \Model\User::user_default_template())) {
				$custom_opt = \Model\Option::forge();
				//parent data set
				$custom_opt->set($opt->to_array());
				//user input data set
				$custom_opt->set($post);
				$custom_opt->id = null;
				$custom_opt->parent_id = $opt->id;
				$custom_opt->status_id = \Model\Option::S_CUSTOM;
				$custom_opt->save();

				$custom_opt_id = $custom_opt->id;
				if ( !in_array($post['user_id'], \Model\User::user_default_template()) ) {
					$options = \Model\Option::find(array(
						'where' => array(
							'step_id' => $opt->step_id,
							'status_id' => \Model\Option::S_SYSTEM
						)
					));
					if ( $options !== null ) {
						foreach ($options as $row) {
							$check = \DB::select('*')->from(\Model\Option::forge()->get_table_name())->where('parent_id', '=', $row->id)
								->where('status_id', '=', \Model\Option::S_CUSTOM)
								->where('user_id', '=', $post['user_id'])
								->execute();
							if (count($check) <= 0) {
								$custom_opt = \Model\Option::forge()->set($row->to_array());
								$custom_opt->id = null;
								$custom_opt->user_id = $post['user_id'];
								$custom_opt->status_id = \Model\Option::S_CUSTOM;
								$custom_opt->parent_id = $row->id;
								$custom_opt->save(false);
							}
						}
					}
				}

				$hash = $custom_opt->id;
			} else {
				$opt->set($post);
				$opt->updated_by = $this->authlite->get_user()->id;
				$opt->save();
				$hash = $opt->id;
			}
			return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-option-' . $hash));
		}
	}

	public function get_add() {
		$view = \View::forge('admin/options/add');
		$data = \Model\Option::find_by_pk(\Input::get('id'));
		$view->status_id = in_array(\Input::get('user_id'), \Model\User::user_default_template()) ? \Model\Option::S_SYSTEM : \Model\Option::S_CUSTOM;
		$view->data = $data;
		$view->step = \Input::get('step_id');
		return $this->response($view);
	}

	public function post_add() {
		$post = array_map('trim', \Input::post('data'));
		$opt = \Model\Option::forge(\Model\Option::$_defaults);
		$val = $opt->validation();
		if ( $opt->is_new() ) {
			$val->add_callable($opt);
			$opt->created_by = $this->authlite->get_user()->id;
		}
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {
			$step = \Model\Step::find_by_pk($post['step_id']);
			$opt->set($post);
			$opt->pos = \Model\Option::forge()->get_last_pos($post);
			$opt->step_id = ($step->parent_id > 0) ? $step->parent_id : $step->id;
			$opt->created_by = $opt->updated_by = $this->authlite->get_user()->id;
			if ( !in_array($post['user_id'], \Model\User::user_default_template()) && $post['type_id'] < \Model\Option::T_MSDS ) {
				$opt->status_id = \Model\Option::S_CUSTOM;
			}
			$opt->save();
			$hash = $post['step_id'];
			return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-step-' . $hash));
		}
	}

	public function post_revert() {
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if ( ($opt = \Model\Option::find_by_pk(\Input::post('id'))) !== null ) {
			if ($opt->parent_id > 0 && $opt->status_id == \Model\Option::S_CUSTOM) {
				$parent = \Model\Option::find_by_pk($opt->parent_id);
				$hash = $opt->parent_id;
				$opt->title = $parent->title;
				$opt->save(false);
				//$opt->remove($opt->id);
			} elseif ($opt->parent_id == 0 && $opt->status_id == \Model\Option::S_CUSTOM) {
				//custom option user;s defined
				\Model\Option::forge()->revert_other_pos($opt->to_array());
				$hash = $opt->id;
			}
			$response['hash'] = '#href-option-' . \Input::post('id');
		}
		return $this->response($response);
	}

	public function post_up() {
		return $this->response(\Model\Option::forge()->up_order(\Input::post()));
	}

	public function post_down() {
		return $this->response(\Model\Option::forge()->down_order(\Input::post()));
	}

}