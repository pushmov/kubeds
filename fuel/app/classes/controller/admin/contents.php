<?php
namespace Controller\Admin;

class Contents extends Auth {

	public function action_submit() {
		session_start();
		$_SESSION['content'] = \Input::post('data');
		\Response::redirect('admin/contents/preview');
		//$this->mask_auth->logout();
	}

	public function action_preview() {
		$data = \Input::get();
		$user = \Model\User::find_by_pk(\Input::get('user_id'));
		$option = \Model\Option::find_by_pk(\Input::get('option_id'));
		$step = \Model\Step::find_by_pk($option->step_id);
		$mask_auth = \Authlite::instance('auth_user');
		$mask_auth->force_login($user->email);
		$params = array('content_id' => $data['content_id']);
		\Response::redirect(\Uri::create(':route/:id', array('route' => $step->route, 'id' => $option->id)).'?'.http_build_query($params));
	}

	public function post_enable() {
		$content = \Model\Content::find_by_pk(\Input::post('id'));
		if ($content) {
			switch ($content->parent_id) {
				case 0:
					$content->status_id = \Model\Content::S_SYSTEM;
					break;
				default:
					$content->status_id = \Model\Content::S_CUSTOM;
					break;
			}
			$content->save(false);
			$hash = $content->option_id;
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-option-' . $hash));
	}

	public function post_disable() {
		$content = \Model\Content::find_by_pk(\Input::post('id'));
		if ($content) {
			if ( $content->parent_id > 0 || in_array(\Input::post('user_id'), \Model\User::user_default_template()) ) {
				$content->status_id = \Model\Content::S_UNUSED;
				$content->save(false);
				$hash = $content->option_id;
			} else {
				$custom_content = \Model\Content::forge();
				$custom_content->set($content->to_array());
				$custom_content->id = null;
				$custom_content->parent_id = $content->id;
				$custom_content->status_id = \Model\Content::S_UNUSED;
				$custom_content->user_id = \Input::post('user_id');
				$custom_content->save(false);
				$hash = $custom_content->option_id;
			}
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-option-' . $hash));
	}

	public function get_edit() {
		$view = \View::forge('admin/content/edit');

		$data = \Model\Content::find_by_pk(\Input::get('id'));
		$view->status_attr = array('id' => 'status_id');
		$view->status_id = in_array(\Input::get('user_id'), \Model\User::user_default_template()) ? \Model\Option::S_SYSTEM : \Model\Option::S_CUSTOM;
		if ($data === null) {
			$data = \Model\Content::$_defaults;
			$data['heading_id'] = \Input::get('heading_id');
			$data['option_id'] = \Input::get('option_id');
			$content_type_field = \Model\Content::TYPE_HTML;
			$view->title = 'Add Content';
		} else {
			$data = $data->to_array();
			$content_type_field = $data['content_type'];
			$view->title = 'Edit Content';
		}
		$data['opt'] = \Input::get('option_id');
		$view->status_attr['disabled'] = 'disabled';
		$data['content_type_field'] = \View::forge('admin/partials/content_type_html', array('data' => $data));
		$view->data = $data;
		return $this->response($view);
	}

	public function post_edit() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($content = \Model\Content::find_by_pk($post['id'])) === null ) {
			$content = \Model\Content::forge();
		}

		$val = $content->validation();
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {

			if ($content->is_new()) {
				$content = \Model\Content::forge();
				$content->pos = \Model\Content::forge()->position_number($post);
				$content->created_by = $this->authlite->logged_in()->id;
				$content->parent_id = 0;
				$content->title = '';
				$content->content = '';
			}

			$response = array('status' => 'OK', 'msg' => 'Success');
			//is title and content is same as parent ?
			if ( strcmp($content->title, $post['title']) === 0 && strcmp($content->content, $post['content']) === 0) {
				$response['hash'] = '#href-option-' . $content->option_id;
				$response['id'] = $content->id;
				return $this->response($response);
			} else {


				if ( !$content->is_new() && $content->status_id == \Model\Content::S_SYSTEM && !in_array($post['user_id'], \Model\User::user_default_template())) {
					$custom_content = \Model\Content::forge()->set($content->to_array());
					$custom_content->set($post);
					$custom_content->id = null;
					$custom_content->status_id = \Model\Content::S_CUSTOM;
					$custom_content->user_id = $post['user_id'];
					$custom_content->option_id = $content->option_id;
					$custom_content->parent_id = $content->id;
					$custom_content->save(false);
					$id = $custom_content->id;
				} else {
					$option = \Model\Option::find_by_pk($post['option_id']);
					$content->set($post);
					$content->updated_by = $this->authlite->get_user()->id;
					$content->status_id = in_array($post['user_id'], \Model\User::user_default_template()) ? \Model\Content::S_SYSTEM : \Model\Content::S_CUSTOM;
					$content->option_id = ($option->parent_id == 0) ? $option->id : $option->parent_id;
					$content->save();
					$id = $content->id;
				}
				$response['hash'] = '#href-option-' . $post['opt'];
				$response['id'] = $id;
			}
			return $this->response($response);
		}
	}

	public function post_revert() {
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if ( ($ct = \Model\Content::find_by_pk(\Input::post('id'))) !== null ) {
			$hash = '';
			if ($ct->parent_id > 0 && $ct->status_id == \Model\Content::S_CUSTOM) {
				$parent = \Model\Content::find_by_pk($ct->parent_id);
				$hash = $ct->option_id;
				$ct->title = $parent->title;
				$ct->content = $parent->content;
				$ct->save(false);
				//$opt->remove($opt->id);
			}
			$response['hash'] = '#href-option-' . \Input::post('option_id');
		}
		return $this->response($response);
	}

	public function post_up() {
		return $this->response(\Model\Content::forge()->up_order(\Input::post()));
	}

	public function post_down() {
		return $this->response(\Model\Content::forge()->down_order(\Input::post()));
	}

}