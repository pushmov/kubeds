<?php
namespace Controller\Admin;

class Product extends Auth {

	public function get_index() {
		return $this->response(\View::forge('admin/product/index'));
	}

	public function get_all() {
		return $this->response(\Model\Product::forge()->datatable(\Input::get()));
	}

	public function get_modify() {
		$view = \View::forge('admin/product/modify');
		$data = \Model\Product::forge()->view(\Input::get('id'));
		if ($data === null) {
			$view->title = 'Add New Product Data';
		} else {
			$view->title = 'Edit Product Data';
		}
		$data['supplier_name'] = isset($data['supplier_name']) ? $data['supplier_name'] : '';
		$view->data = $data;
		return $this->response($view);
	}

	public function post_modify() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($product = \Model\Product::find_by_pk($post['id'])) === null ) {
			$product = \Model\Product::forge();
		}
		
		$val = $product->validation();
		$req = $val->add('part_num', 'Part Number')->add_rule('required');
		if ( $product->is_new() || $post['part_num'] != $product->part_num ) {
			$val->add_callable($product);
			$req->add_rule('unique');
			$product->created_by = $this->authlite->get_user()->id;
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
			
		} else {
			$product->set($post);
			$product->updated_by = $this->authlite->get_user()->id;
			$product->save();
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_view() {
		$view = \View::forge('admin/product/view');
		$view->data = \Model\Product::forge()->view(\Input::get('id'));
		return $this->response($view);
	}

	public function get_delete() {
		$view = \View::forge('admin/product/delete');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Product::forge()->remove(\Input::post('data.id')));
	}

	public function get_autocomplete() {
		$data = array();
		$products = \Model\Product::find( function($query) {
			return $query->select(\Model\Product::get_table_name() . '.id', \Model\Product::get_table_name() . '.part_num', 'sup_id', array('s.name', 'sup_name'))
			->join(array(\Model\Supplier::get_table_name(), 's'))
			->on('s.id', '=', \Model\Product::get_table_name() . '.sup_id');
		});
		if ($products !== null) {
			foreach ($products as $row) {
				$data[] = array('name' => $row->part_num, 'id' => $row->id, 'sup_id' => $row->sup_id, 'sup_name' => $row->sup_name);
			}
		}
		return $this->response($data);
	}
}