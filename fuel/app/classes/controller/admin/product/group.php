<?php
namespace Controller\Admin\Product;

class Group extends \Controller\Admin\Auth {

	public function get_index() {
		return $this->response(\View::forge('admin/product_group/index'));
	}

	public function get_orderable() {
		$view = \View::forge('admin/product_group/orderable');
		$view->data = \Model\Product\Group::forge()->get_active(\Input::get());
		$view->unused = \Model\Product\Group::forge()->get_unused(\Input::get());
		return $this->response($view);
	}

	public function get_all() {
		return $this->response(\Model\Product\Group::forge()->datatable(\Input::get()));
	}

	public function get_modify() {
		$view = \View::forge('admin/product_group/modify');
		$data = \Model\Product\Group::find_by_pk(\Input::get('id'));
		$view->data = $data;
		$view->title = 'Add New Product Group';
		$view->heading = \Model\Heading::find_by_pk(\Input::get('heading_id'));
		if ($data !== null) {
			$view->title = 'Edit Product Group';
		}
		return $this->response($view);
	}

	public function post_modify() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($group = \Model\Product\Group::find_by_pk($post['id'])) === null ) {
			$group = \Model\Product\Group::forge();
		}
		
		$val = $group->validation();
		$req = $val->add('title', 'Title')->add_rule('required');
		if ( $group->is_new() || $post['title'] != $group->title ) {
			$val->add_callable($group);
			$req->add_rule('unique');
			$group->created_by = $this->authlite->get_user()->id;
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
			
		} else {

			if ( $group->is_new() ) {
				$group->pos = \Model\Product\Group::forge()->position_number($post['heading_id']);
			}
			if ( !$group->is_new() && $group->title != $post['title'] ) {
				$custom_group = \Model\Product\Group::forge()->set($group->to_array());
				$custom_group->set($post);
				$custom_group->id = null;
				$custom_group->status_id = \Model\Product\Group::S_CUSTOM;
				$custom_group->parent_id = $group->id;
				$custom_group->save();
			} else {
				$group->set($post);
				$group->updated_by = $this->authlite->get_user()->id;
				$group->save();
			}
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_delete() {
		$view = \View::forge('admin/product_group/delete');
		$view->data = \Input::get();
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Product\Group::forge()->remove(\Input::post('data.id')));
	}

	public function post_order() {
		return $this->response(\Model\Product\Group::forge()->reorder(\Input::post('data')));
	}

	public function post_user_heading() {
		$view = \View::forge('admin/product_group/user_heading_dropdown');
		$view->user_id = \Input::post('user_id');
		return $this->response($view);
	}

	public function post_activate() {
		return $this->response(\Model\Product\Group::forge()->activate(\Input::post('id')));
	}

	public function post_deactivate() {
		return $this->response(\Model\Product\Group::forge()->deactivate(\Input::post()));
	}

	public function get_revert() {
		$view = \View::forge('admin/product_group/revert');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_revert() {
		return $this->response(\Model\Product\Group::forge()->revert(\Input::post('data.id')));
	}

}