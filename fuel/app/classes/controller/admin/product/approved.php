<?php
namespace Controller\Admin\Product;

class Approved extends \Controller\Admin\Auth {

	public function get_index() {
		return $this->response(\View::forge('admin/product_approved/index'));
	}

	public function get_all() {
		return $this->response(\Model\Product\Approved::forge()->datatable(\Input::get()));
	}

	public function get_list() {
		$view = \View::forge('admin/product_approved/list');
		$view->heading = \Model\Heading::find_by_pk(\Input::get('heading_id'));
		$view->data = \Model\Product\Approved::forge()->count_product_groups(\Input::get('heading_id'), \Input::get('user_id'));
		return $this->response($view);
	}

	public function get_manage_skus() {
		$view = \View::forge('admin/product_approved/manage_skus');
		$view->group = \Model\Product\Approved::forge()->group_data(\Input::get('group_id'));
		$view->data = \Model\Product\Approved::forge()->datatable_2(\Input::get());
		return $this->response($view);
	}

	public function get_confirm_skus() {
		$view = \View::forge('admin/product_approved/confirm_skus');
		$view->group = \Model\Product\Group::find_by_pk(\Input::get('group_id'));
		$view->product = \Model\Product::find_by_pk(\Input::get('product_id'));
		return $this->response($view);
	}

	public function post_manage_skus() {
		$post = array_map('trim', \Input::post('data'));
		$product_approved = \Model\Product\Approved::forge();
		if ($product_approved->is_new()) {
			$product_approved->created_by = $this->authlite->get_user()->id;
		}
		$product_approved->set($post);
		$product_approved->updated_by = $this->authlite->get_user()->id;
		$product_approved->status_id = \Model\Product\Approved::S_CUSTOM;
		$product_approved->save();
		return $this->response(array('status' => 'OK', 'group_id' => $product_approved->group_id));
	}

	public function get_search_product() {
		return $this->response(\Model\Product::forge()->all_json_data());
	}

	public function get_delete() {
		$view = \View::forge('admin/product_approved/delete');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Product\Approved::forge()->remove(\Input::post('data')));
	}

	public function get_request() {
		$view = \View::forge('admin/product_approved/request');
		$group = \Model\Product\Group::find_by_pk(\Input::get('group_id'));
		$view->group = $group;
		$view->heading = \Model\Heading::find_by_pk($group->heading_id);
		return $this->response($view);
	}

	public function post_request() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($group = \Model\Product\Group::find_by_pk($post['id'])) === null ) {
			return $this->response(array('status' => 'OK'));
		}

		$val = $group->validation();
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {
			$group->content = $post['content'];
			$group->updated_by = $this->authlite->get_user()->id;
			$group->save(false);
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_preview() {
		$data['group'] = \Model\Product\Group::find_by_pk(\Input::get('group_id'));
		return $this->response(\View::forge('admin/product_approved/preview', $data, false));
	}

	public function post_user_heading() {
		$view = \View::forge('admin/product_approved/user_heading_dropdown');
		$view->user_id = \Input::post('user_id');
		return $this->response($view);
	}

	public function post_toggle() {
		return $this->response(\Model\Product\Approved::forge()->update_state(\Input::post()));
	}

	public function get_add() {
		$view = \View::forge('admin/products/add');
		$data = \Model\Product\Approved::find_by_pk(\Input::get('id'));
		if ($data === null) {
			$data = \Model\Product\Approved::$_defaults;
			$data['group_id'] = \Input::get('group');
		} else {
			$data = $data->to_array();
		}
		$view->data = $data;
		return $this->response($view);
	}

	public function post_add() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($approved = \Model\Product\Approved::find_by_pk($post['id'])) === null ) {
			$approved = \Model\Product\Approved::forge();
			$approved->id = null;
		}
		$val = $approved->validation();
		$req = $val->add('product_name', 'Product Number')->add_rule('required');
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		}
		$approved = \Model\Product\Approved::forge();
		$approved->set($post);
		$approved->id = null;
		$approved->parent_id = 0;
		$approved->updated_by = $approved->created_by = $this->authlite->get_user()->id;
		$approved->status_id = \Model\Product\Approved::S_SYSTEM;
		if ( !in_array($post['user_id'], \Model\User::user_default_template()) ) {
			$approved->status_id = \Model\Product\Approved::S_CUSTOM;
		}
		$approved->save(false);
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-group-'.$approved->group_id));
	}

	public function get_edit() {
		$view = \View::forge('admin/products/update_approved');
		$data = \Model\Product\Group::find_by_pk(\Input::get('id'));
		if ($data === null) {
			$data = \Model\Product\Group::$_defaults;
		} else {
			$data = $data->to_array();
		}
		$view->data = $data;
		return $this->response($view);
	}

	public function post_edit() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($group = \Model\Product\Group::find_by_pk($post['id'])) === null ) {
			$group = \Model\Product\Group::forge();
		}

		$val = $group->validation();
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		}

		if ( in_array($post['user_id'], \Model\User::user_default_template()) || $group->status_id == \Model\Product\Group::S_CUSTOM ) {
			$group->set($post);
			$group->save();
			$hash = '#href-group-' . $group->id;
		} else {
			$custom = \Model\Product\Group::forge()->set($group->to_array());
			$custom->set($post);
			$custom->id = null;
			$custom->parent_id = $group->id;
			$custom->content = null;
			$custom->status_id = \Model\Product\Group::S_CUSTOM;
			$group->updated_by = $this->authlite->get_user()->id;
			$custom->save();
			$hash = '#href-group-' . $custom->id;
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => $hash));
	}

}