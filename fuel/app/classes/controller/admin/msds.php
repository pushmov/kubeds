<?php
namespace Controller\Admin;

class Msds extends Auth {

	public function before() {
		parent::before();
		\Asset::add_path('assets/kube', 'img');
		\Asset::add_path('assets/kube/0001', 'img');
	}

	public function get_index() {
		return $this->response(\View::forge('admin/msds/index'));
	}

	public function get_all() {
		return $this->response(\Model\Msds::forge()->datatable(\Input::get()));
	}

	public function get_view() {
		$view = \View::forge('admin/msds/view');
		$view->data = \Model\Msds::forge()->view(\Input::get('id'));
		return $this->response($view);
	}

	public function get_update() {
		$view = \View::forge('admin/msds/modify');
		$data = \Model\Msds::find_by_pk(\Input::get('id'));
		if ($data !== null) {
			$data['supplier_name'] = \Model\Supplier::find_by_pk($data->sup_id)->name;
			$title = 'Edit Msds';
		} else {
			$data = \Model\Msds::$_defaults;
			$data['supplier_name'] = '';
			$title = 'Add New Msds';
		}
		$data['option'] = \Input::get('option');
		$view->data = $data;
		$view->title = $title;
		return $this->response($view);
	}

	public function post_update() {
		$post = array_map('trim', \Input::post('data'));
		$post['description'] = urldecode($post['description']);
		if ( ($msds = \Model\Msds::find_by_pk($post['id'])) === null ) {
			$msds = \Model\Msds::forge();
		}
		$val = $msds->validation();
		$req = $val->add('description', 'Display title')->add_rule('required');
		if ( $msds->is_new() || $msds['description'] != $msds->description ) {
			$val->add_callable($msds);
			$req->add_rule('unique');
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));	
		} else {
			$msds->set($post);
			$msds->save();
			return $this->response(array('status' => 'OK', 'hash' => '#href-option-' . $post['option']));
		}
	}

	public function get_delete() {
		$view = \View::forge('admin/msds/delete');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Msds::forge()->remove(\Input::post('data.id')));
	}

	public function post_toggle() {
		if ( ($msds = \Model\Msds::find_by_pk(\Input::post('id'))) !== null ) {
			$msds->status_id = \Input::post('checked');
			$msds->save(false);
			$option = \Model\Option::find_by_pk(\Input::post('option'));
			return $this->response(array('hash' => '#href-option-'.$option->id));
		}
	}

}