<?php
namespace Controller\Admin;

class Dashboard extends Auth {
	public function action_index() {
		$page = \View::forge('admin/dashboard', null, false);
		$page->status = \Model\User::forge()->get_status_array();
		$page->users = (array)\Model\User::find(function($query){
			$query->select('users.*', 't.name');
			$query->join(array('users', 'u'), 'LEFT')->on('users.template_id', '=', 'u.id');
			$query->join(array('templates', 't'), 'INNER')->on('t.id', '=', 'users.template_id');
			$query->where('users.id','<>',1);
			if ($this->authlite->get_user()->role_id > 0) {
				$query->where('t.role_id', $this->authlite->get_user()->role_id);
			}
			return $query;
		});
		$this->template->styles = $this->_datatable_css;
		$this->template->scripts = $this->_datatable_js;
		$this->template->styles = array_merge($this->_datatable_css, array('easy-autocomplete.min.css', 'style.css'));
		$this->template->scripts = array_merge($this->_datatable_js, array('jquery.easy-autocomplete.min.js'));
		$this->template->content = $page;
	}

	public function action_modules() {
		$this->template->styles = array_merge($this->_datatable_css, array('select2.min.css', 'easy-autocomplete.min.css', 'uploadfile.css', 'style.css'));
		$this->template->scripts = array_merge($this->_datatable_js, array('select2.full.min.js', 'jquery-ui.min.js', 'jquery.easy-autocomplete.min.js', 'jquery.uploadfile.min.js', 'app.js'));
	}
}