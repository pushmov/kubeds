<?php
namespace Controller\Admin;

use \Model\Admin;

class Admins extends Auth
{
	public $is_active = 'admins';

	public function before()
	{
		parent::before();
		if (!in_array($this->authlite->get_user()->role_id, array(0, 10, 20))) {
			\Response::redirect('/admin');
		}
	}

	public function action_index()
	{
		$page = \View::forge('admin/admins/index', null, false);
		$page->admins = Admin::forge()->get_datatable_list($this->authlite->get_user());
		$this->template->styles = $this->_datatable_css;
		$this->template->scripts = $this->_datatable_js;
		$this->template->styles = array_merge($this->_datatable_css, array('easy-autocomplete.min.css', 'style.css'));
		$this->template->scripts = array_merge($this->_datatable_js, array('jquery.easy-autocomplete.min.js'));
		$this->template->content = $page;
	}

	public function get_edit()
	{
		$html = \View::forge('admin/admins/edit', null, false);
		$html->data = Admin::find_by_pk(\Input::get('id'));
		return $this->response($html);
	}

	public function post_edit()
	{
		$post = array_map('trim', \Input::post('data'));
		if (($admin = Admin::find_by_pk($post['id'])) === null) {
			$admin = Admin::forge();
		}
		$admin->set($post);
		$data = $admin->to_array();
		$val = $admin->validation();

		$req = $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
		if ($admin->is_new()) {
			$val->add_callable($admin);
			$req->add_rule('unique_email');
			$admin->passwrd = \Authlite::instance('auth_admin')->hash($admin->passwrd);
		}
		if (!$val->run($data)) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {
			$admin->save();
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_add()
	{
		$html = \View::forge('admin/admins/add', null, false);
		$html->data = Admin::forge();
		return $this->response($html);
	}

	public function get_reset()
	{
		$html = \View::forge('admin/admins/reset', null, false);
		$html->data = Admin::find_by_pk(\Input::get('id'));
		return $this->response($html);
	}

	public function post_reset()
	{
		$post = array_map('trim', \Input::post('data'));
		if (($admin = Admin::find_by_pk($post['id'])) === null) {
			return $this->response(array('status' => 'FAIL'));
		}
		if ($post['password'] == '') {
			return $this->response(array('status' => 'FAIL'));
		}
		$admin->passwrd = \Authlite::instance('auth_admin')->hash($post['password']);
		$admin->save(false);
		return $this->response(array('status' => 'OK'));
	}

}