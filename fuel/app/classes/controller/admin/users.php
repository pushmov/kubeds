<?php

namespace Controller\Admin;

use \Model\User;

class Users extends Auth
{

    public function action_index()
    {
        \Response::redirect('/admin');
    }

    public function action_load($id)
    {
        \Asset::add_path('assets/kubeds', 'img');
        $user = User::find_by_pk($id);
        if ($user) {
            $page = \View::forge('admin/user', null, false);
            $page->profile = \View::forge('admin/users/profile');
            $userarray = $user->to_array();
            $template = \Model\Template::find_by_pk($user->template_id);
            if ($template->status_id == \Model\Template::S_SYSTEM) {
                $userarray['account_label'] = 'Template ID';
            } else {
                $userarray['account_label'] = 'Account #';
            }
            $page->profile->data = $userarray;
            $page->profile->template = \Model\Template::find_by_pk($user->template_id)->to_array();
            $page->profile->logos = \Model\User\Logo::forge()->get_for_user($user->id);
            $page->pages = \View::forge('admin/users/pages');
            $page->pages->print_href = \Uri::create('admin/users/print/:id', array('id' => $user->id));
            $page->pages->user = $user->to_array();
            $page->pages->steps = \Model\Step::forge()->get_for_user($user);
            $page->pages->template_id = null;
            $options = \Model\Option::forge()->get_for_user($user);
            usort($options, array('\Model\Option', 'comp'));
            $page->pages->options = $options;
            $content = \Model\Content::forge()->get_for_user($user);
            usort($content, array('\Model\Content', 'comp'));
            $page->pages->content = $content;
            $approved = \Model\User\Product::forge()->get_for_user($user);
            usort($approved, array('\Model\User\Product', 'comp'));
            $page->pages->approved = \Model\User\Product::forge()->format($approved);
            $page->pages->msdss = \Model\Msds::forge()->get_for_user();
            $page->pages->all_supports = \Model\Support\All::find(array('order_by' => array('name' => 'asc')));
            $this->template->styles = array('easy-autocomplete.min.css', 'uploadfile.css', 'style.css');
            $this->template->scripts = array('jquery.easy-autocomplete.min.js', 'jquery.uploadfile.min.js', 'tinymce.min.js', 'jquery.tinymce.min.js');
        } else {
            $page = \View::forge('admin/users/notfound', null, false);
        }
        $this->template->content = $page;
    }

    public function action_print($id)
    {
        if (($user = \Model\User::find_by_pk($id)) === null) {
            throw new \HttpNotFoundException;
        }
        $mpdf = new \Mpdf\Mpdf(array(
            //'orientation' => 'L',
            'tempDir' => APPPATH . 'tmp'
        ));
        $page['data'] = \Pages::home($user);
        $page['logo'] = \Model\Template::forge()->get_logo($user);
        $data['yield'] = \View::forge('home', $page)->render();

        //example : generate the 2 repair pages for a test
        $step = \Model\Step::find_one_by(array('id' => 3, 'template_id' => $user->template_id));
        $option = \Model\Option::find_one_by(array('step_id' => $step->id));
        $contents = \Model\Content::find(array(
            'where' => array('option_id' => $option->id),
            'order_by' => array(
                'pos' => 'asc'
            )
        ));
        if ( empty($contents) ) {
            throw new \HttpNotFoundException;
        }
        $mpdf->WriteHTML(\Asset::css('app.css', array(), null, true), 1);
        $mpdf->WriteHTML(\Asset::css('style.css', array(), null, true), 1);
        $mpdf->WriteHTML(\Asset::css('foundation-icons.css', array(), null, true), 1);
        $mpdf->WriteHTML(\Asset::css('home.css', array(), null, true), 1);
        $mpdf->WriteHTML('h3,h4{font-weight:bold;margin-top:10px;margin-bottom:15px;}', 1);
        foreach ($contents as $content) {
            $data['yield'] = $content->content;
            $mpdf->AddPage();
            $mpdf->WriteHTML(\View::forge('print', $data)->render(), 2);
        }
        $mpdf->output();
    }

    public function action_view_book()
    {
        $is_user_found = true;
        $user = User::find_by_pk(\Input::get('userid'));
        if (($user = User::find_by_pk(\Input::get('userid'))) === null) {
            $user = User::find_by_pk(User::U_DEFAULT);
            $is_user_found = false;
        }
        $mask_auth = \Authlite::instance('auth_user');
        if ($mask_auth->force_login($user->email)) {
            $mask_auth->get_user()->template_id = \Input::get('template_id') != '' ? \Input::get('template_id') : $mask_auth->get_user()->template_id;
            \Response::redirect(\Uri::create(':route', array('route' => 'home')));
        }
    }

    /*
      public function () {
      return $this->response(User::forge()->all_users(\Input::get()));
      }
     */

    public function get_view()
    {
        $data['data'] = User::find_by_pk(\Input::get('id'));
        return $this->response(\View::forge('admin/user/view', $data));
    }

    public function get_update()
    {
        $data['data'] = User::find_by_pk(\Input::get('id'));
        if ($data['data'] == null) {
            $data['data'] = User::$_defaults;
        }
        $data['cboTemplates'] = \Model\Template::forge()->dropdown();
        return $this->response(\View::forge('admin/users/add', $data, false));
    }

    public function post_save()
    {
        $post = array_map('trim', \Input::post('data'));
        if (($user = User::find_by_pk($post['id'])) === null) {
            $user = User::forge();
            $passwrd = trim(\Input::post('passwrd'));
            $post['passwrd'] = $this->authlite->hash($passwrd);
        } else {
            $post['passwrd'] = $user->passwrd;
        }
        
        // If password not empty then hash before save
        $val = $user->validation();
        $val->add_callable($user);
        $req_account = $val->add('account', 'Account ID')->add_rule('required');
        $req_email = $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
        if ($user->is_new() || $post['email'] != $user->email) {
            $req_email->add_rule('unique_email');
        }

        if (!$val->run($post)) {
            return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
        } else {
            $user->set($post);
            if ($user->is_new()) {
                $template = \Model\Template::find_one_by('id', $post['template_id']);
                $user->template_id = $template->id;
            }
            $user->updated_by = $this->authlite->get_user()->id;
            if ($user->is_new()) {
                $user->created_by = $this->authlite->get_user()->id;
            }
            $user->save();
            // If template then clone
            /*
            if ($template->status_id == \Model\Template::S_ACTIVE) {
                User::forge()->clone_all($user);
                // User's template_id will now be self
                $user->template_id = $user->id;
                $user->save();
            }
             */
            return $this->response(array('status' => 'OK', 'msg' => 'Success'));
        }
    }

    public function post_reset()
    {
        return $this->response(User::forge()->forgot_password(\Input::post('data')));
    }

    public function get_delete()
    {
        $view = \View::forge('admin/users/delete');
        $view->data = \Input::get();
        return $this->response($view);
    }

    public function post_delete()
    {
        return $this->response(User::forge()->remove(\Input::post('data.id')));
    }

    public function get_template()
    {
        $template = \Model\Template::find_one_by_name(\Input::get('name'));
        if ($template->status_id == \Model\Template::S_SYSTEM) {
            return $this->response('Clone Template');
        }
        return $this->response('Use Template');
    }

    public function get_password()
    {
        $view = \View::forge('admin/users/password');
        $view->data = \Input::get('data');
        return $this->response($view);
    }

    public function post_password()
    {
        $passwrd = trim(\Input::post('passwrd'));
        if (strlen($passwrd) < 5) {
            return array('status' => 'FAIL', 'msg' => 'Password needs minimum 5 characters');
        }
        if (($user = User::find_by_pk(\Input::post('id')))) {
            $user->passwrd = $this->authlite->hash($passwrd);
            $user->save(false);
            return array('status' => 'OK');
        }
        return array('status' => 'FAIL', 'msg' => 'Reset failed try again');
    }

    public function get_logo()
    {
        $view = \View::forge('admin/users/logo');
        $view->data = \Model\User\Logo::find_by_pk(\Input::get('id'));
        return $this->response($view);
    }

    public function post_logo()
    {
        $post = \Input::post();
        $response = array('status' => 'OK', 'hash' => $post['hash']);
        $logo = \Model\User\Logo::forge();
        $logo->set($post);
        $data['pos'] = \Model\User\Logo::forge()->get_last_position($post['user_id']);
        $logo->set($data);
        $logo->save(false);
        return $response;
    }

    public function post_delete_logo()
    {
        return \Model\User\Logo::forge()->delete_thumbnails(\Input::post('thumbnails'));
    }

}
