<?php
namespace Controller\Admin;

class Auth extends \Controller_Hybrid {
	public $template = 'admin/admin_base';
	public $authlite;
	public $auto_render;
	public $is_active = 'users';

	//protected $_datatable_css = array('datatables.min.css','dataTables.foundation.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
	protected $_datatable_css = array('datatables.min.css','dataTables.foundation.min.css', 'responsive.dataTables.min.css');
	//protected $_datatable_js = array('datatables.min.js','dataTables.foundation.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
	protected $_datatable_js = array('datatables.min.js','dataTables.foundation.min.js', 'dataTables.responsive.min.js');

	public function before() {
		parent::before();
		\Asset::add_path('assets/foundation-icons/', 'css');
		$this->authlite = \Authlite::instance('auth_admin');
		$this->auto_render = !\Input::is_ajax();

		// Login check
		if ((\Request::active()->controller != 'Controller\Admin\Login') && !$this->authlite->logged_in()) {
			\Response::redirect('/admin/login');
		}

		if ($this->auto_render) {
			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$this->template->content = '';
			$this->template->logged_in = $this->authlite->logged_in();

			$this->template->styles = array();
			$this->template->scripts = array();

			$menu_items = array('users' => 'SOPs', 'templates' => 'Templates', 'documents' => 'Documents');
			if ($this->authlite->logged_in() && in_array($this->authlite->get_user()->role_id, array(0, 10, 20))) {
				$menu_items['admins'] = 'Admins';
			}
			$menu = '';
			foreach ($menu_items as $k => $v) {
				$menu .= '<li';
				if ($k == $this->is_active) {
					$menu .= ' class="is-active"';
				}
				$menu .= '>' . \Html::anchor('admin/' . $k, $v, array('id' => $k)) . '</li>';
			}
			$this->template->set_safe('menu', $menu);
		}
	}
}