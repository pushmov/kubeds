<?php
namespace Controller\Admin;

class Procedure extends Auth {

	public function get_index() {
		return $this->response(\View::forge('admin/procedure/index'));
	}

	public function get_orderable() {
		$view = \View::forge('admin/procedure/orderable');
		$view->sub_page = \Model\Heading::find_by_pk(\Input::get('heading_id'));
		$view->data = \Model\Procedure::forge()->get_heading(\Input::get());
		$view->unused = \Model\Procedure::forge()->get_unused(\Input::get());
		return $this->response($view);
	}

	public function post_order() {
		return $this->response(\Model\Procedure::forge()->reorder(\Input::post('data')));
	}

	public function get_modify() {
		$view = \View::forge('admin/procedure/modify');
		$data = \Model\Procedure::find_by_pk(\Input::get('id'));
		$view->status_attr = array('id' => 'status_id');
		if ($data === null) {
			$data = \Model\Procedure::$_defaults;
			$data['heading_id'] = \Input::get('heading_id');
			$content_type_field = \Model\Procedure::TYPE_HTML;
		} else {
			$data = $data->to_array();
			$content_type_field = $data['content_type'];
		}
		$data['status_id'] = \Model\Procedure::S_CUSTOM;
		$view->status_attr['disabled'] = 'disabled';
		$data['content_type_field'] = \View::forge('admin/partials/content_type_html', array('data' => $data));
		$view->data = $data;
		return $this->response($view);
	}

	public function post_modify() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($procedure = \Model\Procedure::find_by_pk($post['id'])) === null ) {
			$procedure = \Model\Procedure::forge();
		}

		$val = $procedure->validation();
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {

			if ($procedure->is_new()) {
				$procedure->pos = \Model\Procedure::forge()->position_number($post['heading_id']);
				$procedure->created_by = $this->authlite->get_user()->id;
				$procedure->parent_id = 0;
			}

			if ($procedure->title != $post['title']) {
				$custom_procedure = \Model\Procedure::forge()->set($procedure->to_array());
				$custom_procedure->set($post);
				$custom_procedure->id = null;
				$custom_procedure->status_id = \Model\Procedure::S_CUSTOM;
				$custom_procedure->user_id = $post['user_id'];
				$custom_procedure->parent_id = $procedure->id;
				$custom_procedure->save(false);
			} else {
				$procedure->set($post);
				$procedure->updated_by = $this->authlite->get_user()->id;
				$procedure->save();
			}
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_parent_dropdown() {
		$view = \View::forge('admin/partials/procedure_parent_dropdown');
		$view->data = array('parent_id' => null);
		return $this->response($view);
	}

	public function post_deactivate() {
		return $this->response(\Model\Procedure::forge()->deactivate(\Input::post()));
	}

	public function post_activate() {
		return $this->response(\Model\Procedure::forge()->activate(\Input::post('id')));
	}

	public function post_user_heading() {
		$view = \View::forge('admin/procedure/user_heading_dropdown');
		$view->user_id = \Input::post('user_id');
		return $this->response($view);
	}

	public function get_revert() {
		$view = \View::forge('admin/procedure/revert');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_revert() {
		return $this->response(\Model\Procedure::forge()->revert(\Input::post('data.id')));
	}

}