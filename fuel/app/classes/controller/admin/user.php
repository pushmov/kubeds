<?php
namespace Controller\Admin;

class User extends \Controller_Rest {
	public function action_index() {
	}

	public function get_index() {
		return $this->response(\View::forge('admin/user/index'));
	}

	public function get_all() {
		return $this->response(\Model\User::forge()->all_users(\Input::get()));
	}

	public function get_view() {
		$data['data'] = \Model\User::find_by_pk(\Input::get('id'));
		return $this->response(\View::forge('admin/user/view', $data));
	}

	public function get_update() {
		$data['data'] = \Model\User::find_by_pk(\Input::get('id'));
		$data['cboStatus'] = \Model\User::forge()->get_status_array();
		return $this->response(\View::forge('admin/dialog/user', $data, false));
	}

	public function post_save() {
		$post = array_map('trim', \Input::post('data'));
		if (($user = \Model\User::find_by_pk($post['id'])) === null) {
			$user = \Model\User::forge();
		}
		$passwrd = trim(\Input::post('passwrd'));
		// If password not empty then hash before save
		if ($passwrd != '') {
			$post['passwrd'] = $this->authlite->hash($passwrd);
		}

		$val = $user->validation();
		$val->add_callable($user);
		$req_account = $val->add('account', 'Account ID')->add_rule('required');
		$req_email = $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
		if ( $user->is_new() || $post['email'] != $user->email ) {
			$req_email->add_rule('unique_email');
		}

		if ( $user->is_new() || $post['account'] != $user->account ) {
			$req_account->add_rule('unique_id');
		}

		$template = \Model\Template::find_one_by('name', $post['name']);
		if ($template === null) {
			$template = \Model\Template::forge();
			$template->name = $post['name'];
			$template->status_id = \Model\Template::S_ACTIVE;
			$template->updated_by = $template->created_by = \Authlite::instance('auth_user')->get_user()->id;
			$template->save(false);
		}
		
		$post['template_id'] = $template->id;

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {
			$user->set($post);
			$user->template_id = $post['template_id'];
			$user->save();
			return $this->response(array('status' => 'OK'));
		}
	}

	public function post_reset() {
		return $this->response(\Model\User::forge()->forgot_password(\Input::post('data')));
	}

	public function get_delete() {
		$view = \View::forge('admin/user/delete');
		$view->data = \Input::get();
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\User::forge()->remove(\Input::post('data.id')));
	}
}