<?php
namespace Controller\Admin;

class Fittest extends Auth {
	public function get_index() {
		$result = \DB::query('SELECT * FROM content')->execute();
		foreach($result as $row) {
			$row['content'] = str_replace('class="row', 'class="grid-x', $row['content']);
			$row['content'] = str_replace('class="column', 'class="cell', $row['content']);
			$row['content'] = str_replace(' column ', ' cell ', $row['content']);
			\DB::update('content')->value('content', $row['content'])->where('id', '=', $row['id'])->execute();
		}
	}
}