<?php
namespace Controller\Admin;

class Process extends Auth {

	public function get_index() {
		$data['user_id'] = $this->authlite->get_user()->id;
		return $this->response(\View::forge('admin/process/index', $data, false));
	}

	public function get_orderable() {
		$view = \View::forge('admin/process/orderable');
		$hascustom = \Model\Heading::find(array(
			'where' => array(
				'parent_id' => \Input::get('heading_id'),
				'user_id' => \Input::get('user_id')
			)
		));
		if ($hascustom === null) {
			$sub_page = \Model\Heading::find_by_pk(\Input::get('heading_id'));
		} else {
			$sub_page = current($hascustom);
		}
		$view->sub_page = $sub_page;
		$view->data = \Model\Process::forge()->get_heading(\Input::get());
		$view->unused = \Model\Process::forge()->get_unused(\Input::get());
		return $this->response($view);
	}

	public function get_modify() {
		$view = \View::forge('admin/process/modify');
		$data = \Model\Process::find_by_pk(\Input::get('id'));
		$view->status_attr = array('id' => 'status_id');
		if ($data === null) {
			$data = \Model\Process::$_defaults;
			$data['heading_id'] = \Input::get('heading_id');
			$view->status_attr['disabled'] = 'disabled';
		} else {
			$data = $data->to_array();
			$view->status_attr['disabled'] = 'disabled';
		}
		$data['status_id'] = \Model\Heading::S_CUSTOM;
		$data['content_type_field'] = \View::forge('admin/partials/content_type_html', array('data' => $data));
		$view->data = $data;
		return $this->response($view);
	}

	public function post_modify() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($process = \Model\Process::find_by_pk($post['id'])) === null ) {
			$process = \Model\Process::forge();
		}
		$val = $process->validation();
		if ( $post['content_type'] != \Model\Process::TYPE_IMAGE ) {
			$req = $val->add('content', 'Content')->add_rule('required');
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
		} else {

			if ($process->is_new()) {
				$process->pos = \Model\Process::forge()->position_number($post['heading_id']);
				$process->created_by = $this->authlite->get_user()->id;
				$process->parent_id = 0;
			}

			if ($process->title != $post['title'] && $process->status_id == \Model\Process::S_SYSTEM) {
				//duplicate this row for this user, with status = custom
				$custom_process = \Model\Process::forge();
				$custom_process->set($process->to_array());
				$custom_process->set($post);
				$custom_process->parent_id = $process->id;
				$custom_process->id = null;
				$custom_process->status_id = \Model\Heading::S_CUSTOM;
				$custom_process->save();
			} else {
				$process->set($post);
				$process->updated_by = $this->authlite->get_user()->id;
				$process->save();
			}
			
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_parent_dropdown() {
		$view = \View::forge('admin/partials/process_parent_dropdown');
		$view->data = array('parent_id' => null);
		return $this->response($view);
	}

	public function post_activate() {
		return $this->response(\Model\Process::forge()->activate(\Input::post('id')));
	}

	public function post_deactivate() {
		return $this->response(\Model\Process::forge()->deactivate(\Input::post()));
	}

	public function post_order() {
		return $this->response(\Model\Process::forge()->reorder(\Input::post('data')));
	}

	public function get_image() {
		$data['data'] = \Model\Process::find_by_pk(\Input::get('id'));
		return $this->response(\View::forge('admin/process/image', $data, false));
	}

	public function get_revert() {
		$view = \View::forge('admin/process/revert');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_revert() {
		return $this->response(\Model\Process::forge()->revert(\Input::post('data.id')));
	}

	public function post_user_heading() {
		$view = \View::forge('admin/process/user_heading_dropdown');
		$view->user_id = \Input::post('user_id');
		return $this->response($view);
	}

}