<?php
namespace Controller\Admin;

class Templates extends Auth {
	public $is_active = 'templates';

	public function get_index() {
		$page = \View::forge('admin/templates/index', null, false);
		$page->users = (array)\Model\Template::find(function($query){
            if ($this->authlite->get_user()->role_id > 0) {
                $query->where('role_id', $this->authlite->get_user()->role_id);
            }
            return $query;
        });
		$this->template->styles = $this->_datatable_css;
		$this->template->scripts = $this->_datatable_js;
		$this->template->styles = array_merge($this->_datatable_css, array('easy-autocomplete.min.css', 'style.css'));
		$this->template->scripts = array_merge($this->_datatable_js, array('jquery.easy-autocomplete.min.js'));
		$this->template->content = $page;
	}

    public function action_load($id) {
        \Asset::add_path('assets/kubeds', 'img');
        $where = array('id' => $id);
        if ($this->authlite->get_user()->role_id > 0) {
            $where['role_id'] = $this->authlite->get_user()->role_id;
        }
        $template = \Model\Template::find_one_by($where);
        if ($template) {
            $page = \View::forge('admin/template', null, false);
            $page->profile = null;
            $page->pages = \View::forge('admin/users/pages');
            $page->pages->user = null;
            $page->pages->template_id = $template->id;
            $page->pages->print_href = \Uri::create('admin/templates/print/:id', array('id' => $template->id));
            $page->pages->steps = \Model\Step::forge()->get_for_template($template->id);
            $options = \Model\Option::forge()->get_for_template($template->id);
            usort($options, array('\Model\Option', 'comp'));
            $page->pages->options = $options;
            $content = \Model\Content::forge()->get_for_template($template->id);
            usort($content, array('\Model\Content', 'comp'));
            $page->pages->content = $content;
            $approved = \Model\User\Product::forge()->get_for_template($options);
            usort($approved, array('\Model\User\Product', 'comp'));
            $page->pages->approved = \Model\User\Product::forge()->format($approved);
            $page->pages->msdss = \Model\Msds::forge()->get_for_user(true);
            $page->pages->all_supports = \Model\Support\All::find(array('order_by' => array('name' => 'asc')));
            $this->template->styles = array('easy-autocomplete.min.css', 'uploadfile.css', 'style.css');
            $this->template->scripts = array('jquery.easy-autocomplete.min.js', 'jquery.uploadfile.min.js', 'tinymce.min.js', 'jquery.tinymce.min.js');
        } else {
            $page = \View::forge('admin/templates/notfound', null, false);
        }
        $this->template->content = $page;
    }

    public function action_print($id) {
        if ( ($template = \Model\Template::find_by_pk($id)) === null ) {
            throw new \HttpNotFoundException;
        }
        $mpdf = new \Mpdf\Mpdf(array(
            //'orientation' => 'L'
            'tempDir' => APPPATH . 'tmp'
        ));
        $page['data'] = \Pages::home(\Model\User::find_by_pk(\Model\User::U_DEFAULT));
        $page['logo'] = \Model\Template::forge()->get_logo();
        $data['yield'] = \View::forge('home', $page)->render();

        $step = \Model\Step::find_one_by(array('route' => 'repair', 'template_id' => $template->id));
        $option = \Model\Option::find_one_by(array('step_id' => $step->id));
        $contents = \Model\Content::find(array(
            'where' => array('option_id' => $option->id),
            'order_by' => array(
                'pos' => 'asc'
            )
        ));

        if ( empty($contents) ) {
            throw new \HttpNotFoundException;
        }
        $mpdf->WriteHTML(\Asset::css('app.css', array(), null, true), 1);
        $mpdf->WriteHTML(\Asset::css('style.css', array(), null, true), 1);
        $mpdf->WriteHTML(\Asset::css('foundation-icons.css', array(), null, true), 1);
        $mpdf->WriteHTML(\Asset::css('home.css', array(), null, true), 1);
        $mpdf->WriteHTML('h3,h4{font-weight:bold;margin-top:10px;margin-bottom:15px;}', 1);
        foreach ($contents as $content) {
            $data['yield'] = $content->content;
            $mpdf->AddPage();
            $mpdf->WriteHTML(\View::forge('print', $data)->render(), 2);
        }
        $mpdf->output();
    }

	public function get_update() {
        $view = \View::forge('admin/templates/add');
        $view->data = \Model\Template::find_by_pk(\Input::get('id'));
        return $this->response($view);
    }

    public function get_edit() {
        $view = \View::forge('admin/templates/edit');
        $view->data = \Model\Template::find_by_pk(\Input::get('id'));
        return $this->response($view);
    }

    public function post_update() {
        $post = array_map('trim', \Input::post('data'));
        if ( ($template = \Model\Template::find_by_pk($post['id'])) === null ) {
            $template = \Model\Template::forge();
        }

        $val = $template->validation();
        $req = $val->add('name', 'Name')->add_rule('required');
        if ( $template->is_new() || $post['name'] != $template->name ) {
            $val->add_callable($template);
            $req->add_rule('unique');
            $template->created_by = $this->authlite->get_user()->id;
        }

        if (! $val->run($post) ) {
            return $this->response(array('status' => 'FAIL', 'msg' => $val->show_errors()));
        } else {
            $template->set($post);
            $template->updated_by = $this->authlite->get_user()->id;
            $clone = \Model\Template::find_by_pk($post['clone']);
            if (!empty($clone)) {
                $template->role_id = $clone->role_id;
            }
            $template->save();
            \Model\Template::forge()->clone_template(array('clone_id' => $post['clone'], 'template_id' => $template->id));
            return $this->response(array('status' => 'OK'));
        }
    }

    public function get_delete() {
        $view = \View::forge('admin/companies/delete');
        $view->data = array('id' => \Input::get('id'));
        return $this->response($view);
    }

    public function post_delete() {
        return $this->response(\Model\Template::forge()->remove(\Input::post('data.id')));
    }


}