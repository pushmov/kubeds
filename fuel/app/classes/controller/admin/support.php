<?php
namespace Controller\Admin;

class Support extends Auth {

	public function before() {
		parent::before();
		\Asset::add_path('assets/kubeds', 'img');
		\Asset::add_path('assets/kubeds/0001', 'img');
	}

	public function get_index() {
		return $this->response(\View::forge('admin/support/index'));
	}

	public function get_all() {
		return $this->response(\Model\Support::forge()->datatable(\Input::get()));
	}

	public function get_view() {
		$view = \View::forge('admin/support/view');
		$view->data = \Model\Support::forge()->load(\Input::get('id'));
		$files = glob( DOCROOT .'assets/kube/'. \Model\Support::TECH_SUPPORT_PATH . str_pad(\Input::get('id'), 3, '0', STR_PAD_LEFT) . '.*' );
		if( !empty($files) && is_readable(current($files)) ) {
			$view->logo = basename(current($files));
		} else {
			$view->logo = '';
		}
		$view->qr = str_pad(\Input::get('id'), 3, '0', STR_PAD_LEFT) . '-qr.png';
		return $this->response($view);
	}

	public function get_update() {
		$view = \View::forge('admin/support/modify');
		$id = \Input::get('id');
		$supports = \Model\Support::find( function($query) use ($id) {
			return $query
				->select(\Model\Support::get_table_name() . '.*', array('s.name', 'support_name'), array('s.site_url', 'support_url'))
				->join(array(\Model\Supplier::get_table_name(), 's'))
				->on('s.id', '=', \Model\Support::get_table_name() . '.sup_id')
				->where( \Model\Support::get_table_name() . '.id', '=', $id);
		});
		if ($supports !== null) {
			$data = current($supports)->to_array();
			$data['show_upload'] = false;
			$title = 'Edit Support';
			$path = DOCROOT .'assets/kubeds/' . \Model\Support::TECH_SUPPORT_PATH . str_pad($data['id'], 3, '0', STR_PAD_LEFT);
			$logos = glob( $path . '.*');
			$data['logo_label'] = basename(current($logos));
		} else {
			$data = \Model\Support::$_defaults;
			$data['support_name'] = $data['support_url'] = '';
			$data['show_upload'] = true;
			$title = 'Add New Support';
			$data['logo_label'] = '';
		}
		$data['option'] = \Input::get('option');
		$view->data = $data;
		$view->title = $title;
		return $this->response($view);
	}

	public function post_update() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($support = \Model\Support::find_by_pk($post['id'])) === null ) {
			$support = \Model\Support::forge();
			$is_new = $support->is_new();
		}

		if ($support->is_new() && ($_FILES['support_file']['error'] == 4 || empty($_FILES['support_file']))) {
			$json['msg'] = array('support_file' => 'Logo required');
			$json['status'] = \Model\Base::RES_FAIL;
			return $this->response($json);
		}

		$val = $support->validation();
		$val->add_callable($support);
		$req = $val->add('sup_id', 'Support')->add_rule('required');
		$req->add_rule('valid_supplier');
		if ( $support->is_new() || $support['sup_id'] != $support->sup_id ) {
			$req->add_rule('unique');
		}
		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));	
		} else {
			$support->set($post);
			$support->save();

			if ( $_FILES['support_file']['name'] != '' ) {
				$output_dir = DOCROOT .'assets/kubeds/'. \Model\Support::TECH_SUPPORT_PATH;
				$ext = pathinfo($_FILES['support_file']['name'], PATHINFO_EXTENSION);
				$fileName = str_pad($support->id, 3, '0', STR_PAD_LEFT) . '.' . strtolower($ext);
				//delete current logo if any
				$files = glob($output_dir . str_pad($support->id, 3, '0', STR_PAD_LEFT) . '.*');
				if ( is_readable(current($files)) ) {
					unlink(current($files));
				}
				move_uploaded_file($_FILES['support_file']['tmp_name'], $output_dir . $fileName);
			}
			return $this->response(array('status' => 'OK', 'hash' => '#href-option-'.$post['option']));
		}
	}

	public function get_delete() {
		$view = \View::forge('admin/support/delete');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Support::forge()->remove(\Input::post('data.id')));
	}

	public function get_logo() {
		$view = \View::forge('admin/support/logo');
		$data = \Model\Support::forge()->load(\Input::get('id'));
		$filename = str_pad(\Input::get('id'), 3, '0', STR_PAD_LEFT);
		$files = glob( DOCROOT .'assets/kube/'. \Model\Support::TECH_SUPPORT_PATH.$filename.'.*' );
		if( !empty($files) && is_readable(current($files)) ) {
			$data['img'] = basename(current($files));
		} else {
			$data['img'] = '';
		}
		$view->data = $data;
		return $this->response($view);
	}

	public function get_qr() {
		$view = \View::forge('admin/support/qr');
		$view->data = \Model\Support::forge()->load(\Input::get('id'));
		$view->qr = str_pad(\Input::get('id'), 3, '0', STR_PAD_LEFT) . '-qr.png';
		return $this->response($view);
	}

	public function get_logo_file() {
		$return = array();
		$filename = str_pad(\Input::get('id'), 3, '0', STR_PAD_LEFT);
		$files = glob( DOCROOT .'assets/kube/'. \Model\Support::TECH_SUPPORT_PATH.$filename.'.*' );
		if( !empty($files) && is_readable(current($files)) ) {
			$return['img'] = basename(current($files)).PHP_EOL;
			$return['status'] = \Model\Base::RES_OK;
		} else {
			$return['status'] = \Model\Base::RES_FAIL;
		}
		return $this->response($return);
	}

	public function post_doupload() {
		$output_dir = DOCROOT .'assets/kube/'. \Model\Support::TECH_SUPPORT_PATH;
		$json = array();
		if ($_FILES['support_file']['error']) {
			$json['msg'] = $_FILES['support_file']['error'];
			$json['status'] = \Model\Base::RES_FAIL;
		} else {
			$ext = pathinfo($_FILES['support_file']['name'], PATHINFO_EXTENSION);
			$fileName = str_pad(\Input::post('id'), 3, '0', STR_PAD_LEFT) . '.' . strtolower($ext);
			//delete current logo if any
			$files = glob(DOCROOT .'assets/kube/'. \Model\Support::TECH_SUPPORT_PATH.str_pad(\Input::post('id'), 3, '0', STR_PAD_LEFT) . '.*');
			if ( is_readable(current($files)) ) {
				unlink(current($files));
			}
			move_uploaded_file($_FILES['support_file']['tmp_name'], $output_dir . $fileName);
			$json['img'] = $fileName;
		}
		return $this->response($json);
	}

	public function post_toggle() {
		if ( ($support_master = \Model\Support\All::find_by_pk(\Input::post('id'))) !== null ) {
			if (\Input::post('checked') == \Model\Support::S_DISABLED) {
				$row = \Model\Support::find_one_by(array('option_id' => \Input::post('option'), 'support_id' => \Input::post('id')));
				$row->remove($row->id);
			} else {
				$support = \Model\Support::forge();
				$support->option_id = \Input::post('option');
				$support->support_id = $support_master->id;
				$support->status_id = \Input::post('checked');
				$support->save(false);
			}
			$option = \Model\Option::find_by_pk(\Input::post('option'));
			return $this->response(array('hash' => '#href-option-'.$option->id));
		}
	}

}