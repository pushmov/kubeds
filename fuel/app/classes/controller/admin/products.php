<?php
namespace Controller\Admin;

class Products extends Auth {
	public $is_active = 'products';

	public function get_index() {
		$this->template->styles = array_merge($this->_datatable_css, array('easy-autocomplete.min.css', 'style.css'));
		$this->template->scripts = array_merge($this->_datatable_js, array('jquery.easy-autocomplete.min.js'));
		$this->template->content = \View::forge('admin/products/index');
	}

	public function get_all() {
		return $this->response(\Model\Product::forge()->datatable(\Input::get()));
	}

	public function get_update() {
		$view = \View::forge('admin/products/update');
		$data = \Model\Product::forge()->view(\Input::get('id'));
		if ($data['id'] === null) {
			$view->title = 'Add New Product Data';
		} else {
			$view->title = 'Edit Product Data';
		}
		$data['supplier_name'] = isset($data['supplier_name']) ? $data['supplier_name'] : '';
		$view->data = $data;
		return $this->response($view);
	}

	public function post_update() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($product = \Model\Product::find_by_pk($post['id'])) === null ) {
			$product = \Model\Product::forge();
		}

		$val = $product->validation();
		$val->add_callable($product);
		if (!$val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->show_errors()));
		} else {
			$product->set($post);
			if ( $product->is_new()) {
				$product->created_by = $this->authlite->get_user()->id;
			}
			$product->updated_by = $this->authlite->get_user()->id;
			$product->save(false);
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_view() {
		$view = \View::forge('admin/products/view');
		$view->data = \Model\Product::forge()->view(\Input::get('id'));
		return $this->response($view);
	}

	public function get_delete() {
		$view = \View::forge('admin/products/delete');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Product::forge()->remove(\Input::post('data.id')));
	}

	public function get_autocomplete() {
		$data = array();
		$products = \Model\Product::find( function($query) {
			return $query->select(\Model\Product::get_table_name() . '.id', \Model\Product::get_table_name() . '.part_num', 'sup_id', array('s.name', 'sup_name'))
			->join(array(\Model\Supplier::get_table_name(), 's'))
			->on('s.id', '=', \Model\Product::get_table_name() . '.sup_id');
		});
		if ($products !== null) {
			foreach ($products as $row) {
				$data[] = array('name' => $row->part_num, 'id' => $row->id, 'sup_id' => $row->sup_id, 'sup_name' => $row->sup_name);
			}
		}
		return $this->response($data);
	}
}