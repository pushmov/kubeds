<?php
namespace Controller\Admin;

class Steps extends Auth {
	public function before() {
		parent::before();
		\Asset::add_path('assets/kubeds', 'img');
		\Asset::add_path('assets/kubeds/files', 'img');
	}
	public function post_enable() {
		$step = \Model\Step::find_by_pk(\Input::post('id'));
		if ($step) {
			switch ($step->parent_id) {
				case 0:
					$step->status_id = \Model\Step::S_SYSTEM;
					break;
				default:
					$step->status_id = \Model\Step::S_CUSTOM;
					break;
			}
			$step->save(false);
			$hash = $step->id;

			//default parent
			if ($step->parent_id > 0) {
				$parent = \Model\Step::find_by_pk($step->parent_id);
				if ($parent->title == $step->title && $parent->route == $step->route) {
					$step->remove($step->id);
					$hash = $parent->id;
				}
			}
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-step-' . $hash));
	}

	public function post_disable() {
		$step = \Model\Step::find_by_pk(\Input::post('id'));
		if ($step) {
			if ($step->parent_id > 0 || in_array(\Input::post('user_id'), \Model\User::user_default_template())) {
				$step->status_id = \Model\Step::S_UNUSED;
				$step->save(false);
				$hash = $step->id;
			} else {
				$custom = \Model\Step::forge();
				$custom->set($step->to_array());
				$custom->id = null;
				$custom->parent_id = $step->id;
				$custom->user_id = \Input::post('user_id');
				$custom->status_id = \Model\Step::S_UNUSED;
				$custom->save(false);
				$hash = $custom->id;
			}
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-step-' . $hash));
	}

	public function get_edit() {
		$view = \View::forge('admin/steps/update');
		$data = \Model\Step::find_by_pk(\Input::get('id'));
		$view->user_id = \Input::get('user_id');
		$view->title = 'Add New Step';
		$view->status_attr = array('id' => 'status_heading');
		$view->status_id = in_array(\Input::get('user_id'), \Model\User::user_default_template()) ? \Model\Step::S_SYSTEM : \Model\Step::S_CUSTOM;
		if ($data !== null) {
			$view->title = 'Edit Step';
			$view->status_attr['disabled'] = 'disabled';
			$data['created_name'] = \Model\Admin::find_by_pk($data['created_by'])->full_name();
			$data['updated_name'] = \Model\Admin::find_by_pk($data['updated_by'])->full_name();
		}
		$view->data = $data;
		return $this->response($view);
	}

	public function post_edit() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($step = \Model\Step::find_by_pk($post['id'])) === null ) {
			$step = \Model\Step::forge(\Model\Step::$_defaults);
		}

		$val = $step->validation();
		if ( $step->is_new() ) {
			$step->created_by = $this->authlite->get_user()->id;
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));

		} else {

			//clone record with S_CUSTOM when title or route has different value from system template. and the status_id is system template
			if ( $post['title'] != $step->title && $step->status_id == \Model\Step::S_SYSTEM && !in_array($post['user_id'], \Model\User::user_default_template()) ) {
				$custom_step = \Model\Step::forge();
				//parent data set
				$custom_step->set($step->to_array());
				//user input data set
				$custom_step->set($post);
				$custom_step->id = null;
				$custom_step->parent_id = $step->id;
				$custom_step->status_id = \Model\Step::S_CUSTOM;
				$custom_step->save();
				$hash = $custom_step->id;
			} else {
				$step->set($post);
				$step->updated_by = $this->authlite->get_user()->id;
				$step->save();
				$hash = $step->id;
			}
			return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-step-' . $hash));
		}
	}

	public function post_revert() {
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if ( ($step = \Model\Step::find_by_pk(\Input::post('id'))) !== null ) {
			if ($step->parent_id > 0 && $step->status_id == \Model\Step::S_CUSTOM) {
				$hash = $step->parent_id;
				$step->remove($step->id);
			}
			$response['hash'] = '#href-step-' . $hash;
		}
		return $this->response($response);
	}

	public function get_icons() {
		$view = \View::forge('admin/steps/icon');
		$view->data = \Model\Step::find_by_pk(\Input::get('id'));
		return $this->response($view);
	}

	public function post_icons() {
		if ( ($step = \Model\Step::find_by_pk(\Input::post('id'))) !== null ) {
			if ( in_array(\Input::post('userid'), \Model\User::user_default_template()) ) {
				$step->icon = \Input::post('icons');
				$step->save(false);
			} else {
				$custom = \Model\Step::forge()->set($step->to_array());
				$custom->id = null;
				$custom->parent_id = $step->id;
				$custom->user_id = \Input::post('userid');
				$custom->icon = \Input::post('icons');
				$custom->status_id = \Model\Step::S_CUSTOM;
				$custom->save(false);
			}
			return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-step-' . $step->id));
		}
	}

}