<?php
namespace Controller\Admin;

class Suppliers extends Auth {
	public $is_active = 'suppliers';

	public function get_index() {
		$this->template->styles = $this->_datatable_css;
		$this->template->scripts = $this->_datatable_js;
		$this->template->content = \View::forge('admin/suppliers/index');
	}

	public function get_all() {
		return $this->response(\Model\Supplier::forge()->datatable(\Input::get()));
	}

	public function get_view() {
		$view = \View::forge('admin/suppliers/view');
		$view->data = \Model\Supplier::forge()->view(\Input::get('id'));
		return $this->response($view);
	}

	public function get_update() {
		$view = \View::forge('admin/suppliers/update');
		$data = \Model\Supplier::find_by_pk(\Input::get('id'));
		$view->data = $data;
		$view->title = 'Add New Supplier';
		if ($data !== null) {
			$view->title = 'Edit Supplier';
		}
		return $this->response($view);
	}

	public function post_update() {
		$post = array_map('trim', \Input::post('data'));

		if ( ($supplier = \Model\Supplier::find_by_pk($post['id'])) === null ) {
			$supplier = \Model\Supplier::forge();
		}

		$val = $supplier->validation();
		$req = $val->add('name', 'Name')->add_rule('required');
		if ($supplier->is_new() || $post['name'] != $supplier->name ) {
			$val->add_callable($supplier);
			$req->add_rule('unique');
			$supplier->created_by = $this->authlite->get_user()->id;
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->show_errors()));
		} else {
			$supplier->set($post);
			$supplier->updated_by = $this->authlite->get_user()->id;
			$supplier->save();
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_delete() {
		$view = \View::forge('admin/suppliers/delete');
		$view->data = \Model\Supplier::find_by_pk(\Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Supplier::forge()->remove(\Input::post('data.id')));
	}

	public function get_autocomplete() {
		$data = array();
		$suppliers = \Model\Supplier::find_by_status_id(\Model\Supplier::S_ACTIVE);
		if ($suppliers !== null) {
			foreach ($suppliers as $row) {
				$data[] = array('name' => $row->name, 'id' => $row->id, 'url' => $row->site_url);
			}
		}
		return $this->response($data);
	}

}