<?php
namespace Controller\Admin;

class Login extends Auth
{
	public $template = 'admin/admin_base';

	public function before()
	{
		parent::before();
		$this->authlite->logout();
	}

	public function action_index()
	{
		if (\Input::post('login')) {
			$data = \Input::post();
			if ($this->authlite->login(\Input::post('email'), \Input::post('passwrd'))) {
				$user = $this->authlite->get_user();
				$admin = \Model\Admin::find_by_pk($user->id);
				$admin->last_login = date_create()->format('Y-m-d H:i:s');
				$admin->save(false);
				if ($user->status_id == \Model\Admin::S_ACTIVE) {
					\Response::redirect('/admin');
				}
			}
			$data['message'] = '<div class="callout alert small">Login Error. Try again</div>';
		} else {
			$data = array('email' => '', 'passwrd' => '', 'message' => '');
		}

		$this->template->content = \View::forge('admin/login', $data, false);
	}

	public function action_logout()
	{
		$this->authlite->logout();
		\Response::redirect('/admin/login');
	}

}