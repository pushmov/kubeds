<?php
namespace Controller\Admin;

class Heading extends Auth {

	public function get_index() {
		return $this->response(\View::forge('admin/heading/index'));
	}

	public function get_view() {
		$view = \View::forge('admin/heading/view');
		$view->data = \Model\Heading::forge()->view(\Input::get('id'));
		return $this->response($view);
	}

	public function get_modify() {
		$view = \View::forge('admin/heading/modify');
		$active = \Model\Heading::forge()->user_active_heading_template(\Input::get('id'));
		$unused = \Model\Heading::forge()->user_unused_heading_template(\Input::get('id'));
		$view->active = ($active === null) ? array() : $active;
		$view->unused = ($unused === null) ? array() : $unused;
		return $this->response($view);
	}

	public function post_modify() {
		return $this->response(\Model\Heading::forge()->reorder(\Input::post('data')));
	}

	public function post_activate() {
		return $this->response(\Model\Heading::forge()->activate(\Input::post('id')));
	}

	public function post_deactivate() {
		return $this->response(\Model\Heading::forge()->deactivate(\Input::post()));
	}

	public function get_update() {
		$view = \View::forge('admin/heading/update');
		$data = \Model\Heading::find_by_pk(\Input::get('id'));
		$view->data = $data;
		$view->user_id = \Input::get('user_id');
		$view->title = 'Add New Heading';
		$view->status_attr = array('id' => 'status_heading');
		if ($data !== null) {
			$view->title = 'Edit Heading';
			$view->status_attr['disabled'] = 'disabled';
		}
		return $this->response($view);
	}

	public function get_activate() {
		$view = \View::forge('admin/heading/activate');
		$view->data = \Model\Heading::find_by_pk(\Input::get('id'));
		return $this->response($view);
	}

	public function get_delete() {
		$view = \View::forge('admin/heading/delete');
		$view->data = \Model\Heading::find_by_pk(\Input::get('id'));
		return $this->response($view);
	}

	public function post_save() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($heading = \Model\Heading::find_by_pk($post['id'])) === null ) {
			$heading = \Model\Heading::forge(\Model\Heading::$_defaults);
		}
		
		$val = $heading->validation();
		$req = $val->add('route', 'Route')->add_rule('required');
		if ( $heading->is_new() || $post['route'] != $heading->route ) {
			$val->add_callable($heading);
			$req->add_rule('unique');
			$heading->created_by = $this->authlite->get_user()->id;
		}

		if ( ! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->error_message()));
			
		} else {

			//clone record with S_CUSTOM when title or route has different value from system template. and the status_id is system template
			if ( ($post['route'] != $heading->route || $post['title'] != $heading->title) && $heading->status_id == \Model\Heading::S_SYSTEM ) {
				$custom_heading = \Model\Heading::forge();
				//parent data set
				$custom_heading->set($heading->to_array());
				//user input data set
				$custom_heading->set($post);
				$custom_heading->id = null;
				$custom_heading->parent_id = $heading->id;
				$custom_heading->status_id = \Model\Heading::S_CUSTOM;
				$custom_heading->save();
			} else {
				$heading->set($post);
				$heading->updated_by = $this->authlite->get_user()->id;
				$heading->save();
			}
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_parent_dropdown() {
		$data['parent_id'] = null;
		$view = \View::forge('admin/partials/parent_dropdown');
		$view->data = $data;
		return $this->response($view);
	}

	public function get_revert() {
		$view = \View::forge('admin/heading/revert');
		$view->data = \Model\Heading::find_by_pk(\Input::get('id'));
		return $this->response($view);
	}

	public function post_revert() {
		return $this->response(\Model\Heading::forge()->revert(\Input::post('data.id')));
	}

}