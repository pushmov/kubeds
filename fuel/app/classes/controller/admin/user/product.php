<?php
namespace Controller\Admin\User;

class Product extends \Controller\Admin\Auth
{

	public function post_update()
	{
		$option = \Model\Option::find_by_pk(\Input::post('option_id'));

		try {
			$inputFileName = $_FILES['document']['tmp_name'];
			$inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
			$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch (Exception $e) {
			return $this->response(array('status' => 'FAIL', 'msg' => $e->getMessage()));
		}
		$post = \Input::post();
		$post['user_id'] = $option->user_id;
		\Model\User\Product::forge()->delete_for_user($post);
		$post['updated_by'] = $this->authlite->get_user()->id;
		$post['updated_at'] = date_create()->format('Y-m-d H:i:s');

		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		$arrWorksheet = $activeSheet->toArray();
		for ($j = 2; $j <= count($arrWorksheet) - 1; $j++) {
			$part_num = $arrWorksheet[$j][1];
			$manufacturer = $arrWorksheet[$j][0];
			$description = $arrWorksheet[$j][2];
			$data = array_merge($post, array('manufacturer' => $manufacturer, 'part_num' => $part_num, 'description' => $arrWorksheet[$j][2]));
			$userproduct = \Model\User\Product::forge()->set($data);
			$userproduct->save(false);
		}
		return $this->response(array('status' => 'OK', 'msg' => 'Success', 'hash' => '#href-option-' . $option->id));
	}
}