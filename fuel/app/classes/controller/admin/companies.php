<?php
namespace Controller\Admin;

class Companies extends Auth {
	public $is_active = 'companies';

	public function get_index() {
		$this->template->styles = $this->_datatable_css;
		$this->template->scripts = array_merge($this->_datatable_js, array('jquery.uploadfile.min.js'));
		$this->template->content = \View::forge('admin/companies/index');
	}

	public function get_all() {
		return $this->response(\Model\Template::forge()->datatable(\Input::get()));
	}

	public function get_view() {
		$view = \View::forge('admin/companies/view');
		$view->data = \Model\Template::forge()->view(\Input::get('id'));
		return $this->response($view);
	}

	public function get_update() {
		$view = \View::forge('admin/companies/update');
		$data = \Model\Template::find_by_pk(\Input::get('id'));
		$view->data = $data;
		$view->title = 'Add New Company';
		if ($data !== null) {
			$view->title = 'Edit Company';
		}
		return $this->response($view);
	}

	public function post_update() {
		$post = array_map('trim', \Input::post('data'));
		if ( ($template = \Model\Template::find_by_pk($post['id'])) === null ) {
			$template = \Model\Template::forge();
		}

		$val = $template->validation();
		$req = $val->add('name', 'Name')->add_rule('required');
		if ( $template->is_new() || $post['name'] != $template->name ) {
			$val->add_callable($template);
			$req->add_rule('unique');
			$template->created_by = $this->authlite->get_user()->id;
		}

		if (! $val->run($post) ) {
			return $this->response(array('status' => 'FAIL', 'msg' => $val->show_errors()));
		} else {
			$template->set($post);
			$template->updated_by = $this->authlite->get_user()->id;
			$template->save();
			return $this->response(array('status' => 'OK'));
		}
	}

	public function get_delete() {
		$view = \View::forge('admin/company/delete');
		$view->data = array('id' => \Input::get('id'));
		return $this->response($view);
	}

	public function post_delete() {
		return $this->response(\Model\Template::forge()->remove(\Input::post('data.id')));
	}

	public function get_autocomplete() {
		$data = array();
		$companies = \DB::select('*')->from('users')->group_by('customer')->execute();
		if ($companies !== null) {
			foreach ($companies as $row) {
				$data[] = array('name' => $row['customer'], 'id' => $row['id']);
			}
		}
		return $this->response($data);
	}

	public function get_logo() {
		\Asset::add_path('assets/kubeds', 'img');
		\Asset::add_path('assets/kubeds/0001', 'img');
		$files = glob(DOCROOT .'assets/kubeds/company/'.str_pad(\Input::get('id'), 5, '0', STR_PAD_LEFT) . '.*');
		$logo = '';
		if ( ! empty($files) ) {
			$logo = \Asset::img('company/'.basename(current($files)));
		}
		$data['logo'] = $logo;
		$data['id'] = \Input::get('id');
		$view = \View::forge('admin/companies/logo', $data, false);
		return $this->response($view);
	}

	public function post_logo() {
		\Asset::add_path('assets/kubeds/company', 'img');
		$output_dir = DOCROOT .'assets/kubeds/company/';
		if (!is_dir($output_dir)) {
			mkdir( DOCROOT . 'assets/kubeds/company', 0777);
		}
		$json = array();
		if ($_FILES['company_logo']['error']) {
			$json['msg'] = $_FILES['company_logo']['error'];
			$json['status'] = 'OK';
		} else {
			$ext = pathinfo($_FILES['company_logo']['name'], PATHINFO_EXTENSION);
			$fileName = str_pad(\Input::post('id'), 5, '0', STR_PAD_LEFT) . '.' . strtolower($ext);
			//delete current logo if any
			$files = glob(DOCROOT .'assets/kubeds/company/'.str_pad(\Input::post('id'), 5, '0', STR_PAD_LEFT) . '.*');
			if ( is_readable(current($files)) ) {
				unlink(current($files));
			}
			move_uploaded_file($_FILES['company_logo']['tmp_name'], $output_dir . $fileName);
			$json['img'] = \Asset::get_file($fileName, 'img');
		}
		return $this->response($json);
	}


}