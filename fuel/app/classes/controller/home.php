<?php
namespace Controller;

class Home extends Auth {
	protected $_user;

	public function before() {
		parent::before();
		\Asset::add_path('assets/kubeds', 'img');
		$this->_user = $this->authlite->get_user();
	}

	public function action_index() {
		$page['data'] = \Pages::home($this->_user);
		$page['logo'] = \Model\Template::forge()->get_logo($this->authlite->get_user());
		$this->template->styles = array('home.css');
		$this->template->content = \View::forge('home', $page, false);
	}

}