<?php
namespace Controller;

class Contents extends Auth {

	public function before() {
		parent::before();
		\Asset::add_path('assets/kubeds', 'img');
		\Asset::add_path('assets/kubeds/0001', 'img');
	}

	public function post_load() {
		$view = \Model\Content::find_by_pk(\Input::post('id'));
		if ($view == null) {
			throw new HttpNotFoundException;
		}
		return $this->response($view->content, null, false);
	}

}