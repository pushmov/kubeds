<?php

class Common {
	// Content types
	const CT_HTML = 1;	// Content is html
	const CT_IMG = 2;	// content is an image
	const CT_PDF = 3;	// content is a PDF
	const CT_URL = 4;	// content is a link to a url
	// Page status
	const S_INACTIVE = 0;	// Inactive don't show
	const S_TEMPLATE = 1;	// This is a system template
	const S_PARENT = 2;		// Use the parent content
	const S_CUSTOM = 3;		// Use custom content

	public static function content($id = null) {
		$types = array(self::CT_HTML => 'HTML',self::CT_IMG => 'Image',self::CT_PDF => 'PDF',self::CT_URL => 'URL');
		if ($id === null) {
			return $types;
		}
		if (array_key_exists($id, $types)) {
			return $types[$id];
		}
		return null;
	}
	public static function status($id = null) {
		$status = array(self::S_INACTIVE => 'Inactive',self::S_TEMPLATE => 'Template',self::S_PARENT => 'Parent',self::S_CUSTOM => 'Custom');
		if ($id === null) {
			return $status;
		}
		if (array_key_exists($id, $status)) {
			return $status[$id];
		}
		return null;
	}
}
