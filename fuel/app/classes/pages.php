<?php

class Pages {
	public static function home($user) {
		// Colors for each button
		$colors = array('btn-process', 'dark-blue', 'yellow', 'btn-msds', 'btn-support');
//.btn-greyed
		$data = array();
		if (\Authlite::instance('auth_admin')->logged_in()) {
			$steps = \Model\Step::forge()->get_for_template($user->template_id);
			$options = \Model\Option::forge()->get_for_template($steps);
			usort($options, array('\Model\Option', 'comp'));
		} else {
			$steps = \Model\Step::forge()->get_for_user($user);
			$options = \Model\Option::forge()->get_for_user($user);
			usort($options, array('\Model\Option', 'comp'));
		}
		foreach($steps as $step) {
			$data[$step['pos']] = array('title' => $step['title'], 'pos' => $step['pos'], 'img' => $step['icon'], 'status_id' => $step['status_id']);
			$data[$step['pos']]['label'] = $step['pos'] . '. ' . $step['title'];
			foreach($options as $option) {
				if (($option['step_id'] == $step['id'] || $option['step_id'] == $step['parent_id']) && $option['type_id'] <= \Model\Option::T_PRODUCT) {
					$path_id = ($option['parent_id'] > 0) ? $option['parent_id'] : $option['id'];
					$path = $step['route'] . '/' . $path_id;
					$attr = array('class' => 'button ' . $colors[$option['type_id']]);
					if ($step['status_id'] == \Model\Option::S_UNUSED || $option['status_id'] == \Model\Option::S_UNUSED) {
						$attr['disabled'] = 'disabled';
						$path = '#';
					}
					if ($option['external_url'] != '') {
						$path = $option['external_url'];
						$attr['target'] = '_blank';
					}
					$data[$step['pos']]['options'][$option['pos']] = \Html::anchor($path, $option['title'], $attr);
				}
			}
		}
		return $data;
	}


	public static function get_module_num($module_name) {
		$data = array_flip(self::$_allowed_modules);
		return array_key_exists($module_name, $data) ? $data[$module_name] : $data[0];
	}

	public static function url_map($module = null) {
		$data = array(
			'admins' => '',
			'companies' => '',
			'headings' => \Uri::create('admin/heading/index.html'),
			'msds' => '',
			'procedures' => '',
			'processes' => '',
			'products' => '',
			'product_groups' => '',
			'suppliers' => '',
			'users' => ''
		);
		return array_key_exists($module, $data) ? $data[$module] : $data;
	}

	public static function title($module = null) {
		$data = array(
			'admins' => 'Admins',
			'companies' => 'Companies',
			'headings' => 'Headings',
			'msds' => 'MSDS',
			'procedures' => 'Procedures',
			'processes' => 'Processes',
			'products' => 'Products',
			'product_groups' => 'Product Groups',
			'suppliers' => 'Suppliers',
			'users' => 'Users'
		);
		return array_key_exists($module, $data) ? $data[$module] : $data;
	}

	public static function has_msds() {

	}

	public static function has_support() {

	}

	public static function has_fittest() {

	}

	public static function user_template($user_id) {
		$data = array();
		$system_headings = \Model\Heading::find(array(
			'where' => array(
				'status_id' => \Model\Heading::S_SYSTEM,
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		), 'id');
		$user_headings = \Model\Heading::find(array(
			'where' => array(
				'user_id' => $user_id
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		), 'id');

		if ( !empty($user_headings) ) {
			foreach ($user_headings as $id => $row) {
				$system_headings[$row->parent_id] = $row;
			}
		}

		if ( !empty($system_headings) ) {
			foreach ($system_headings as $id => $heading) {

				$attr_class = 'button';
				if ($heading->status_id == \Model\Heading::S_UNUSED) {
					$attr_class .= ' disabled';

					$target['process']['href'] =
					$target['products']['href'] =
					$target['procedures']['href'] = '#';

					$target['process']['class'] = $attr_class . ' process';
					$target['procedures']['class'] = $attr_class . ' sop';
					$target['products']['class'] = $attr_class . ' products';

				} else {
					$target['process']['href'] = \Uri::create($heading->route);
					$target['procedures']['href'] = \Uri::create($heading->route . '/procedures');
					$target['products']['href'] = \Uri::create($heading->route . '/products');

					$target['process']['class'] = $attr_class . ' process';
					$target['procedures']['class'] = $attr_class . ' sop';
					$target['products']['class'] = $attr_class . ' products';
				}

				switch ($id) {
					case self::SYS_CHECKIN:
						$heading->procedures = false;
						$heading->products = false;
						$heading->process = new StdClass();
						$heading->process->coords = '33,216,128,240';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => $target['process']['class']));
						break;

					case self::SYS_OFFICE:
						$heading->procedures = false;
						$heading->products = false;
						$heading->process = new StdClass();
						$heading->process->coords = '197,352,294,377';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => $target['process']['class']));
						break;

					case self::SYS_REPAIR:
						$heading->procedures = false;
						$heading->products = false;
						$heading->process = new StdClass();
						$heading->process->coords = '611,112,707,137';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => $target['process']['class']));
						break;

					case self::SYS_BODYSHOP: //system has child

						$heading->procedures = new StdClass();
						$heading->procedures->coords = '905,31,1002,55';
						$heading->procedures->target = $target['procedures']['href'];
						$heading->procedures->href = \Html::anchor($target['procedures']['href'], 'Body SOP', array('class' => self::get_button_class($heading->id, $user_id, 'procedures', $heading->pos)));

						$heading->products = new StdClass();
						$heading->products->coords = '907,95,1001,130';
						$heading->products->target = $target['products']['href'];
						$heading->products->href = \Html::anchor($target['products']['href'], 'Approved Products', array('class' => self::get_button_class($heading->id, $user_id, 'products', $heading->pos)));

						$heading->process = new StdClass();
						$heading->process->coords = '905,63,1002,87';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => self::get_button_class($heading->id, $user_id, 'process', $heading->pos)));
						break;

					case self::SYS_PAINTSHOP: //system has child
						$heading->procedures = new StdClass();
						$heading->procedures->coords = '914,496,1009,520';
						$heading->procedures->target = $target['procedures']['href'];
						$heading->procedures->href = \Html::anchor($target['procedures']['href'], 'Paint SOP', array('class' => self::get_button_class($heading->id, $user_id, 'procedures', $heading->pos)));

						$heading->products = new StdClass();
						$heading->products->coords = '914,555,1010,589';
						$heading->products->target = $target['products']['href'];
						$heading->products->href = \Html::anchor($target['products']['href'], 'Approved Products', array('class' => self::get_button_class($heading->id, $user_id, 'products', $heading->pos)));

						$heading->process = new StdClass();
						$heading->process->coords = '913,527,1011,551';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => self::get_button_class($heading->id, $user_id, 'process', $heading->pos)));
						break;

					case self::SYS_REASSEMBLY:
						$heading->procedures = new StdClass();
						$heading->procedures->coords = '670,495,805,521';
						$heading->procedures->target = $target['procedures']['href'];
						$heading->procedures->href = \Html::anchor($target['procedures']['href'], 'Reassembly SOP', array('class' => $target['procedures']['class']));
						$heading->process = false;
						$heading->products = false;
						break;

					case self::SYS_DETAILSHOP: //system has child
						$heading->procedures = new StdClass();
						$heading->procedures->coords = '455,495,551,521';
						$heading->procedures->target = $target['procedures']['href'];
						$heading->procedures->href = \Html::anchor($target['procedures']['href'], 'Detail SOP', array('class' => self::get_button_class($heading->id, $user_id, 'procedures', $heading->pos)));

						$heading->products = new StdClass();
						$heading->products->coords = '455,558,552,594';
						$heading->products->target = $target['products']['href'];
						$heading->products->href = \Html::anchor($target['products']['href'], 'Approved Products', array('class' => self::get_button_class($heading->id, $user_id, 'products', $heading->pos)));

						$heading->process = new StdClass();
						$heading->process->coords = '454,527,550,551';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => self::get_button_class($heading->id, $user_id, 'process', $heading->pos)));
						break;

					case self::SYS_DELIVERY:
						$heading->procedures = false;
						$heading->products = false;
						$heading->process = new StdClass();
						$heading->process->coords = '32,331,128,356';
						$heading->process->target = $target['process']['href'];
						$heading->process->href = \Html::anchor($target['process']['href'], 'Process', array('class' => $target['process']['class']));
						break;

					default:
						$heading->procedures = false;
						$heading->products = false;
						$heading->process = false;
						break;
				}
				$data[$id] = $heading;
			}
		}
		return $data;
	}

	protected static function get_button_class($heading_id, $user_id, $type, $pos) {
		//is this top parent level ? => everything under top parent level is disabled
		$heading = \Model\Heading::find_by_pk($heading_id);
		$topparent = \Model\Heading::forge()->find_top_parent($heading_id);
		if ($heading->parent_id == $topparent->id && $heading->status_id == \Model\Heading::S_UNUSED) {
			return $target[$type]['class'] = 'button disabled ' . $type;
		}

		$syschilds = \Model\Heading::find( function($query) use ($heading_id, $user_id, $pos, $type) {
			return $query
			->where('pos', '=', $pos)
			->where('route', '=', $type)
			->where_open()
				->where('parent_id', '=', $heading_id)
				->or_where('user_id', '=', $user_id)
			->where_close()
			->order_by('id', 'desc');
		}, 'id');

		$type = ($type == 'procedures') ? 'sop' : $type;
		$target[$type]['class'] = 'button ' . $type;
		if ( !empty ($syschilds) ) {
			foreach ($syschilds as $child) {
				if ($child->user_id == $user_id && $child->status_id == \Model\Heading::S_UNUSED) {
					return $target[$type]['class'] = 'button disabled ' . $type;
				}

			}
		}
		return $target[$type]['class'];
	}


}
