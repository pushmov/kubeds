<?php
namespace Model;

class Msds extends \Model_Crud {

	const S_ACTIVE = 1;
	const S_DISABLED = 0;

	//const QR_FILE_PATH = 'assets/kubeds/msds/';

	protected static $_table_name = 'msds';
	public static $_defaults =	array('id' => null, 'sup_id' => '', 'part_num' => '', 'description' => '', 'url' => '', 'status_id' => self::S_ACTIVE);
	protected static $_rules = array('sup_id' => 'required', 'part_num' => 'required', 'url' => 'required|valid_url');
	protected static $_mass_whitelist =	array('id', 'sup_id', 'part_num', 'description', 'url', 'status_id');
	protected $_status = array(self::S_DISABLED => 'Disabled', self::S_ACTIVE => 'Active');

	public function get_for_user($use_filter=false) {
		$msdsquery = \DB::select('*', static::$_table_name.'.id', static::$_table_name.'.status_id')->from(static::$_table_name)->join('suppliers', 'inner')->on('suppliers.id', '=', 'sup_id');
		if ($use_filter === true) {
			$msdsquery->where(static::$_table_name . '.status_id', '<>', self::S_DISABLED);
		}
		$msds = $msdsquery->execute()->as_array();
		return $msds;
	}

	public function get_status_array($id=null) {
		if ($id === null)
			return $this->_status;
		return array_key_exists($id, $this->_status) ? $this->_status[$id] : $this->_status;
	}

	public function _validation_unique($val) {
		$rows = self::find_by(array(
			'description' => $val
		));
		if ($rows !== null) {
			\Validation::active()->set_message('unique', 'Display title \'' . $val . '\' already exists for this MSDS');
			return false;
		}
		return true;
	}

	public function save($validate = true) {
		//$this->qr = ($this->qr == '') ? $this->url : $this->qr;
		return parent::save();
	}

	public function remove($id) {
		$data = self::find_by_pk($id);
		if ( $data !== null) {
			$data->delete();
		}
		return array('status' => \Model\Base::RES_OK);
	}

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function view($id) {
		$msds = self::find( function($query) use ($id) {
			return $query
				->select(self::$_table_name . '.*', array('s.name', 'supplier_name'), array('s.site_url', 'supplier_url'))
				->join(array(\Model\Supplier::get_table_name(), 's'))
				->on('s.id', '=', self::$_table_name . '.sup_id')
				->where(self::$_table_name . '.id', '=', $id);
		});
		if ($msds !== null) {
			return current($msds)->to_array();
		}
		return self::$_defaults;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$tb = self::$_table_name;
		$status = "(CASE ";
		foreach($this->_status as $val => $name){
			$status .= " WHEN {$tb}.status_id = '{$val}' THEN '{$name}'";
		}
		$status .= " END)";
		$aColumns = array('supplier_name', 'part_num', 'description', 'status', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT {$tb}.id, s.name AS supplier_name, s.site_url AS supplier_url, {$tb}.part_num, {$tb}.description, {$tb}.url, {$status} AS status FROM " . self::$_table_name . " INNER JOIN " . \Model\Supplier::get_table_name() . " s ON s.id = {$tb}.sup_id) AS tmp ";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'WHERE (';
			$sql .= 'supplier_name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR part_num LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR description LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$action = \Html::anchor('#', 'View', array('data-id' => $row['id'], 'class' => 'button small btn-action msds-view'));
			$action .= \Html::anchor('#', 'Edit', array('data-id' => $row['id'], 'class' => 'button small warning btn-action msds-edit'));
			$action .= \Html::anchor('#', 'Delete', array('data-id' => $row['id'], 'class' => 'button small alert btn-action msds-delete'));
			$row['supplier_name'] = \Html::anchor($row['supplier_url'], $row['supplier_name'], array('target' => '_blank'));
			$row['description'] = \Html::anchor($row['url'], $row['description'], array('target' => '_blank'));
			$row['action'] = $action;
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}


}