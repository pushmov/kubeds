<?php
namespace Model\User;

class Product extends \Model_Crud {

	protected static $_table_name = 'user_products';
	protected static $_mass_whitelist =	array('id', 'user_id', 'option_id', 'part_num', 'manufacturer', 'description', 'updated_at', 'updated_by');
	public static $_defaults =	array('id' => null, 'user_id' => null, 'option_id' => null, 'part_num' => '', 'manufacturer' => '', 'description' => '',	'updated_at' => '', 'updated_by' => 0);


	public function get_for_user($user, $group_id = null) {
		// Load default
		$groups = array();
		$defaultquery = \DB::select(static::$_table_name.'.*')->from(static::$_table_name)->where(static::$_table_name.'.user_id', $user->template_id);
		$default = $defaultquery->as_assoc()->execute();
            // Company 1 = template
        if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
			$data = array();
			// Load user specific
			$userquery = \DB::select(static::$_table_name.'.*')->from(static::$_table_name)->where(static::$_table_name.'.user_id', $user->id);
			$product = $userquery->as_assoc()->execute();
			if ($product->count() > 0) {
				$approved = array_merge($data, $product->as_array());
				$groups = $approved;
			} else {
				$groups = $default->as_array();
			}
		} else {
			$groups = $default->as_array();
		}
		return $groups;
	}

	public function delete_for_user($data) {
		\DB::delete(static::$_table_name)->where('user_id', $data['user_id'])->where('option_id', $data['option_id'])->execute();
		return true;
	}

	public function get_for_template($options) {
		$defaultquery = \DB::select()->from(static::$_table_name);
		if ( !empty($options) ) {
			foreach ($options as $option) {
				$in[] = $option['id'];
			}
			$defaultquery->where('option_id', 'in', $in);
		}
		$defaultassoc = $defaultquery->as_assoc()->execute();
		$default = $defaultassoc->as_array('id');
		return $default;
	}

	static function comp($a, $b) {
	    return $a['option_id'] - $b['option_id'];
	}

	public function format($approved) {
		$data = array();
		if (!empty($approved)) {
			foreach ($approved as $row) {
				$data[$row['option_id']][] = $row;
			}
		}
		return $data;
	}
}
