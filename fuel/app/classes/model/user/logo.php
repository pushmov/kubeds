<?php
namespace Model\User;

class Logo extends \Model\Base {

	const LOGO_PATH = 'assets/kubeds/';
	const LOGO_DIR = 'logo/';
	protected static $_table_name = 'user_logos';

	public static $_defaults =	array( 'id' => null, 'user_id' => 0, 'file_name' => null, 'pos' => 1 );

	protected static $_rules = array('file_name' => 'required');
	protected static $_mass_whitelist =	array('id', 'user_id', 'file_name', 'pos');

	public function get_for_user($id) {
		$logo = self::find(array(
			'where' => array('user_id' => $id),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		$data = array();
		if (!empty($logo)) {
			foreach ($logo as $row) {
				$pathinfo = pathinfo(DOCROOT . $row['file_name']);
				if (is_readable( $pathinfo['dirname'] . '/.thumbs/'.$pathinfo['basename'] )) {
					$path = str_replace($pathinfo['basename'], '', $row['file_name']);
					$filename = $path . '.thumbs/'.$pathinfo['basename'];
				} else {
					$filename = $row['file_name'];
				}
				$row['asset_file_name'] = $filename;
				$data[] = $row;
			}
		}
		return $data;
	}

	public function get_last_position($user_id) {
		$row = \DB::query("SELECT MAX(pos) AS cnt FROM " . self::$_table_name . " WHERE user_id = {$user_id}", \DB::SELECT)->execute()->get('cnt');
		if ($row !== null) {
			return $row + 1;
		} else {
			return 1;
		}
	}

	public function delete_thumbnails($data) {
		if (!empty($data)) {
			foreach ($data as $id) {
				$logo = self::find_by_pk($id);
				$this->remove($id);
			}
		}
		return array('status' => 'OK');
	}

	protected function post_delete($result) {
		$data = self::find(array(
			'where' => array('user_id' => $this->user_id),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		if ( !empty($data) ) {
			$pos = 1;
			foreach ($data as $row) {
				$logo = self::find_by_pk($row->id);
				$logo->pos = $pos;
				$logo->save(false);
				$pos++;
			}
		}
		return $result;
	}

}