<?php
namespace Model\support;

class All extends \Model_Crud
{

    const S_DISABLED = 0;
    const S_ACTIVE = 1;

    const TECH_SUPPORT_PATH = 'tech_support/';

    protected static $_table_name = 'support_all';

    public static $_defaults = array('id' => null, 'name' => '', 'url' => '', 'logo' => '', 'status_id' => self::S_ACTIVE);
    protected static $_mass_whitelist = array('id', 'name', 'url', 'logo', 'status_id' );
    protected $_status = array(self::S_DISABLED => 'Disabled', self::S_ACTIVE => 'Active');

    public static function get_table_name()
    {
        return self::$_table_name;
    }

    public function get_status_array($id = null)
    {
        if ($id === null)
            return $this->_status;
        return array_key_exists($id, $this->_status) ? $this->_status[$id] : $this->_status;
    }

    public function load($id = null)
    {
        if ($id === null) {
            return self::$_defaults;
        }
        $data = self::find(function ($query) use ($id) {
            return $query
                ->select(self::$_table_name . '.*', array('s.name', 'support_name'), array('s.site_url', 'support_url'))
                ->join(array(\Model\Supplier::get_table_name(), 's'))
                ->on('s.id', '=', self::$_table_name . '.sup_id')
                ->where(self::$_table_name . '.id', '=', $id);
        });
        return current($data)->to_array();
    }

    protected function delete_logofile()
    {
        $file = DOCROOT . 'assets/kube/' . self::TECH_SUPPORT_PATH . $this->logo;
        if (is_readable($file)) {
            unlink($file);
        }
        return true;
    }

    protected function post_delete($result)
    {
        $this->delete_logofile();
        return $result;
    }


}