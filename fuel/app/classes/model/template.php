<?php
namespace Model;

class Template extends \Model\Base {
	const S_NEW = 0; // New not validated
	const S_ACTIVE = 1;	// Active company
	const S_SYSTEM = 5; // System template
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Account closed, info archived

	const TEMPLATE_DEFAULT = 1;
	const LOGO_DEFAULT = 'kube-logo.png';
	const LOGO_PATH = 'company/';

	protected static $_table_name = 'templates';
	public static $_defaults =	array('id' => null, 'name' => '', 'role_id' => 0, 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'status_id' => 1);
	protected static $_mass_whitelist = array( 'id', 'name', 'role_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status_id');
	protected $_status = array(0 => 'New', 'Active', self::S_SYSTEM => 'System', self::S_HOLD =>  'Password', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');

	public function _validation_unique($val) {
		$rows = self::find_by(array(
			'name' => $val
		));
		if ($rows !== null) {
			\Validation::active()->set_message('unique', 'Template \'' . $val . '\' already exists');
			return false;
		}
		return true;
	}

	public function status_array($id=null) {
		if ($id === null) {
			return $this->_status;
		}
		return array_key_exists($id, $this->_status) ? $this->_status[$id] : $this->_status;
	}

	public function view($id) {
		$templates = self::find( function($query) use ($id) {
			return $query
				->select(self::$_table_name . '.*', array('a1.first_name', 'created_first_name'), array('a1.last_name', 'created_last_name'), array('a2.last_name', 'updated_last_name'), array('a2.first_name', 'updated_first_name'))
				->join(array(\Model\Admin::get_table_name(), 'a1'))
				->on('a1.id', '=', self::$_table_name . '.created_by')
				->join(array(\Model\Admin::get_table_name(), 'a2'))
				->on('a2.id', '=', self::$_table_name . '.updated_by')
				->where(self::$_table_name . '.id', '=', $id);
		});
		if ($templates !== null) {
			return current($templates)->to_array();
		}
		return self::$_defaults;
	}

	public function dropdown($placeholder=false, $placeholder_title='') {
		$data = array();
		if ($placeholder === true) {
			$data[''] = $placeholder_title;
		}
		$templates = self::find_by('status_id', self::S_ACTIVE);
		if($templates !== null) {
			foreach ($templates as $template) {
				$data[$template->id] = $template->name;
			}
		}
		return $data;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$status = "(CASE ";
		foreach($this->_status as $val => $name){
			$status .= " WHEN status_id = '{$val}' THEN '{$name}'";
		}
		$status .= " END)";

		$aColumns = array('id', 'name', 'status', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT id,name,{$status} AS status FROM " . self::$_table_name . ") AS tmp ";

		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'WHERE (';
			$sql .= 'id LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$action = html_tag('button', array('data-id' => $row['id'], 'data-action' => 'view', 'class' => 'button small'), 'View');
			$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'update', 'class' => 'button small warning'), 'Edit');
			$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'logo', 'class' => 'button small success'), 'Add/Edit Logo');
			//$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'delete', 'class' => 'button small alert'), 'Delete');
			$row['action'] = $action;
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function get_logo($user=null) {
		if ($user === null) {
			return self::LOGO_DEFAULT;
		}
		$logos = \Model\User\Logo::find(array(
			'where' => array('user_id' => $user->id),
			'order_by' => array('updated_at' => 'desc')
		));
		if( !empty($logos) ) {
			return 'files/' . basename($logos[0]->file_name);
		}
		$files = glob(DOCROOT .'assets/kubeds/company/'. str_pad($user->template_id, 5, '0', STR_PAD_LEFT) . '.*');
		if( !empty($files) && is_readable(current($files)) ) {
			return self::LOGO_PATH . basename(current($files));
		}
		return self::LOGO_DEFAULT;
	}

	public function get_system_template()
	{
		$data = array();
		$templates = self::find_by_status_id(self::S_SYSTEM);
		if (!empty($templates)) {
			foreach ($templates as $row) {
				$data[] = $row->id;
			}
		}
		return $data;
	}

	public function clone_template($data) {
		//clone step
		$steps = \Model\Step::find_by_template_id($data['clone_id']);
		if ( $steps !== null ) {
			foreach ($steps as $step) {
				$step_clone = \Model\Step::forge();
				$step_clone->set($step->to_array());
				$step_clone->template_id = $data['template_id'];
				$step_clone->id = null;
				$step_clone->save(false);

				//option clone
				$options = \Model\Option::find_by_step_id($step->id);
				if ($options !== null) {
					foreach ($options as $opt) {
						$option_clone = \Model\Option::forge();
						$option_clone->set($opt->to_array());
						$option_clone->id = null;
						$option_clone->step_id = $step_clone->id;
						$option_clone->save(false);

						//content clone
						$contents = \Model\Content::find_by_option_id($opt->id);
						if ($contents !== null) {
							foreach ($contents as $ct) {
								$ct_clone = \Model\Content::forge();
								$ct_clone->set($ct->to_array());
								$ct_clone->id = null;
								$ct_clone->option_id = $option_clone->id;
								$ct_clone->save();
							}
						}

					}
				}
			}
		}
		return true;
	}

	protected function post_delete($result) {
		$users = \Model\User::find_by_template_id($this->id);
		if ($users !== null) {
			foreach ($users as $row) {
				$user = \Model\User::find_by_pk($row->id);
				$user->remove($user->id);
			}
		}
		return $result;
	}
}
