<?php
namespace Model;

class Process extends \Model\Base {

	const TYPE_TEXT = 0;
	const TYPE_HTML = 1;
	const TYPE_IMAGE = 2;

	protected static $_table_name = 'processes';
	protected static $_rules = array('title' => 'required');

	public static $_defaults =	array('id' => null, 'parent_id' => 0, 'user_id' => 0, 'heading_id' => null, 'title' => '', 'content' => '',
		'content_type' => self::TYPE_HTML, 'pos' => 1, 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'status_id' => self::S_SYSTEM);
	protected $_type = array(self::TYPE_TEXT => 'Plain Text', self::TYPE_HTML => 'Html', self::TYPE_IMAGE => 'Image');

	public static function get_table_name() {
		return self::$_table_name;
	}

	/**
	* @override
	*/
	public function get_status_array($id=null) {
		$status = parent::get_status_array($id);
		if ($id !== null) {
			switch ($id) {
				case self::S_SYSTEM: 
					$class = 'secondary';
					break;
				case self::S_UNUSED:
					$class = 'info';
					break;
				case self::S_PARENT:
					$class = 'warning';
					break;
				default: 
					$class = '';
					break;
			}
			$html = '<span class="radius '.$class.' label">'.$status.'</span>';
			return $html;
		}
		return $status;
	}

	public function get_type_array($id = null) {
		if ($id === null) {
			return $this->_type;
		}
		return array_key_exists($id, $this->_type) ? $this->_type[$id] : $this->_type;
	}

	public function get_heading($params) {
		$data = array();
		$processes = self::find( function($query) use ($params) {
			return $query->where('heading_id', '=', $params['heading_id'])
			->where_open()
				->where('user_id', 'in', \Model\User::forge()->user_default_template())
				->or_where('user_id', '=', $params['user_id'])
			->where_close()
			->order_by('pos', 'asc');
		}, 'id');
		if ($processes !== null) {
			foreach ($processes as $id => $process) {
				if ($process->parent_id > 0) {
					unset($processes[$process->parent_id]);
				}
				if ($process->status_id == self::S_UNUSED) {
					unset($processes[$id]);
				}
			}
			return $processes;
		}
		return $data;
	}

	public function get_unused($params) {
		$data = array();
		$processes = self::find(array(
			'where' => array(
				'heading_id' => $params['heading_id'],
				'status_id' => self::S_UNUSED,
				'user_id' => $params['user_id']
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		if ($processes !== null) {
			return $processes;
		}
		return $data;
	}

	public function get_parent_from($parent_id) {
		$data = self::find_one_by('id', $parent_id);
		if ($data !== null) {
			return $data->title;
		}
		return false;
	}

	public function position_number($heading_id) {
		$row = \DB::query("SELECT MAX(pos) AS cnt FROM " . self::$_table_name . " WHERE heading_id = {$heading_id}", \DB::SELECT)->execute()->get('cnt');
		if ($row !== null) {
			$pos = $row + 1;
		} else {
			$pos = 1;
		}
		return $pos;
	}

	public function get_sub_menu($heading_id, $user_id=null) {
		$data = self::find( function($query) use ($heading_id, $user_id) {
			$table = self::$_table_name;
			$query->select( $table . '.id', $table . '.title', $table . '.content', array('h.title', 'heading'), 'h.route', $table.'.parent_id', $table.'.status_id');
			$query->join(array(\Model\Heading::get_table_name(), 'h'));
			$query->on('h.id', '=', $table . '.heading_id');
			$query->where('h.id', '=', $heading_id);
			$query->where_open();
			$query->where($table.'.user_id', 'in', \Model\User::forge()->user_default_template());
			if ($user_id !== null) {
				$query->or_where($table.'.user_id', '=', $user_id);	
			}
			$query->where_close();
			$query->order_by($table . '.pos', 'asc');
			return $query;
		}, 'id');
		if ( $data !== null ) {
			foreach ($data as $id => $row) {
				if (array_key_exists($row->parent_id, $data)) {
					unset($data[$row->parent_id]);
				}
				if ($row->status_id == self::S_UNUSED) {
					unset($data[$row->id]);
				}
			}
		}
		return $data;
	}

	protected function post_update($result) {
		$parent = self::find_by_parent_id($this->id);
		if ($parent !== null) {
			foreach ($parent as $inline) {
				$row = self::find_by_pk($inline->id);
				$row->pos = $this->pos;
				$row->save(false);
			}
		}
		return $result;
	}

}
