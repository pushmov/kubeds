<?php
namespace Model;

class Supplier extends \Model\Base {
	const S_INACTIVE = 0; // New not validated
	const S_ACTIVE = 1;
	const S_ARCHIVE = 9; // Account closed, info archived

	protected static $_table_name = 'suppliers';

	public static $_defaults =	array('id' => null, 'name' => '', 'site_url' => '', 'created_at' => '', 'updated_at' => '',
		'created_by' => 0, 'updated_by' => 0, 'status_id' => 0);
	protected $_status = array(0 => 'Inactive', 'Active', self::S_ARCHIVE => 'Archived');
	protected static $_rules = array('site_url' => 'valid_url');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function _validation_unique($val) {
		if ( self::find_by_name($val) !== null ) {
			\Validation::active()->set_message('unique', 'Name need to be unique');
			return false;
		}
		return true;
	}

	public function view($id) {
		$supplier = self::find( function($query) use ($id) {
			return $query
			->select(self::$_table_name . '.*', array('a1.first_name', 'created_first_name'), array('a1.last_name', 'created_last_name'), array('a2.last_name', 'updated_last_name'), array('a2.first_name', 'updated_first_name'))
			->join(array(\Model\Admin::get_table_name(), 'a1'))
			->on('a1.id', '=', self::$_table_name . '.created_by')
			->join(array(\Model\Admin::get_table_name(), 'a2'))
			->on('a2.id', '=', self::$_table_name . '.updated_by')
			->where(self::$_table_name . '.id', '=', $id);
		});
		if ($supplier !== null) {
			return current($supplier)->to_array();
		}
		return self::$_defaults;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$status = "(CASE ";
		foreach($this->_status as $val => $name){
			$status .= " WHEN status_id = '{$val}' THEN '{$name}'";
		}
		$status .= " END)";

		$aColumns = array('name', 'site_url', 'status', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT id, name, site_url, {$status} AS status FROM " . self::$_table_name . ") AS tmp ";

		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'WHERE (';
			$sql .= 'name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR site_url LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$action = html_tag('button', array('data-id' => $row['id'], 'data-action' => 'view', 'class' => 'button small'), 'View');
			$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'update', 'class' => 'button small warning'), 'Edit');
			//$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'delete', 'class' => 'button small alert'), 'Delete');
			$row['action'] = $action;
			if ($row['site_url'] == '') {
				$row['site_url'] = '';
			} else {
				$row['site_url'] = \Html::anchor($row['site_url'], $row['site_url'], array('target' => '_blank'));
			}
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function filter_dropdown() {
		$data = array();
		$suppliers = self::find(array(
			'order_by' => array(
        		'name' => 'asc')
		));
		if ($suppliers !== null) {
			$data[''] = 'All Suppliers';
			foreach ($suppliers as $row) {
				$data[$row->name] = $row->name;
			}
		}
		return $data;
	}

}
