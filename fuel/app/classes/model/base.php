<?php
namespace Model;

class Base extends \Model_Crud {
	const S_UNUSED = 0; //dont use
	const S_SYSTEM = 1; //system
	const S_PARENT = 2; //use parent content. this could be the system template or custom parent
	const S_CUSTOM = 3; //custom content

	const NICE_DATE_FORMAT = '%b %d, %Y %H:%I:%S';
	const RES_OK = 'OK';
	const RES_FAIL = 'FAIL';

	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected $_status = array(self::S_UNUSED => 'Unused', self::S_SYSTEM => 'System Template', self::S_PARENT => 'Parent Template', self::S_CUSTOM => 'Custom Template');

	public function get_status_array($id=null) {
		if ($id === null)
			return $this->_status;
		return array_key_exists($id, $this->_status) ? $this->_status[$id] : $this->_status;
	}

	public function save($validate = true) {
		return parent::save();
	}

	public function remove($id) {
		$data = self::find_by_pk($id);
		if ( $data !== null) {
			$data->delete();
		}
		return array('status' => self::RES_OK);
	}

	public function get_parent_dropdown() {
		$data = array();
		$records = self::find(array(
			'where' => array(
				array('status_id', '<>', self::S_UNUSED),
				array('status_id', '<>', self::S_PARENT)
			))
		);
		if ($records !== null) {
			foreach ($records as $row) {
				$data[$row->id] = $row->title;
			}
		}
		return $data;
	}

	public function reorder($data = array()) {
		if ( ! empty($data) ) {
			if ( isset($data['user_id']) ) {
				unset($data['user_id']);
			}
			foreach ( $data as $pk => $pos ) {
				$record = self::find_by_pk($pk);
				$record->pos = $pos;
				$record->save(false);
			}
		}
		return array('status' => self::RES_OK, 'msg' => 'Success');
	}

	public function activate($id) {
		$return = array('status' => 'FAIL', 'msg' => 'Failed to activate item');
		if ( ($item = self::find_by_pk($id)) !== null ) {
			$parent = self::find_by_pk($item->parent_id);
			if ($item->parent_id > 0 && $item->status_id == self::S_UNUSED && $item->title == $parent->title) {
				$item->remove($item->id);
			}
			if ($item->parent_id > 0 && $item->status_id == self::S_UNUSED) {
				$item->status_id = self::S_CUSTOM;
				$item->save(false);
			}
			$return['status'] = 'OK';
			$return['msg'] = 'Success';
		}
		return $return;
	}

	public function deactivate($params) {
		$return = array('status' => 'FAIL', 'msg' => 'Failed to deactivate item');
		if ( ($my_item = self::find_by_pk($params['id'])) !== null ) {
			if ($my_item->status_id == self::S_SYSTEM || $my_item->status_id == self::S_PARENT) {
				$item = self::forge()->set($my_item->to_array());
				$item->id = null;
				$item->parent_id = $params['id'];
				$item->user_id = $params['user_id'];
				$item->status_id = self::S_UNUSED;
				$item->save();
			} else {
				$my_item->status_id = self::S_UNUSED;
				$my_item->save(false);
			}
			$return['status'] = 'OK';
			$return['msg'] = 'Success';
		}
		return $return;
	}

	public function revert($id) {
		$response = array('status' => 'FAIL', 'msg' => 'Failed to revert item');
		if ( ($item = self::find_by_pk($id)) !== null ) {
			if ($item->parent_id > 0 && $item->status_id == self::S_CUSTOM) {
				$item->remove($item->id);
			}
			$response['status'] = 'OK';
			$response['msg'] = 'Success';
		}
		return $response;
	}

}