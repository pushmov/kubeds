<?php
namespace Model;

class User extends \Model\Base {
 	const S_TEMPLATE = 0; // Template user
	const S_ACTIVE = 1;
	const S_PASSWORD = 2; // Needs new password
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Account closed, info archived

    const F_PATH = DOCROOT .'assets/kubeds/files/';
    const U_DEFAULT = 1;

	protected static $_table_name = 'users';

	protected static $_mass_whitelist =	array('template_id', 'account', 'email', 'passwrd', 'sales_person', 'division', 'customer', 'token', 'session', 'created_at', 'updated_at', 'created_by', 'updated_by', 'last_login', 'login_hash', 'last_ip', 'status_id');
	public static $_defaults =	array('id' => null, 'template_id' => self::U_DEFAULT, 'account' => null, 'email' => '', 'passwrd' => '', 'sales_person' => '', 'division' =>'', 'customer' => '', 'token' => '','session' => '', 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'last_login' => '', 'last_ip' => '', 'status_id' => 1);
	protected static $_rules = array('customer' => 'required');
	protected static $_labels = array('account' => 'Account ID', 'customer' => 'Company Name');
	protected $_status = array(0 => 'New', 'Active', 'Password', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function generate_token($id) {
		return md5(uniqid($id, true));
	}

	public function login() {
		// Confirm active
		if ($this->status_id != self::S_ACTIVE) {
			return array('status' => 'FAIL', 'msg' => 'Account problem');
		}
		// Record the login
		$this->last_login = date_create()->format('Y-m-d H:i:s');
		$this->last_ip = \Input::ip();
		$this->save(false);
		return array('status' => 'OK');
	}

	public function profile($id) {
		$data = self::find_by_pk($id);
		return $data->to_array();
	}

	protected function post_save($result) {
		//$this->passwrd = \Authlite::instance('auth_user')->hash($this->passwrd);
		$this->save(false);
		$path = self::F_PATH . $this->account;
		if ( !is_readable($path) && !is_dir($path) ) {
			mkdir($path, 0777);
		}
		return $result;
	}

	public function all_users($params) {
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';

		$status = "(CASE ";
		foreach($this->_status as $val => $name){
			$status .= " WHEN users.status_id = '{$val}' THEN '{$name}'";
		}
		$status .= " END)";

		$aColumns = array('account','email','name','login_date', 'status', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT users.id,users.template_id,users.email,users.account,companies.name,users.last_login, DATE_FORMAT(users.last_login, '" . self::NICE_DATE_FORMAT . "') AS login_date, {$status} AS status FROM users INNER JOIN companies ON users.template_id = templates.id) AS tmp ";

		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if ( isset($params['company']) && $params['company'] != '' ) {
			$sql .= 'WHERE template_id = \'' . $params['company'] . '\' ';
			if ( isset($params['search']['value']) && $params['search']['value'] != '' ) {
				$sql .= 'AND ';
			}
		} elseif ( isset($params['search']['value']) && $params['search']['value'] != '' ) {
			$sql .= 'WHERE ';
		}

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= '(';
			$sql .= 'account LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR login_date LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR email LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}

		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		$status = $this->get_status_array();
		foreach ($result->as_array() as $row) {
			//$row['status'] = $status[$row['status_id']];
			$action = \Html::anchor('#', 'View', array('data-id' => $row['id'], 'class' => 'button small btn-action user-view'));
			$action .= \Html::anchor('#', 'Edit', array('data-id' => $row['id'], 'class' => 'button small warning btn-action user-edit'));
			$action .= \Html::anchor('#', 'Delete', array('data-id' => $row['id'], 'class' => 'button small alert btn-action user-delete'));
			$row['action'] = $action;
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function dologin($data){
		$response = array();
		$authlite = \Authlite::instance('auth_user');
		$authlite->logout();
		if ($authlite->login($data['email'], $data['passwrd'], isset($data['remember']))) {
			$status = $authlite->get_user()->status_id;
			switch ($status) {
				case self::S_TEMPLATE:
					$response['error']['message'] = 'No login allowed for this account';
					break;
				case self::S_HOLD:
					$response['error']['message'] = 'Your account was termprarily on hold.';
					break;
				case self::S_ARCHIVE:
					$response['error']['message'] = 'Your account archived';
					break;
				default: //success
					$response['success']['message'] = 'Login Successful';
					break;
			}
		} else {
			$response['error']['message'] = 'Invalid email or password';
		}
		return $response;
	}

	public function is_email_used($data){
		$user = self::find_one_by_email($data['email'], '=');
		if ($user === null) {
			$response['status'] = false;
			$response['success'] = array('message' => 'This email ready to use');
		} else {
			$response['status'] = true;
			$response['error'] = array('message' => 'This email already in use');
		}
		return $response;
	}

	public function forgot_password($post){
		$post = array_map('trim', $post);
		if (($post['email'] == '') && ($post['account'] == '')) {
			return array('status' => 'FAILED', 'msg' => 'Enter your email or account ID');
		}
		if ($post['email'] == '') {
			$user = self::find_one_by_account($post['account']);
		} else {
			$user = self::find_one_by_email($post['email']);
		}
		if ($user !== null) {
			$user->token = \Security::generate_token();
			$user->status_id = self::S_PASSWORD;
			$user->save(false);
			\Emails::forge()->reset_password($user->to_array());
			return array('status' => 'OK', 'msg' => 'An email with reset instructions has been sent');
		}
		return array('status' => 'FAILED', 'msg' => 'Email or account not found.');
	}

	public function reset_password($post){
		$post = array_map('trim', $post);
		// length 8, at least one lowercase letter, at least one uppercase letter, at least one number
		if (!preg_match_all('$\S*(?=\S{4,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$', $post['passwrd'])) {
			return array('status' => 'FAILED', 'msg' => 'Invalid password.');
		}
		if ($post['passwrd2'] !== $post['passwrd']) {
			return array('status' => 'FAILED', 'msg' => 'Passwords do not match.');
		}
		$user = self::find_by_pk($post['id']);
		if ($user !== null) {
			$user->token = '';
			$user->status_id = self::S_ACTIVE;
			$user->passwrd = \Authlite::instance('auth_user')->hash($post['passwrd']);
			$user->save(false);
			return array('status' => 'OK');
		}
		return array('status' => 'FAILED', 'msg' => 'Password reset error.');
	}

	public function _validation_unique_email($val) {
		if (self::find_one_by_email($val)) {
			\Validation::active()->set_message('unique_email', 'Email already exists');
			return false;
		}
		return true;
	}

	public function companies($id = null) {
		$data = array();
		$users = self::find_all();
		if($users !== null) {
			foreach ($users as $user) {
				$data[$user->id] = $user->email;
			}
		}
		return $data;
	}

	protected function pre_delete(&$query) {
		if (array_key_exists($this->id, $this->user_template())) {
			//delete option data for this user
			$options = \Model\Option::find_by(array('user_id' => $this->id));
			if ($options) {
				foreach ($options as $opt) {
					$opt->remove($opt->id);
				}
			}

			//delete content data for this user
			$contents = \Model\Content::find_by(array('user_id' => $this->id));
			if ($contents) {
				foreach ($contents as $ct) {
					$ct->remove($ct->id);
				}
			}
		}
	}

	public function remove($id) {
		$user = self::find_by_pk($id);
		if ( $user !== null) {
			$user->delete();
		}
		return array('status' => 'OK');
	}

	public function user_template() {
		$data = array();
		$users = self::find(function($query){
			return $query->select('users.*', array('users.id', 'id'))
			->join(array('templates', 't'), 'inner')
			->on(self::$_table_name . '.template_id', '=', 't.id')
			->where('t.status_id', '=', \Model\Template::S_SYSTEM);
		});
		if($users !== null) {
			foreach ($users as $user) {
				$data[$user->id] = $user->email;
			}
		}
		return $data;
	}

	public function clone_all($user) {
		//clone step
        $result = \DB::select('id', 'parent_id', 'route', 'title', 'icon', 'pos','status_id')->from('steps')->where('template_id', $user->template_id)->execute();
        foreach ($result as $row) {
            $step_id = $row['id'];
            unset($row['id']);
            $step = \Model\Step::forge()->set($row);
            $step->user_id = $user->id;
            $step->created_by = $step->updated_by = $user->created_by;
            $step->save($row);
            // Clone option
            $result2 = \DB::select('id', 'parent_id', 'type_id', 'title', 'external_url', 'pos','status_id')->from('options')
                ->where('user_id', $user->template_id)->where('step_id', $step_id)->execute();
            foreach ($result2 as $row2) {
                $option_id = $row2['id'];
                unset($row2['id']);
                $option = \Model\Option::forge()->set($row2);
                $option->step_id = $step->id;
                $option->user_id = $user->id;
                $option->created_by = $option->updated_by = $user->created_by;
                $option->save($row);
                // Clone content
                $result3 = \DB::select('title', 'content', 'content_type', 'pos','status_id')->from('content')
                    ->where('user_id', $user->template_id)->where('option_id', $option_id)->execute();
                foreach ($result3 as $row3) {
                    $content = \Model\Content::forge()->set($row3);
                    $content->option_id = $option->id;
                    $content->user_id = $user->id;
                    $content->created_by = $content->updated_by = $user->created_by;
                    $content->save($row);
                }
            }
        }
		return true;
	}

	public static function user_default_template() {
		$data = array();
		$users =  \DB::select('users.id')->from(static::$_table_name)
		->join(array('templates', 't'), 'inner')
		->on('t.id', '=', static::$_table_name.'.template_id')
		->where('t.status_id', '=', \Model\Template::S_SYSTEM)->execute();
		if ( $users->count() > 0) {
			foreach ($users as $row) {
				$data[] = $row['id'];
			}
		}
		return $data;
	}
	
}