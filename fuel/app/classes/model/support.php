<?php
namespace Model;

class Support extends \Model_Crud
{
	const S_DISABLED = 0;
	const S_ACTIVE = 1;

	const TECH_SUPPORT_PATH = 'tech_support/';

	protected static $_table_name = 'support';

	public static $_defaults = array('id' => null, 'option_id' => 0, 'support_id' => 0, 'status_id' => self::S_ACTIVE);
	protected static $_mass_whitelist = array('id', 'sup_id', 'status_id');
	protected $_status = array(self::S_DISABLED => 'Disabled', self::S_ACTIVE => 'Active');

	public static function get_table_name()
	{
		return self::$_table_name;
	}

	public function get_for_user($option_id)
	{
		$query = \DB::select('*', array('support.status_id', 'status_idx'))->from(static::$_table_name);
		$query->where('option_id', $option_id);
		$query->join('support_all', 'inner')->on('support.support_id', '=', 'support_all.id');
		$supports = $query->execute()->as_array();
		return $supports;
	}

	public function checked($support_id, $option_id) {
		$query = \DB::select('*')->from(static::$_table_name);
		$query->where('option_id', $option_id);
		$query->where('support_id', $support_id);
		$data = $query->execute()->as_array();
		return empty ($data) ? false : true;
	}

	public function get_status_array($id = null)
	{
		if ($id === null)
			return $this->_status;
		return array_key_exists($id, $this->_status) ? $this->_status[$id] : $this->_status;
	}

	public function remove($id)
	{
		$data = self::find_by_pk($id);
		if ($data !== null) {
			$data->delete();
		}
		return array('status' => \Model\Base::RES_OK);
	}

	public function load($id = null)
	{
		if ($id === null) {
			return self::$_defaults;
		}
		$data = self::find(function ($query) use ($id) {
			return $query
				->select(self::$_table_name . '.*', array('s.name', 'support_name'), array('s.site_url', 'support_url'))
				->join(array(\Model\Supplier::get_table_name(), 's'))
				->on('s.id', '=', self::$_table_name . '.sup_id')
				->where(self::$_table_name . '.id', '=', $id);
		});
		return current($data)->to_array();
	}

}