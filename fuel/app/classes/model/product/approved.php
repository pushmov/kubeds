<?php
namespace Model\Product;

class Approved extends \Model\Base {

	protected static $_table_name = 'product_approveds';

	protected static $_mass_whitelist =	array('id', 'parent_id', 'user_id', 'product_id', 'group_id', 'comments', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status_id');

	public static $_defaults =	array( 'id' => null, 'parent_id' => 0, 'user_id' => 0, 'product_id' => 0, 'group_id' => 0, 'comments' => null, 'created_at' => null, 'created_by' => 0, 'updated_at' => null, 
		'updated_by' => 0, 'status_id' => self::S_UNUSED );

	protected static $_rules = array('product_id' => 'required');
	protected static $_labels = array('product_id' => 'Product Number');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function get_for_user($user, $group_id = null) {
		$groups = \Model\Product\Group::forge()->get_for_user($user, null, $group_id);
		foreach ($groups as $k => $group) {
			// Load default
			$defaultquery = \DB::select('*', array(static::$_table_name.'.id', 'id'), array(static::$_table_name.'.status_id', 'status_id'), array(static::$_table_name.'.parent_id', 'parent_id'), array('suppliers.name', 'manufacturer'))->from(static::$_table_name)->where(static::$_table_name.'.parent_id', 0)->where(static::$_table_name.'.user_id', 'in', \Model\User::forge()->user_default_template())->where('group_id', '=', $group['id'])
			->join('products', 'INNER')
			->on(static::$_table_name.'.product_id', '=', 'products.id')
			->join('product_groups', 'INNER')
			->on(static::$_table_name.'.group_id', '=', 'product_groups.id')
			->join('suppliers', 'inner')
			->on('suppliers.id', '=', 'products.sup_id');
			$default = $defaultquery->as_assoc()->execute();
			if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
				$data = array();
				// Override with user
				foreach($default as $row) {
					$userquery = \DB::select('*', array(static::$_table_name.'.id', 'id'), array(static::$_table_name.'.status_id', 'status_id'), array(static::$_table_name.'.parent_id', 'parent_id'), array('suppliers.name', 'manufacturer'))->from(static::$_table_name)->where(static::$_table_name.'.parent_id', $row['id'])->where(static::$_table_name.'.user_id', $user->id)->where('group_id', '=', $group['id'])
					->join('products', 'INNER')
					->on(static::$_table_name.'.product_id', '=', 'products.id')
					->join('product_groups', 'INNER')
					->on(static::$_table_name.'.group_id', '=', 'product_groups.id')
					->join('suppliers', 'inner')
					->on('suppliers.id', '=', 'products.sup_id');
					$user = $userquery->as_assoc()->execute();
					if ($user->count() > 0) {
						$data[] = $user->current();
					} else {
						$data[] = $row;
					}
					
					$groups[$k]['approved'] = $data;
				}
				// Load user specific
				$userquery = \DB::select('*', array(static::$_table_name.'.id', 'id'), array(static::$_table_name.'.status_id', 'status_id'), array(static::$_table_name.'.parent_id', 'parent_id'))->from(static::$_table_name)->where(static::$_table_name.'.parent_id', 0)->where(static::$_table_name.'.user_id', $user->id)->where('group_id', '=', $group['id'])
				->join('products', 'INNER')
				->on(static::$_table_name.'.product_id', '=', 'products.id')
				->join('product_groups', 'INNER')
				->on(static::$_table_name.'.group_id', '=', 'product_groups.id');
				$user = $userquery->as_assoc()->execute();
				if ($user->count() > 0) {
					$approved = array_merge($data, $user->as_array());
					$groups[$k]['approved'] = $approved;
				} else {
					$groups[$k]['approved'] = $data;
				}
			} else {
				$groups[$k]['approved'] = $default->as_array();
			}
		}
		return $groups;
	}

	//override
	public function remove($params) {
		//- custom parent : remove
		//- sys template : clone w/ parent_id = id, status_id = custom
		$response = array('status' => self::RES_OK);
		$data = self::find_by_pk($params['id']);
		if ( $data === null ) {
			return $response;
		}
		if ( $data->status_id == self::S_CUSTOM ) {
			$status = parent::remove($params['id']);
			$status['group_id'] = $data->group_id;
			return $status;
		}
		if ( $data->parent_id == 0 && $data->status_id == self::S_SYSTEM ) {

			$groups = \Model\Product\Group::find(function($query) use ($params, $data) {
				$query->where('parent_id', '=', $data->group_id);
				$query->where('user_id', '=', $params['user_id']);
				return $query;
			});

			$product = self::forge()->set($data->to_array());
			$product->id = null;
			$product->parent_id = $data->id;
			$product->group_id = $groups === null ? $data->group_id : current($groups)->id;
			$product->user_id = $params['user_id'];
			$product->status_id = self::S_UNUSED;
			$product->save(false);
			$response['group_id'] = $product->group_id;
		}
		return $response;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$aColumns = array('part_num', 'sup_name', 'description', 'comments', 'action');
		$tbname = self::$_table_name;
		$pk = \Model\Product\Group::find_by_pk($params['group_id']);
		$params['parent_id'] = $pk->parent_id;
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT ".self::$_table_name.".id,".self::$_table_name.".comments,p.part_num,p.description,s.name AS sup_name FROM ".self::$_table_name." INNER JOIN " . \Model\Product::get_table_name() . " p on p.id = {$tbname}.product_id INNER JOIN " . \Model\Product\Group::get_table_name() . " g on g.id = {$tbname}.group_id INNER JOIN ".\Model\Supplier::get_table_name()." s ON s.id = p.sup_id WHERE (" . self::$_table_name . ".group_id = '".$params['group_id']."' OR " . self::$_table_name . ".group_id = '".$params['parent_id']."')) AS tmp ";

		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'WHERE (';
			$sql .= 'part_num LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR sup_name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR description LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR comments LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {

			$row['action'] = \Html::anchor('#', 'Delete', array('data-id' => $row['id'], 'class' => 'button small alert btn-action product-approved-delete'));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function datatable_2($params) {
		$pk = \Model\Product\Group::find_by_pk($params['group_id']);
		$params['parent_id'] = $pk->parent_id;
		$data = self::find( function ($query) use ($params) {
			$query->select(self::$_table_name . '.id', self::$_table_name.'.parent_id', self::$_table_name . '.comments', 'p.part_num', 'p.description', array('s.name', 'sup_name'), self::$_table_name.'.status_id');
			$query->join(array(\Model\Product::get_table_name(), 'p'), 'inner');
			$query->on('p.id', '=', self::$_table_name.'.product_id');
			$query->join(array('product_groups', 'g'), 'inner');
			$query->on('g.id', '=', self::$_table_name . '.group_id');
			$query->join(array(\Model\Supplier::get_table_name(), 's'), 'inner');
			$query->on('s.id', '=', 'p.sup_id');
			$query->where(self::$_table_name . '.group_id', '=', $params['group_id']);
			$query->or_where(self::$_table_name . '.group_id', '=', $params['parent_id']);
			return $query;
		}, 'id');
		if ($data !== null) {
			foreach ($data as $id => $row) {
				if ($row->parent_id > 0) {
					unset($data[$row->parent_id]);
				}
				if ($row->status_id == self::S_UNUSED) {
					unset($data[$id]);
				}
			}
		}
		return $data;
	}

	public function count_product_groups($page_id, $user_id) {
		$data = array();
		$groups = \Model\Product\Group::find( function($query) use ($page_id, $user_id) {
			$query->where('heading_id', '=', $page_id);
			$query->where_open();
				$query->where('user_id', 'in', \Model\User::forge()->user_default_template());
				if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
					$query->or_where('user_id', '=', $user_id);
				}
			$query->where_close();
			$query->where_open();
				$query->where('status_id', '=', \Model\Product\Group::S_SYSTEM);
				$query->or_where('status_id', '=', \Model\Product\Group::S_CUSTOM);
			$query->where_close();
			$query->order_by('pos', 'asc');
			return $query;
		}, 'id');
		
		if ($groups !== null) {
			foreach ($groups as $row) {
				if ($row->parent_id > 0 && array_key_exists($row->parent_id, $groups)) {
					unset($groups[$row->parent_id]);
				}
				$where = function($query) use ($row) {
					$query->where('group_id', '=', $row->id);
					if ($row->parent_id > 0) {
						$query->or_where('group_id', '=', $row->parent_id);
					}
					return $query;
				};
				$records = self::find( function($query) use ($row) {
					$query->where('group_id', '=', $row->id);
					if ($row->parent_id > 0) {
						$query->or_where('group_id', '=', $row->parent_id);
					}
					return $query;
				}, 'id' );
				
				if ($records !== null) {
					foreach ($records as $id => $inline) {
						if ($inline->parent_id > 0) {
							unset($records[$inline->parent_id]);
						}
						if ($inline->status_id == self::S_UNUSED) {
							unset($records[$id]);
						}
					}
				}
				$row->test = count($records);
			}
			return $groups;
		}
		return $data;
	}

	public function group_data($id) {
		$groups = \Model\Product\Group::find( function($query) use ($id) {
			$g = \Model\Product\Group::get_table_name();
			return $query
				->select($g.'.id', $g . '.pos', $g . '.title', array('h.title', 'heading_title'))
				->join(array(\Model\Heading::get_table_name(), 'h'))
				->on('h.id', '=', $g . '.heading_id')
				->where($g . '.id', '=', $id);
		});
		if ($groups !== null) {
			return current($groups)->to_array();
		}

		return \Model\Product\Group::$_defaults;
	}

	public function update_state($post) {
		$response = array('hash' => '#');
		if ( ($product = self::find_by_pk($post['id'])) !== null ) {
			//clone record if parent_id = 0 
			if ( $product->parent_id == 0 && !in_array($post['userid'], \Model\User::user_default_template()) ) {
				$custom = self::forge()->set($product->to_array());
				$custom->id = null;
				$custom->status_id = $post['checked'];
				$custom->parent_id = $product->id;
				$custom->user_id = $post['userid'];
				$custom->save(false);
				$group_id = $custom->group_id;
			} else {
				$product->status_id = $post['checked'];
				$product->save(false);
				$group_id = $product->group_id;
			}
			$response['hash'] = '#href-group-' . $group_id;
		}
		return $response;
	}

}
