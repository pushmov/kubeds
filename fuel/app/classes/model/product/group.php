<?php
namespace Model\Product;

class Group extends \Model\Base {

	const CT_LIST = 0; //list & table
	const CT_HTML = 1; //html

	protected static $_table_name = 'product_groups';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	public static $_defaults =	array('id' => null, 'parent_id' => 0, 'user_id' => 0, 'heading_id' => 0, 'title' => '', 'content' => null, 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'content_type' => self::CT_LIST, 'status_id' => 0);

	protected $_type = array(self::CT_LIST => 'Table List', self::CT_HTML => 'HTML');
	protected static $_rules = array('title' => 'required');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function get_for_user($user, $step=null, $group_id=null) {
		// Load default
		$defaultquery = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $user->template_id);
		if ($step !== null) {
			$defaultquery->where('heading_id', $step);
		}
		if ($group_id !== null) {
			$defaultquery->where('id', $group_id);
		}
		$default = $defaultquery->as_assoc()->execute();
        // Company 1 = template
        if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
			$data = array();
			// Override with user
			foreach($default as $row) {
				$userquery = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->where('user_id', $user->id);
				if ($step !== null) {
					$userquery->where('heading_id', $step);
				}
				if ($group_id !== null) {
					$userquery->where('id', $group_id);
				}
				$group = $userquery->as_assoc()->execute();
				if ($group->count() > 0) {
					$data[] = $group->current();
				} else {
					$data[] = $row;
				}
			}
			// Load user specific
			$userquery = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $user->id);
			if ($step !== null) {
				$userquery->where('heading_id', $step);
			}
			if ($group_id !== null) {
				$userquery->where('id', $group_id);
			}
            $group = $userquery->as_assoc()->execute();
			if ($group->count() > 0) {
				return array_merge($data, $group->as_array());
			}
			return $data;
		}
		return $default->as_array();
	}

	/**
	* @override
	*/
	public function get_status_array($id=null) {
		$status = parent::get_status_array($id);
		if ($id !== null) {
			switch ($id) {
				case self::S_SYSTEM: 
					$class = 'secondary';
					break;
				case self::S_UNUSED:
					$class = 'info';
					break;
				case self::S_PARENT:
					$class = 'warning';
					break;
				default: 
					$class = '';
					break;
			}
			$html = '<span class="radius '.$class.' label">'.$status.'</span>';
			return $html;
		}
		return $status;
	}

	public function get_active($params) {
		$data = self::find( function($query) use ($params) {
			$query->where('heading_id', '=', $params['heading_id']);
			$query->where_open();
			$query->where('status_id', '=', self::S_SYSTEM);
			$query->or_where('status_id', '=', self::S_CUSTOM);
			$query->or_where('user_id', '=', $params['user_id']);
			$query->where_close();
			$query->order_by('pos', 'asc');
			return $query;
		}, 'id');
		if ($data !== null) {
			foreach ($data as $id => $row) {
				if ($row->parent_id > 0 && array_key_exists($row->parent_id, $data)) {
					unset($data[$row->parent_id]);
				}
				if ($row->status_id == self::S_UNUSED) {
					unset($data[$row->id]);
				}
			}
		}
		return $data;
	}

	public function get_unused($params) {
		$data = array();
		$groups = self::find(array(
			'where' => array(
				'heading_id' => $params['heading_id'],
				'status_id' => self::S_UNUSED,
				'user_id' => $params['user_id']
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		if ($groups !== null) {
			return $groups;
		}
		return $data;
	}

	public function _validation_unique($val) {
		$rows = self::find_by(array(
			'heading_id' => \Validation::active()->input('heading_id'),
			'title' => $val
		));
		if ($rows !== null) {
			\Validation::active()->set_message('unique', 'Product group title \''.$val.'\' already exists');
			return false;
		}
		return true;
	}

	public function get_type_array($id = null) {
		if ($id === null) {
			return $this->_type;
		}
		return array_key_exists($id, $this->_type) ? $this->_type[$id] : $this->_type;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$type = "(CASE ";
		foreach($this->_type as $val => $name){
			$type .= " WHEN content_type = '{$val}' THEN '{$name}'";
		}
		$type .= " END)";

		$aColumns = array('id', 'heading_title', 'title', 'created_at', 'updated_at', 'created_by_email', 'updated_by_email', 'content');
		$tbname = self::$_table_name;
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT {$tbname}.id, h1.title AS heading_title, {$tbname}.title, {$tbname}.created_at, {$tbname}.updated_at, {$tbname}.created_by, a1.email as created_by_email, {$tbname}.updated_by, a2.email as updated_by_email, {$type} AS content FROM ".self::$_table_name." INNER JOIN " . \Model\Admin::get_table_name() . " a1 on a1.id = {$tbname}.created_by INNER JOIN " . \Model\Admin::get_table_name() . " a2 on a2.id = {$tbname}.updated_by INNER JOIN " . \Model\Heading::get_table_name() . " h1 ON h1.id = ".self::$_table_name.".heading_id) AS tmp ";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'WHERE (';
			$sql .= 'title LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR heading_title LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR created_at LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR updated_at LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR created_by_email LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR updated_by_email LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR content LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$row['DT_RowId'] = $row['id'];
			$action = \Html::anchor('#', 'Edit', array('data-id' => $row['id'], 'class' => 'button small warning btn-action product-group-edit'));
			$action .= \Html::anchor('#', 'Delete', array('data-id' => $row['id'], 'class' => 'button small alert btn-action product-group-delete'));
			$data['data'][] = array($row['title'], $row['heading_title'], $row['created_at'], $row['updated_at'], $row['created_by_email'], $row['updated_by_email'], $row['content'], $action);
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function dropdown() {
		$data = array();
		$groups = self::find(function ($query) {
			return $query
			->select(self::$_table_name . '.*', array('h.title', 'heading_name'))
			->join(array(\Model\Heading::get_table_name(), 'h'))
			->on('h.id', '=', self::$_table_name . '.heading_id')
			->where(self::$_table_name . '.content_type', self::CT_LIST);
		});
		if ($groups !== null) {
			foreach ($groups as $row) {
				$data[$row->id] = $row->heading_name . ' - ' . $row->title;
			}
		}
		return $data;
	}

	public function position_number($heading_id) {
		$row = \DB::query("SELECT MAX(pos) AS cnt FROM " . self::$_table_name . " WHERE heading_id = {$heading_id}", \DB::SELECT)->execute()->get('cnt');
		if ($row !== null) {
			$pos = $row + 1;
		} else {
			$pos = 1;
		}
		return $pos;
	}

	public function filter_dropdown() {
		$data = array();
		$groups = self::find_all();
		if ($groups !== null) {
			$data[''] = 'All Groups';
			foreach ($groups as $row) {
				$data[$row->title] = $row->title;
			}
		}
		return $data;
	}

	public function get_sub_menu($route, $user_id) {
		$data = self::find( function($query) use ($route, $user_id) {
			$table = self::$_table_name;
			return $query->select( $table . '.id', $table . '.title', $table . '.content', array('h.title', 'heading'), 'h.route', $table.'.parent_id', $table.'.status_id')
				->join(array(\Model\Heading::get_table_name(), 'h'))
				->on('h.id', '=', $table . '.heading_id')
				->where('h.route', '=', $route)
				->where_open()
					->where($table . '.status_id', '=', self::S_SYSTEM)
					->or_where($table . '.status_id', '=', self::S_CUSTOM)
					->or_where($table . '.user_id', '=', $user_id)
				->where_close()
				->order_by($table . '.pos', 'asc');
		}, 'id');
		if ($data !== null) {
			foreach ($data as $id => $row) {
				if ($row->parent_id > 0 && array_key_exists($row->parent_id, $data)) {
					unset($data[$row->parent_id]);
				}
				if ($row->status_id == self::S_UNUSED) {
					unset($data[$id]);
				}
			}
		}
		return $data;
	}

	protected function post_delete($result) {
		$groups = self::find(array(
			'where' => array(
				'heading_id' => $this->heading_id,
				array('pos', '>', $this->pos)
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		$pos = $this->pos;
		if ($groups !== null) {
			foreach ($groups as $row) {
				$group = self::find_by_pk($row->id);
				$group->pos = $pos;
				$group->save(false);
				$pos ++;
			}
		}
		return $result;
	}

	protected function post_update($result) {
		$parent = self::find_by_parent_id($this->id);
		if ($parent !== null) {
			foreach ($parent as $inline) {
				$row = self::find_by_pk($inline->id);
				$row->pos = $this->pos;
				$row->save(false);
			}
		}
		return $result;
	}


}
