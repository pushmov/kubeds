<?php
namespace Model;

class Step extends \Model\Base {
	const S_BODY = 4;
	const S_PAINT = 5;
	const S_DETAIL = 7;
	protected static $_table_name = 'steps';
	public static $_defaults =	array('id' => null, 'template_id' => 0, 'parent_id' => 0, 'user_id' => null, 'route' => '', 'title' => '', 'icon' => '', 'pos' => 1, 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'status_id' => 0);
	protected static $_mass_whitelist =	array('id', 'template_id', 'parent_id', 'user_id', 'route', 'title', 'icon', 'pos', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status_id');

	protected static $_rules = array('user_id' => 'required', 'title' => 'required');
	protected $_step_name = array( self::S_BODY => 'Body', self::S_PAINT => 'Paint', self::S_DETAIL => 'Detail');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function get_step_id($name = null, $user_id) {
		//$step = array_flip($this->_step_name);
		$step = self::find_one_by(function($query) use ($name, $user_id){
			return $query->where('title', 'like', $name.'%')
			->where('user_id', $user_id);
		});
		if ( $step === null ) {
			return $step;
		}
		return $step->id;
	}

	public function get_for_user($user) {
		// Load default
		$template = \Model\Template::find_by_pk($user->template_id);
		
		$query = \DB::select()->from(static::$_table_name)->where('parent_id', 0);
		if ($template->status_id == \Model\Template::S_SYSTEM) {
			$query->where('user_id', $user->id);
		} else {
			$query->where('user_id', $user->template_id);
		}
		$query->where('template_id', $user->template_id);
		$default = $query->order_by('pos')->as_assoc()->execute();
		
		// Template 1 = template
		if (! in_array($user->template_id, \Model\Template::forge()->get_system_template()) ) {
			$data = array();
			// Override with user
			foreach($default as $row) {
				$step = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->where('user_id', $user->id)->as_assoc()->execute();
				if ($step->count() > 0) {
					$data[] = $step->current();
				} else {
					$data[] = $row;
				}
			}
			// Load user specific
            $step = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $user->id)->as_assoc()->execute();
			if ($step->count() > 0) {
				return array_merge($data, $step->as_array());
			}
			return $data;
		}
		return $default->as_array();
	}

	public function get_for_template($template_id) {
		$query = \DB::select()->from(static::$_table_name);
		$query->where('parent_id', 0);
		$query->where('template_id', $template_id);
		$default = $query->order_by('pos')->as_assoc()->execute();
		// Template 1 = template
		if (! in_array($template_id, \Model\Template::forge()->get_system_template()) ) {
			$data = array();
			// Override with user
			foreach($default as $row) {
				$step = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->where('template_id', $template_id)->as_assoc()->execute();
				if ($step->count() > 0) {
					$data[] = $step->current();
				} else {
					$data[] = $row;
				}
			}
			return $data;
		}
		return $default->as_array();
	}

	/**
	* @override
	*/
	public function get_status_array($id=null) {
		$status = parent::get_status_array($id);
		if ($id !== null) {
			switch ($id) {
				case self::S_SYSTEM:
					$class = 'secondary';
					break;
				case self::S_UNUSED:
					$class = 'info';
					break;
				case self::S_PARENT:
					$class = 'warning';
					break;
				default:
					$class = '';
					break;
			}
			$html = '<span class="radius '.$class.' label">'.$status.'</span>';
			return $html;
		}
		return $status;
	}

	public function _validation_unique($val) {
		$rows = self::find_by(array(
			'user_id' => \Validation::active()->input('user_id'),
			'parent_id' => \Validation::active()->input('parent_id'),
			'route' => $val
		));
		if ($rows !== null) {
			\Validation::active()->set_message('unique', 'URL Route already exists for this template');
			return false;
		}
		return true;
	}

	public function view($id = null) {
		$heading = self::find( function($query) use ($id) {
			return $query
			->select(self::$_table_name . '.*', array('u1.name', 'created_name'), array('u2.name', 'updated_name'))
			->join(array(\Model\User::get_table_name(), 'u1'))
			->on('u1.id', '=', self::$_table_name . '.created_by')
			->join(array(\Model\User::get_table_name(), 'u2'))
			->on('u2.id', '=', self::$_table_name . '.updated_by')
			->where(self::$_table_name . '.id', '=', $id);
		});
		if ($heading !== null) {
			return current($heading)->to_array();
		}
		return self::$_defaults;
	}

	public function get_dropdown($user_id = null) {
		$data = array();
		$headings = self::find(array(
			'where' => array('parent_id' => 0),
			'order_by' => array(
				'pos' => 'asc'
			)
			), 'id');

		//override user_id
		if ($user_id !== null && !in_array($user_id, \Model\User::forge()->user_default_template())) {
			$user_headings = self::find(array(
				'where' => array(
					'user_id' => $user_id
				),
				'order_by' => array(
					'pos' => 'asc'
				)
			));
			if ($user_headings !== null) {
				foreach ($user_headings as $h) {
					if (array_key_exists($h->parent_id, $headings)) {
						$headings[$h->parent_id] = $h;
					}
				}
			}
		}
		if ($headings !== null) {
			foreach ($headings as $id => $row) {
				$data[$id] = $row->title;
			}
		}
		return $data;
	}

	private function navigation_child($heading_id, $type, $user_id) {
		$nav_process['attr'] = array('class' => 'button');
		$nav_process['label'] = ucwords($type);
		$nav_process['route'] = '/'.strtolower($type);
		$nav_process['child'] = self::find( function($query) use ($heading_id, $type, $user_id) {
			$query->select('*', array('h1.title', 'title_1'), array('h1.status_id', 'status_1'));
			$query->join(array('headings', 'h1'), 'left');
			$query->on('headings.id', '=', 'h1.parent_id');
			$query->where_open();
			$query->where('headings.id', '=', $heading_id);
			$query->or_where('headings.parent_id', '=', $heading_id);
			$query->where_close();
			$query->where('headings.route', '=', strtolower($type));
			$query->where_open();
			$query->where('headings.user_id', 'in', \Model\User::forge()->user_default_template());
			if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
				$query->or_where('headings.user_id', '=', $user_id);
			}
			$query->where_close();
			return $query;
		});

		if ($nav_process['child'] !== null && current($nav_process['child'])->status_1 !== null && current($nav_process['child'])->status_1 == self::S_UNUSED) {
			$nav_process['attr']['disabled'] = 'disabled';
			$nav_process['label'] = current($nav_process['child'])->title_1 === null ? current($nav_process['child'])->title : current($nav_process['child'])->title_1;
		}
		if ($type == 'products') {
			$nav_process['attr']['class'] .= ' warning';
		}
		if ($type == 'process') {
			$nav_process['attr']['class'] .= ' info';
		}
		return $nav_process;
	}

	public function get_navigation($route, $module, $user_id) {
		$heading = self::find( function($query) use ($route, $user_id) {
			$query->where('route', '=', $route);
			$query->where('status_id', '<>', self::S_UNUSED);
			$query->where_open();
			$query->where('user_id', 'in', \Model\User::forge()->user_default_template());
			if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
				$query->or_where('user_id', '=', $user_id);
			}
			$query->where_close();
			return $query;
		});
		$heading = count($heading) > 1 ? end($heading) : current($heading);

		$nav_process = $this->navigation_child($heading->id, 'process', $user_id);
		$nav_procedures = $this->navigation_child($heading->id, 'procedures', $user_id);
		$nav_products = $this->navigation_child($heading->id, 'products', $user_id);
		$nav['home'] 		= \Html::anchor(\Uri::create('home'), 'Home', array('class' => 'button'));
		$nav['process'] 	= \Html::anchor(\Uri::create($heading->route . $nav_process['route']), $nav_process['label'], $nav_process['attr']);
		$nav['procedures'] 	= \Html::anchor(\Uri::create($heading->route . $nav_procedures['route']), $heading->title . ' SOPs', $nav_procedures['attr']);
		$nav['products'] 	= \Html::anchor(\Uri::create($heading->route . $nav_products['route']), $nav_products['label'], $nav_products['attr']);
		if (array_key_exists($module, $nav)) {
			unset($nav[$module]);
		}

		$process_count = \Model\Process::count('id', true, array('heading_id' => $heading->id));
		$procedures_count = \Model\Procedure::count('id', true, array('heading_id' => $heading->id));
		$products_count = \Model\Product\Group::count('id', true, array('heading_id' => $heading->id));

		if ( $process_count <= 0 ) {
			unset($nav['process']);
		}

		if ( $procedures_count <= 0 ) {
			unset($nav['procedures']);
		}

		if ( $products_count <= 0 ) {
			unset($nav['products']);
		}

		/** SPECIAL NAV **/
		//msds nav
		//fix me (?)
		if ( $process_count > 0 && $procedures_count > 0 && $heading->id == \Modules::SYS_BODYSHOP && \Modules::get_module_num($module) != \Modules::MOD_PRODUCTS ) {
			$nav['msds'] = \Html::anchor(\Uri::create($heading->route . '/msds'), 'MSDS', array('class' => 'button alert'));
		}

		//support nav
		if ( $process_count > 0 && $procedures_count > 0 && $products_count > 0 && \Modules::get_module_num($module) != \Modules::MOD_PRODUCTS ) {
			$nav['support']	= \Html::anchor(\Uri::create($heading->route . '/supports'), 'Technical Support', array('class' => 'button alert'));
		}

		//fittest
		if ( $process_count > 0 && $procedures_count > 0 && $heading->id == \Modules::SYS_PAINTSHOP && \Modules::get_module_num($module) != \Modules::MOD_PRODUCTS ) {
			$nav['fittest']	= \Html::anchor(\Uri::create($heading->route . '/fittest'), 'Fit Test', array('class' => 'button alert'));
		}

		$data['nav'] = join('', $nav);
		$data['title'] = $heading->title;
		return $data;
	}

	public function system_template() {
		$data = self::find(array(
			'where' => array(
				'status_id' => self::S_SYSTEM
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		return $data;
	}

	public function user_active_heading_template($user_id) {
		$active = self::find( function($query) use ($user_id) {
			return $query
			->where_open()
			->where('status_id', '=', self::S_SYSTEM)
			->or_where('status_id', '=', self::S_PARENT)
			->where_close()
			->or_where('user_id', '=', $user_id)
			->order_by('pos', 'asc');
			}, 'id');
		if ( !empty($active) ) {
			foreach ($active as $id => $inline) {
				if ($inline->parent_id > 0 && $inline->status_id != self::S_PARENT) {
					unset($active[$inline->parent_id]);
				}
				if ($inline->status_id == self::S_UNUSED || $inline->status_id == self::S_PARENT) {
					unset($active[$id]);
				}
				if ($inline->status_id == self::S_PARENT && in_array($inline->user_id, \Model\User::forge()->user_default_template())) {
					unset($active[$inline->id]);
				}
				$child_heading = self::find( function($query) use ($user_id, $inline) {
					$query
					->where_open()
					->where_open()
					->where('status_id', '=', self::S_PARENT)
					->where('parent_id', '=', $inline->id)
					->where_close();
					if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
						$query->or_where('user_id', '=', $user_id);
					}
					$query->where_close()
					->where('pos', '=', $inline->pos)
					->where('parent_id', '<>', 0)
					->where('status_id', '<>', self::S_CUSTOM)
					->order_by('pos', 'asc');
					return $query;
					}, 'id');
				if (!empty($child_heading)) {
					foreach ($child_heading as $id => $row) {
						if ($row->status_id == self::S_UNUSED) {
							unset($child_heading[$row->parent_id]);
							unset($child_heading[$id]);
						}
					}
				}
				$inline->child_sql = \DB::last_query();
				$inline->child = ($child_heading === null) ? array() : $child_heading;
			}
		}
		return $active;
	}

	public function user_unused_heading_template($id) {
		$data = self::find(array(
			'where' => array(
				'status_id' => self::S_UNUSED,
				'user_id' => $id
			),
			'order_by' => array(
				'parent_id' => 'asc'
			)
		));
		return $data;
	}

	public function find_top_parent($id) {
		$found = false;
		while (!$found) {
			$row = self::find_by_pk($id);
			if ($row->parent_id > 0) {
				$id = $row->parent_id;
				$found = false;
			} else {
				break;
			}
		}
		return $row;
	}

	public function parent_level($id) {
		$i = 0;
		$found = false;
		while (!$found) {
			$row = self::find_by_pk($id);
			if ($row->parent_id > 0) {
				$id = $row->parent_id;
				$found = false;
			} else {
				$found = true;
			}
			$i++;
		}
		return $i;
	}

	protected function pre_save(&$query) {
		$row = \DB::query("SELECT MAX(pos) AS cnt FROM " . self::$_table_name . " WHERE user_id = {$this->user_id}", \DB::SELECT)->execute()->get('cnt');
		if ($row !== null) {
			$pos = $row + 1;
		} else {
			$pos = 1;
		}
		$this->pos = $pos;
	}

	protected function post_update($result) {
		//update position for all childrens based on top parent pos
		$childs = self::find_by_parent_id($this->id);
		if ( $childs !== null ) {
			foreach ($childs as $c) {
				$row = self::find_by_pk($c->id);
				$row->pos = $this->pos;
				$row->save(false);

				$newchilds = self::find_by_parent_id($c->id);
				if ($newchilds !== null) {
					foreach ($newchilds as $n) {
						$nrow = self::find_by_pk($n->id);
						$nrow->pos = $this->pos;
						$nrow->save(false);
					}
				}
			}
		}
		return $result;
	}

	protected function post_delete($result) {
		$headings = self::find(array(
			'where' => array(
				'user_id' => $this->user_id,
				'parent_id' => 0,
				array('pos', '>', $this->pos)
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		$pos = $this->pos;
		if ($headings !== null) {
			foreach ($headings as $row) {
				$heading = self::find_by_pk($row->id);
				$heading->pos = $pos;
				$heading->save(false);
				$pos ++;
			}
		}
		return $result;
	}

}
