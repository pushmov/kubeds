<?php
namespace Model;

class Admin extends \Model_Crud
{
	const S_NEW = 0;
	const S_ACTIVE = 1;
	const S_INACTIVE = 2;
	const S_PASSWORD = 3;
	const S_HOLD = 4;
	const S_ARCHIVE = 5;

	protected static $_table_name = 'admins';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';

	protected static $_mass_whitelist = array(
		'id', 'email', 'passwrd', 'first_name', 'last_name', 'session', 'created_at', 'last_login', 'session', 'role_id', 'status_id'
	);
	protected static $_defaults = array(
		'id' => null, 'email' => '', 'passwrd' => '', 'first_name' => '', 'last_name' => '', 'created_at' => '', 'last_login' => '', 'session' => '', 'role_id' => 0, 'status_id' => 1
	);
	protected static $_rules = array('passwrd' => 'required');
	protected static $_labels = array('email' => 'Email', 'passwrd' => 'Password');
	protected $_status = array(self::S_NEW => 'New', self::S_ACTIVE => 'Active', self::S_PASSWORD => 'Password', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');

	protected $_roles = array(0 => 'Super Admin', 10 => 'Kube DS Admin', 20 => 'Kube IM Admin');

	public static function get_table_name()
	{
		return self::$_table_name;
	}

	public function _validation_unique_email($val)
	{
		if (self::find_one_by_email($val)) {
			\Validation::active()->set_message('unique_email', 'Email already exists');
			return false;
		}
		return true;
	}

	public function get_role($id = null)
	{
		if ($id === null) {
			return $this->_roles;
		}
		if (isset($this->_roles[$id])) {
			return $this->_roles[$id];
		}
		return '';
	}

	public function get_status($id = null)
	{
		if ($id === null) {
			return $this->_status;
		}
		if (isset($this->_status[$id])) {
			return $this->_status[$id];
		}
		return '';
	}

	public function get_datatable_list($admin)
	{
		$data = array();
		$users = $this::find_all();

		foreach ($users as $row) {
			$row = $row->to_array();
			$row['name'] = "{$row['first_name']} {$row['last_name']}";
			$row['role'] = $this->get_role($row['role_id']);
			//$row['date'] = date_create($row['last_login'])->format('Y-m-d H:i:s');
			try {
				$row['date'] = \Date::create_from_string($row['last_login'], 'mysql')->format('us_named');
			} catch (\UnexpectedValueException $e) {
				$row['date'] = '';
			}
			$row['action'] = \Html::anchor('#', $this->get_status($row['status_id']), array('title' => 'Click to edit admin', 'id' => $row['id'], 'class' => 'admin-edit'));
			if ($admin->role_id != 0) {
				$row['action'] = $this->get_status($row['status_id']);
			}
			$data[] = $row;
		}
		return $data;
	}

	public function save($validation = null)
	{
		if (empty($this->_data['id'])) {
			//$this->date_created = date_create()->format('Y-m-d H:i:s');
		}
		$this->email = strtolower($this->email);

		// Don't save the password
		/*
		if (($this->passwrd == '') || (strlen($this->passwrd) == 32)) {
			unset($this->_data['passwrd']);
		} else {
			$this->passwrd = \Authlite::instance('auth_admin')->hash($this->passwrd);
		}
		 */
		parent::save();
	}

	public function full_name()
	{
		return $this->first_name . ' ' . $this->last_name;
	}
}