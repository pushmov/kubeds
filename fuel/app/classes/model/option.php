<?php
namespace Model;

class Option extends \Model\Base {
	const T_PROCESS = 0;
	const T_SOP = 1;
	const T_PRODUCT = 2;
	const T_MSDS = 3;
	const T_SUPPORT = 4;

	protected static $_table_name = 'options';
	public static $_defaults =	array('id' => null, 'parent_id' => 0, 'user_id' => null, 'step_id' => null, 'type_id' => 0, 'title' => '', 'external_url' => null,
		'pos' => 1, 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'status_id' => 1);

	protected static $_rules = array('user_id' => 'required', 'title' => 'required', 'external_url' => 'valid_url');
	protected $_type = array(self::T_PROCESS => 'Process', self::T_SOP => 'SOP', self::T_PRODUCT => 'Product', self::T_MSDS => 'MSDS', self::T_SUPPORT => 'Support');

	protected $_button_class = array(self::T_PROCESS => 'btn-process', self::T_SOP => 'btn-sop', self::T_PRODUCT => 'btn-product', self::T_MSDS => 'btn-msds', self::T_SUPPORT => 'btn-support');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function get_button_class($id = null) {
		if ($id === null) {
			return $this->_button_class;
		}
		return array_key_exists($id, $this->_button_class) ? $this->_button_class[$id] : $this->_button_class;
	}

	public function get_type_array($id = null) {
		if ($id === null) {
			return $this->_type;
		}
		return array_key_exists($id, $this->_type) ? $this->_type[$id] : $this->_type;
	}

	public function get_for_user($user, $step=null, $option=null) {
		// Load default
		$defaultquery = \DB::select()->from(static::$_table_name);
		$defaultquery->where_open();
			$defaultquery->where('user_id', $user->template_id);
			$defaultquery->or_where('user_id', $user->id);
		$defaultquery->where_close();
		if ( $step !== null ) {
			$defaultquery->where('step_id', $step);
		}
		if ( $option !== null ) {
			$defaultquery->where('id', '<>', $option);
		}
		$defaultquery->order_by('pos', 'asc');
		$defaultassoc = $defaultquery->as_assoc()->execute();
		$default = $defaultassoc->as_array('id');
        // Template 1 = template
        if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
			$data = array();
			// Override with user
			foreach($default as $key => $row) {
				$userquery = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->where('user_id', $user->id);
				if ( $step !== null ) {
					$userquery->where('step_id', $step);
				}

				if ( $option !== null ) {
					$userquery->where('id', '<>', $option);
				}
				$userquery->order_by('pos', 'asc');
				$userassoc = $userquery->as_assoc()->execute();
				if ($userassoc->count() > 0) {
                    if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
                        $current = $userassoc->current();
						$data[$key] = $current;
					}
				} else {
					if (($row['parent_id'] == 0 && $row['status_id'] == self::S_CUSTOM) === false) {
						$data[$key] = $row;
					}
				}
			}
			// Load user specific
			$userquery = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $user->id);
			if ( $step !== null ) {
				$userquery->where('step_id', $step);
			}
			if ( $option !== null ) {
				$userquery->where('id', '<>', $option);
			}
			$userquery->order_by('pos', 'asc');
			$options = $userquery->as_assoc()->execute();
			
			if ($options->count() > 0) {
				$userdata = $options->as_array('id');
				//$data = array_merge($data, $userdata);
				$merge = $data + $userdata;
				$data = $merge;
			}
			//check if row duplicate based on key array
			if (count($data) > 0) {
				foreach ($data as $key => $row) {
					if (array_key_exists($row['parent_id'], $data)) {
						unset($data[$row['parent_id']]);
					}
				}
			}
			return $data;
		}
		return $default;
	}

	public function get_for_template($template_id, $step=null, $option=null) {
		// Load default
		$defaultquery = \DB::select()->from(static::$_table_name);
		if ( $step !== null ) {
			$defaultquery->where('step_id', $step);
		}
		if ( $option !== null ) {
			$defaultquery->where('id', '<>', $option);
		}
		$defaultquery->order_by('pos', 'asc');
		$defaultassoc = $defaultquery->as_assoc()->execute();
		$default = $defaultassoc->as_array('id');
        // Template 1 = template
        if (!in_array($template_id, \Model\Template::forge()->get_system_template())) {
			$data = array();
			// Override with user
			foreach($default as $key => $row) {
				$userquery = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id']);
				if ( $step !== null ) {
					$userquery->where('step_id', $step);
				}

				if ( $option !== null ) {
					$userquery->where('id', '<>', $option);
				}
				$userquery->order_by('pos', 'asc');
				$userassoc = $userquery->as_assoc()->execute();
				if ($userassoc->count() > 0) {
                    if (!in_array($template_id, \Model\Template::forge()->get_system_template())) {
                        $current = $userassoc->current();
						$data[$key] = $current;
					}
				} else {
					if (($row['parent_id'] == 0 && $row['status_id'] == self::S_CUSTOM) === false) {
						$data[$key] = $row;
					}
				}
			}
			// Load user specific
			$userquery = \DB::select()->from(static::$_table_name)->where('parent_id', 0);
			if ( $step !== null ) {
				$userquery->where('step_id', $step);
			}
			if ( $option !== null ) {
				$userquery->where('id', '<>', $option);
			}
			$userquery->order_by('pos', 'asc');
			$options = $userquery->as_assoc()->execute();
			
			if ($options->count() > 0) {
				$userdata = $options->as_array('id');
				//$data = array_merge($data, $userdata);
				$merge = $data + $userdata;
				$data = $merge;
			}
			//check if row duplicate based on key array
			if (count($data) > 0) {
				foreach ($data as $key => $row) {
					if (array_key_exists($row['parent_id'], $data)) {
						unset($data[$row['parent_id']]);
					}
				}
			}
			return $data;
		}
		return $default;
	}

	/**
	* @override
	*/
	public function get_status_array($id=null) {
		$status = parent::get_status_array($id);
		if ($id !== null) {
			switch ($id) {
				case self::S_SYSTEM:
					$class = 'secondary';
					break;
				case self::S_UNUSED:
					$class = 'info';
					break;
				case self::S_PARENT:
					$class = 'warning';
					break;
				default:
					$class = '';
					break;
			}
			$html = '<span class="radius '.$class.' label">'.$status.'</span>';
			return $html;
		}
		return $status;
	}

	public function _validation_unique($val) {
		$rows = self::find_by(array(
			'user_id' => \Validation::active()->input('user_id'),
			'parent_id' => \Validation::active()->input('parent_id'),
			'route' => $val
		));
		if ($rows !== null) {
			\Validation::active()->set_message('unique', 'URL Route already exists for this template');
			return false;
		}
		return true;
	}

	public function view($id = null) {
		$heading = self::find( function($query) use ($id) {
			return $query
			->select(self::$_table_name . '.*', array('u1.name', 'created_name'), array('u2.name', 'updated_name'))
			->join(array(\Model\User::get_table_name(), 'u1'))
			->on('u1.id', '=', self::$_table_name . '.created_by')
			->join(array(\Model\User::get_table_name(), 'u2'))
			->on('u2.id', '=', self::$_table_name . '.updated_by')
			->where(self::$_table_name . '.id', '=', $id);
		});
		if ($heading !== null) {
			return current($heading)->to_array();
		}
		return self::$_defaults;
	}

	public function get_dropdown($user_id = null) {
		$data = array();
		$headings = self::find(array(
			'where' => array('parent_id' => 0),
			'order_by' => array(
				'pos' => 'asc'
			)
			), 'id');

		//override user_id
		if ($user_id !== null && !in_array($user_id, \Model\User::forge()->user_default_template())) {
			$user_headings = self::find(array(
				'where' => array(
					'user_id' => $user_id
				),
				'order_by' => array(
					'pos' => 'asc'
				)
			));
			if ($user_headings !== null) {
				foreach ($user_headings as $h) {
					if (array_key_exists($h->parent_id, $headings)) {
						$headings[$h->parent_id] = $h;
					}
				}
			}
		}
		if ($headings !== null) {
			foreach ($headings as $id => $row) {
				$data[$id] = $row->title;
			}
		}
		return $data;
	}

	private function navigation_child($heading_id, $type, $user_id) {
		$nav_process['attr'] = array('class' => 'button');
		$nav_process['label'] = ucwords($type);
		$nav_process['route'] = '/'.strtolower($type);
		$nav_process['child'] = self::find( function($query) use ($heading_id, $type, $user_id) {
			$query->select('*', array('h1.title', 'title_1'), array('h1.status_id', 'status_1'));
			$query->join(array('headings', 'h1'), 'left');
			$query->on('headings.id', '=', 'h1.parent_id');
			$query->where_open();
			$query->where('headings.id', '=', $heading_id);
			$query->or_where('headings.parent_id', '=', $heading_id);
			$query->where_close();
			$query->where('headings.route', '=', strtolower($type));
			$query->where_open();
			$query->where('headings.user_id', 'in', \Model\User::forge()->user_default_template());
			if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
				$query->or_where('headings.user_id', '=', $user_id);
			}
			$query->where_close();
			return $query;
		});

		if ($nav_process['child'] !== null && current($nav_process['child'])->status_1 !== null && current($nav_process['child'])->status_1 == self::S_UNUSED) {
			$nav_process['attr']['disabled'] = 'disabled';
			$nav_process['label'] = current($nav_process['child'])->title_1 === null ? current($nav_process['child'])->title : current($nav_process['child'])->title_1;
		}
		if ($type == 'products') {
			$nav_process['attr']['class'] .= ' warning';
		}
		if ($type == 'process') {
			$nav_process['attr']['class'] .= ' info';
		}
		return $nav_process;
	}

	public function get_navigation($route, $module, $user_id) {
		$heading = self::find( function($query) use ($route, $user_id) {
			$query->where('route', '=', $route);
			$query->where('status_id', '<>', self::S_UNUSED);
			$query->where_open();
			$query->where('user_id', 'in', \Model\User::forge()->user_default_template());
			if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
				$query->or_where('user_id', '=', $user_id);
			}
			$query->where_close();
			return $query;
		});
		$heading = count($heading) > 1 ? end($heading) : current($heading);

		$nav_process = $this->navigation_child($heading->id, 'process', $user_id);
		$nav_procedures = $this->navigation_child($heading->id, 'procedures', $user_id);
		$nav_products = $this->navigation_child($heading->id, 'products', $user_id);
		$nav['home'] 		= \Html::anchor(\Uri::create('home'), 'Home', array('class' => 'button'));
		$nav['process'] 	= \Html::anchor(\Uri::create($heading->route . $nav_process['route']), $nav_process['label'], $nav_process['attr']);
		$nav['procedures'] 	= \Html::anchor(\Uri::create($heading->route . $nav_procedures['route']), $heading->title . ' SOPs', $nav_procedures['attr']);
		$nav['products'] 	= \Html::anchor(\Uri::create($heading->route . $nav_products['route']), $nav_products['label'], $nav_products['attr']);
		if (array_key_exists($module, $nav)) {
			unset($nav[$module]);
		}

		$process_count = \Model\Process::count('id', true, array('heading_id' => $heading->id));
		$procedures_count = \Model\Procedure::count('id', true, array('heading_id' => $heading->id));
		$products_count = \Model\Product\Group::count('id', true, array('heading_id' => $heading->id));

		if ( $process_count <= 0 ) {
			unset($nav['process']);
		}

		if ( $procedures_count <= 0 ) {
			unset($nav['procedures']);
		}

		if ( $products_count <= 0 ) {
			unset($nav['products']);
		}

		/** SPECIAL NAV **/
		//msds nav
		//fix me (?)
		if ( $process_count > 0 && $procedures_count > 0 && $heading->id == \Modules::SYS_BODYSHOP && \Modules::get_module_num($module) != \Modules::MOD_PRODUCTS ) {
			$nav['msds'] = \Html::anchor(\Uri::create($heading->route . '/msds'), 'MSDS', array('class' => 'button alert'));
		}

		//support nav
		if ( $process_count > 0 && $procedures_count > 0 && $products_count > 0 && \Modules::get_module_num($module) != \Modules::MOD_PRODUCTS ) {
			$nav['support']	= \Html::anchor(\Uri::create($heading->route . '/supports'), 'Technical Support', array('class' => 'button alert'));
		}

		//fittest
		if ( $process_count > 0 && $procedures_count > 0 && $heading->id == \Modules::SYS_PAINTSHOP && \Modules::get_module_num($module) != \Modules::MOD_PRODUCTS ) {
			$nav['fittest']	= \Html::anchor(\Uri::create($heading->route . '/fittest'), 'Fit Test', array('class' => 'button alert'));
		}

		$data['nav'] = join('', $nav);
		$data['title'] = $heading->title;
		return $data;
	}

	public function system_template() {
		$data = self::find(array(
			'where' => array(
				'status_id' => self::S_SYSTEM
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		return $data;
	}

	public function user_active_heading_template($user_id) {
		$active = self::find( function($query) use ($user_id) {
			return $query
			->where_open()
			->where('status_id', '=', self::S_SYSTEM)
			->or_where('status_id', '=', self::S_PARENT)
			->where_close()
			->or_where('user_id', '=', $user_id)
			->order_by('pos', 'asc');
			}, 'id');
		if ( !empty($active) ) {
			foreach ($active as $id => $inline) {
				if ($inline->parent_id > 0 && $inline->status_id != self::S_PARENT) {
					unset($active[$inline->parent_id]);
				}
				if ($inline->status_id == self::S_UNUSED || $inline->status_id == self::S_PARENT) {
					unset($active[$id]);
				}
				if ($inline->status_id == self::S_PARENT && in_array($inline->user_id, \Model\User::forge()->user_default_template())) {
					unset($active[$inline->id]);
				}
				$child_heading = self::find( function($query) use ($user_id, $inline) {
					$query->where_open()
						->where_open()
							->where('status_id', '=', self::S_PARENT)
							->where('parent_id', '=', $inline->id)
						->where_close();
					if (!in_array($user_id, \Model\User::forge()->user_default_template())) {
						$query->or_where('user_id', '=', $user_id);
					}
					$query->where_close()
					->where('pos', '=', $inline->pos)
					->where('parent_id', '<>', 0)
					->where('status_id', '<>', self::S_CUSTOM)
					->order_by('pos', 'asc');
					return $query;
					}, 'id');
				if (!empty($child_heading)) {
					foreach ($child_heading as $id => $row) {
						if ($row->status_id == self::S_UNUSED) {
							unset($child_heading[$row->parent_id]);
							unset($child_heading[$id]);
						}
					}
				}
				$inline->child_sql = \DB::last_query();
				$inline->child = ($child_heading === null) ? array() : $child_heading;
			}
		}
		return $active;
	}

	public function user_unused_heading_template($id) {
		$data = self::find(array(
			'where' => array(
				'status_id' => self::S_UNUSED,
				'user_id' => $id
			),
			'order_by' => array(
				'parent_id' => 'asc'
			)
		));
		return $data;
	}

	public function find_top_parent($id) {
		$found = false;
		while (!$found) {
			$row = self::find_by_pk($id);
			if ($row->parent_id > 0) {
				$id = $row->parent_id;
				$found = false;
			} else {
				break;
			}
		}
		return $row;
	}

	public function parent_level($id) {
		$i = 0;
		$found = false;
		while (!$found) {
			$row = self::find_by_pk($id);
			if ($row->parent_id > 0) {
				$id = $row->parent_id;
				$found = false;
			} else {
				$found = true;
			}
			$i++;
		}
		return $i;
	}

	public function up_order($post) {
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if ( ($option = self::find_by_pk($post['id'])) !== null ) {
			//clone all records for this step and user
			if ( !in_array($post['user_id'], \Model\User::user_default_template()) ) {
				$options = self::find(array(
					'where' => array(
						'step_id' => $option->step_id,
						'status_id' => self::S_SYSTEM,
						array('id', '<>', $option->id)
					)
				));
				if ( $options !== null ) {
					foreach ($options as $opt) {
						$check = \DB::select('*')->from(static::$_table_name)->where('parent_id', '=', $opt->id)
							->where_open()
							->where('status_id', '=', self::S_CUSTOM)
							->or_where('status_id', '=', self::S_UNUSED)
							->where_close()
							->where('user_id', '=', $post['user_id'])
							->execute();
						if (count($check) <= 0) {
							$custom_opt = self::forge()->set($opt->to_array());
							$custom_opt->id = null;
							$custom_opt->user_id = $post['user_id'];
							$custom_opt->status_id = self::S_CUSTOM;
							$custom_opt->parent_id = $opt->id;
							$custom_opt->save(false);
						}
					}
				}
			}

			if ( in_array($post['user_id'], \Model\User::user_default_template()) || $option->status_id == self::S_CUSTOM) {
				//save record 'up' for this moved position, only for system template user
				$option->pos = $post['pos'] - 1;
				$option->save(false);
			} else {
				//create clone record from this row content with new position
				$custom = self::forge()->set($option->to_array());
				$custom->id = null;
				$custom->parent_id = $option->id;
				$custom->pos = $post['pos'] - 1;
				$custom->status_id = self::S_CUSTOM;
				$custom->user_id = $post['user_id'];
				$custom->save(false);
				$option = $custom;
			}

			//update pos replacement
			$rows = self::find(function($query) use ($option, $post){
				$step = \Model\Step::find_by_pk($option->step_id);
				$query->where('pos', '=', $option->pos);
				$query->where('user_id', '=', $post['user_id']);
				$query->where('id', '<>', $option->id);
				$query->where_open();
				$query->where('step_id', '=', $option->step_id);
				if ($step->parent_id > 0) {
					$query->or_where('step_id', '=', $step->parent_id);
				}
				$query->where_close();
				$query->order_by('id', 'desc');
				return $query;
			});

			if ($rows !== null) {
				$data = current($rows);
				$option2 = self::find_by_pk($data->id);
				if ( in_array($post['user_id'], \Model\User::user_default_template()) || $option2->status_id != self::S_SYSTEM) {
					//update the pos for other record for system template or if status_id already custom or unused
					$option2->pos = $post['pos'];
					$option2->save(false);
				}
			}

			$response['hash'] = '#href-step-' . $option->step_id;
		}
		return $response;
	}

	public function down_order($post) {
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if ( ($option = self::find_by_pk($post['id'])) !== null ) {

			//clone all records for this step and user
			if ( !in_array($post['user_id'], \Model\User::user_default_template()) ) {

				$options = self::find(array(
					'where' => array(
						'step_id' => $option->step_id,
						'status_id' => self::S_SYSTEM,
						array('id', '<>', $option->id)
					)
				));
				if ( $options !== null ) {
					foreach ($options as $opt) {
						$check = \DB::select('*')->from(static::$_table_name)->where('parent_id', '=', $opt->id)
							->where_open()
							->where('status_id', '=', self::S_CUSTOM)
							->or_where('status_id', '=', self::S_UNUSED)
							->where_close()
							->where('user_id', '=', $post['user_id'])
							->execute();
						if (count($check) <= 0) {
							$custom_opt = self::forge()->set($opt->to_array());
							$custom_opt->id = null;
							$custom_opt->user_id = $post['user_id'];
							$custom_opt->status_id = self::S_CUSTOM;
							$custom_opt->parent_id = $opt->id;
							$custom_opt->save(false);
						}
					}
				}
			}

			if ( in_array($post['user_id'], \Model\User::user_default_template()) || $option->status_id != self::S_SYSTEM) {
				//save record 'up' for this moved position, only for system template user
				$option->pos = $post['pos'] + 1;
				$option->save(false);
			} else {
				//create clone record from this row content with new position
				$custom = self::forge()->set($option->to_array());
				$custom->id = null;
				$custom->parent_id = $option->id;
				$custom->pos = $post['pos'] + 1;
				$custom->status_id = self::S_CUSTOM;
				$custom->user_id = $post['user_id'];
				$custom->save(false);
				$option = $custom;
			}

			//update pos replacement
			$rows = self::find(array(
				'where' => array(
					'pos' => $option->pos,
					'user_id' => $post['user_id'],
					'step_id' => $option->step_id,
					array('id', '<>', $option->id)
				),
				'order_by' => array('id' => 'desc')
			));

			if ($rows !== null) {
				$data = current($rows);
				$option2 = self::find_by_pk($data->id);
				if ( in_array($post['user_id'], \Model\User::user_default_template()) || $option2->status_id == self::S_CUSTOM) {
					//update the pos for other record for system template or if status_id already custom
					$option2->pos = $post['pos'];
					$option2->save(false);
				}
			} 
			$response['hash'] = '#href-step-' . $option->step_id;
		}
		return $response;
	}

	public function is_allow_revert($optionid) {
		$option = self::find_by_pk($optionid);
		if ($option === null) {
			return true;
		}
		$parent = self::find_by_pk($option->parent_id);
		if ($parent === null) {
			return true;
		}
		if ($option->title != $parent->title) {
			return true;
		}
		return false;
	}

	public function get_last_pos($data) {
		$step = \Model\Step::find_by_pk($data['step_id']);

		$max = \DB::select(\DB::expr('MAX(pos) AS pos'))->from(static::$_table_name);
		$max->where_open();
		$max->where('step_id', '=', $data['step_id']);
		if ($step->parent_id > 0) {
			$max->or_where('step_id', '=', $step->parent_id);
		}
		$max->where_close();
		$max->where_open();
		$max->where('user_id', '=', $data['user_id']);
		$default_template = \Model\User::forge()->user_default_template();
		if ( count($default_template) > 0 ) {
			$max->or_where('user_id', 'in', \Model\User::forge()->user_default_template());
		}
		$max->where_close();
		$data = $max->execute()->get('pos');
		return $data === null ? 1 : $data + 1;
	}

	public function get_system_last_pos($data) {
		$step = \Model\Step::find_by_pk($data['step_id']);

		$max = \DB::select(\DB::expr('MAX(pos) AS pos'))->from(static::$_table_name);
		$max->where('user_id', 'in', \Model\User::forge()->user_default_template());
		$max->where_open();
		$max->where('step_id', '=', $data['step_id']);
		if ($step->parent_id > 0) {
			$max->or_where('step_id', '=', $step->parent_id);
		}
		$max->where_close();
		$data = $max->execute()->get('pos');
		return $data === null ? 1 : $data + 1;
	}

	public function revert_other_pos($param) {
		$opts = \DB::select()->from(static::$_table_name)->where('parent_id', '>', 0)->execute();
		if ($opts->count() > 0) {
			foreach ($opts->as_array() as $opt) {
				$parent = self::find_by_pk($opt['parent_id']);
				$option = self::find_by_pk($opt['id']);
				$option->pos = $parent->pos;
				$option->save(false);
			}
		}
		///update user defined
		$opts = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $param['user_id'])->where('status_id', self::S_CUSTOM)->order_by('created_by', 'asc')->execute();
		if ($opts->count() > 0) {
			$lastpos = $this->get_last_pos($param);
			foreach ($opts->as_array() as $row) {
				$option = self::find_by_pk($row['id']);
				$option->pos = $lastpos;
				$option->save(false);
				$lastpos++;
			}
		}
		return true;
	}

	static function comp($a, $b) {
		if ($a['step_id'] == $b['step_id']) {
	        return $a['pos'] - $b['pos'];
	    }
	    return $a['step_id'] - $b['step_id'];
	}

	protected function post_save($result) {
		if ( in_array($this->user_id, \Model\User::user_default_template()) ) {
			$rows = self::find(array(
				'where' => array(
					'step_id' => $this->step_id,
					array('pos', '>=', $this->pos),
					array('id', '<>', $this->id)
				)
			));
			if ($rows !== null) {
				foreach ($rows as $row) {
					$row->pos = $row->pos + 1;
					$row->save(false);
				}
			}
		}
		return $result;
	}

	/*
	protected function post_update($result) {
		//update position for all childrens based on top parent pos
		$childs = self::find_by_parent_id($this->id);
		if ( $childs !== null ) {
			foreach ($childs as $c) {
				$row = self::find_by_pk($c->id);
				$row->pos = $this->pos;
				$row->save(false);

				$newchilds = self::find_by_parent_id($c->id);
				if ($newchilds !== null) {
					foreach ($newchilds as $n) {
						$nrow = self::find_by_pk($n->id);
						$nrow->pos = $this->pos;
						$nrow->save(false);
					}
				}
			}
		}
		return $result;
	}
	*/

	protected function post_delete($result) {
		$headings = self::find(array(
			'where' => array(
				'user_id' => $this->user_id,
				'parent_id' => 0,
				array('pos', '>', $this->pos)
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		$pos = $this->pos;
		if ($headings !== null) {
			foreach ($headings as $row) {
				$heading = self::find_by_pk($row->id);
				$heading->pos = $pos;
				$heading->save(false);
				$pos ++;
			}
		}
		return $result;
	}

}
