<?php
namespace Model;

class Content extends \Model\Base
{
	const TYPE_TEXT = 0;
	const TYPE_HTML = 1;
	const TYPE_IMAGE = 2;

	protected static $_table_name = 'content';
	protected static $_rules = array('title' => 'required');

	public static $_defaults = array('id' => null, 'parent_id' => 0, 'user_id' => 0, 'option_id' => null, 'title' => '', 'content' => '', 'content_type' => self::TYPE_HTML, 'pos' => 1, 'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0, 'status_id' => self::S_SYSTEM);
	protected $_type = array(self::TYPE_TEXT => 'Plain Text', self::TYPE_HTML => 'Html', self::TYPE_IMAGE => 'Image');
	protected static $_mass_whitelist = array('id', 'parent_id', 'user_id', 'option_id', 'title', 'content', 'content_type', 'pos', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status_id');

	public static function get_table_name()
	{
		return self::$_table_name;
	}

	public function get_for_user($user)
	{
		// Load default

		$defaultquery = \DB::select()->from(static::$_table_name);
		$defaultquery->where_open();
		$defaultquery->where('user_id', $user->template_id);
		$defaultquery->or_where('user_id', $user->id);
		$defaultquery->where_close();
		$defaultassoc = $defaultquery->order_by('pos', 'asc')->order_by('user_id', 'asc')->as_assoc()->execute();
		$default = $defaultassoc->as_array('id');
		if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
			$data = array();
			// Override with user
			foreach ($default as $key => $row) {
				$content = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->where('user_id', $user->id)->order_by('pos', 'asc')->as_assoc()->execute();
				if ($content->count() > 0) {
					if (!in_array($user->template_id, \Model\Template::forge()->get_system_template())) {
						$data[$key] = $content->current();
					}
				} else {
					if (($row['parent_id'] == 0 && $row['status_id'] == self::S_CUSTOM) === false) {
						$data[$key] = $row;
					}
				}
			}
			// Load user specific
			$contentassoc = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $user->id)->order_by('pos', 'asc')->as_assoc()->execute();
			if ($contentassoc->count() > 0) {
				//return array_merge($data, $contentassoc->as_array('id'));
				$data = $data + $contentassoc->as_array('id');
			}
			//check if row duplicate based on key array
			if (count($data) > 0) {
				foreach ($data as $key => $row) {
					if (array_key_exists($row['parent_id'], $data)) {
						unset($data[$row['parent_id']]);
					}
				}
			}
			return $data;
		}
		return $default;
	}

	public function get_for_options($user, $option_id = null)
	{
		// Load default
		$query = \DB::select()->from(static::$_table_name);
		$query->where('parent_id', 0);
		$template = \Model\Template::find_by_pk($user->template_id);
		// Template user always = 1
		$query->where('user_id', 1);
		if ($option_id !== null) {
			$query->where('option_id', $option_id);
		}
		$query->where('status_id', '<>', self::S_UNUSED);
		$query->order_by('pos', 'asc');
		$query->as_assoc();
		$default = $query->execute();
		if ($user->id != 1) {
			$data = array();
			// Override with user
			foreach ($default as $row) {
				$query_user = \DB::select()->from(static::$_table_name);
				$query_user->where('parent_id', $row['id']);
				$query_user->where('user_id', $user->id);
				if ($option_id !== null) {
					$query_user->where('option_id', $option_id);
				}
				$query_user->order_by('pos', 'asc');
				$query_user->as_assoc();
				$content = $query_user->execute();

				//$user = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->where('user_id', $id)->as_assoc()->execute();
				if ($content->count() > 0) {
					$user_row = $content->current();
					if ($user_row['status_id'] != self::S_UNUSED) {
						$data[] = $user_row;
					}
				} else {
					$data[] = $row;
				}
			}

			// Load user specific
			$query_specific = \DB::select()->from(static::$_table_name);
			$query_specific->where('parent_id', 0);
			$query_specific->where('user_id', $user->id);
			if ($option_id !== null) {
				$query_specific->where('option_id', $option_id);
			}
			$query_specific->where('status_id', '<>', self::S_UNUSED);
			$query_specific->order_by('pos', 'asc');
			$query_specific->as_assoc();
			$content = $query_specific->execute();
			//$user = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->where('user_id', $id)->as_assoc()->execute();
			if ($content->count() > 0) {
				return array_merge($data, $content->as_array());
			}
			return $data;
		}
		return $default;
	}

	public function get_for_template($template_id)
	{
		$defaultquery = \DB::select()->from(static::$_table_name);
		$defaultassoc = $defaultquery->order_by('pos', 'asc')->order_by('user_id', 'asc')->as_assoc()->execute();
		$default = $defaultassoc->as_array('id');
		if (!in_array($template_id, \Model\Template::forge()->get_system_template())) {
			$data = array();
			// Override with user
			foreach ($default as $key => $row) {
				$content = \DB::select()->from(static::$_table_name)->where('parent_id', $row['id'])->order_by('pos', 'asc')->as_assoc()->execute();
				if ($content->count() > 0) {
					if (!in_array($template_id, \Model\Template::forge()->get_system_template())) {
						$data[$key] = $content->current();
					}
				} else {
					if (($row['parent_id'] == 0 && $row['status_id'] == self::S_CUSTOM) === false) {
						$data[$key] = $row;
					}
				}
			}
			// Load user specific
			$contentassoc = \DB::select()->from(static::$_table_name)->where('parent_id', 0)->order_by('pos', 'asc')->as_assoc()->execute();
			if ($contentassoc->count() > 0) {
				//return array_merge($data, $contentassoc->as_array('id'));
				$data = $data + $contentassoc->as_array('id');
			}
			//check if row duplicate based on key array
			if (count($data) > 0) {
				foreach ($data as $key => $row) {
					if (array_key_exists($row['parent_id'], $data)) {
						unset($data[$row['parent_id']]);
					}
				}
			}
			return $data;
		}
		return $default;
	}

	/**
	 * @override
	 */
	public function get_status_array($id = null)
	{
		$status = parent::get_status_array($id);
		if ($id !== null) {
			switch ($id) {
				case self::S_SYSTEM:
					$class = 'secondary';
					break;
				case self::S_UNUSED:
					$class = 'info';
					break;
				case self::S_PARENT:
					$class = 'warning';
					break;
				default:
					$class = '';
					break;
			}
			$html = '<span class="radius ' . $class . ' label">' . $status . '</span>';
			return $html;
		}
		return $status;
	}

	public function get_type_array($id = null)
	{
		if ($id === null) {
			return $this->_type;
		}
		return array_key_exists($id, $this->_type) ? $this->_type[$id] : $this->_type;
	}

	public function get_heading($params)
	{
		$data = array();
		$processes = self::find(function ($query) use ($params) {
			return $query->where('heading_id', '=', $params['heading_id'])
				->where_open()
				->where('user_id', 'in', \Model\User::forge()->user_default_template())
				->or_where('user_id', '=', $params['user_id'])
				->where_close()
				->order_by('pos', 'asc');
		}, 'id');
		if ($processes !== null) {
			foreach ($processes as $id => $process) {
				if ($process->parent_id > 0) {
					unset($processes[$process->parent_id]);
				}
				if ($process->status_id == self::S_UNUSED) {
					unset($processes[$id]);
				}
			}
			return $processes;
		}
		return $data;
	}

	public function get_unused($params)
	{
		$data = array();
		$processes = self::find(array(
			'where' => array(
				'heading_id' => $params['heading_id'],
				'status_id' => self::S_UNUSED,
				'user_id' => $params['user_id']
			),
			'order_by' => array(
				'pos' => 'asc'
			)
		));
		if ($processes !== null) {
			return $processes;
		}
		return $data;
	}

	public function get_parent_from($parent_id)
	{
		$data = self::find_one_by('id', $parent_id);
		if ($data !== null) {
			return $data->title;
		}
		return false;
	}

	public function position_number($data)
	{
		$option = \Model\Option::find_by_pk($data['option_id']);

		$max = \DB::select(\DB::expr('MAX(pos) AS pos'))->from(static::$_table_name);
		$max->where_open();
		$max->where('option_id', '=', $data['option_id']);
		if ($option->parent_id > 0) {
			$max->or_where('option_id', '=', $option->parent_id);
		}
		$max->where_close();
		$max->where_open();
		$max->where('user_id', 'in', \Model\User::forge()->user_default_template());
		$max->or_where('user_id', '=', $data['user_id']);
		$max->where_close();
		$data = $max->execute()->get('pos');
		return $data === null ? 1 : $data + 1;
	}

	public function get_sub_menu($heading_id, $user_id = null)
	{
		$data = self::find(function ($query) use ($heading_id, $user_id) {
			$table = self::$_table_name;
			$query->select($table . '.id', $table . '.title', $table . '.content', array('h.title', 'heading'), 'h.route', $table . '.parent_id', $table . '.status_id');
			$query->join(array(\Model\Heading::get_table_name(), 'h'));
			$query->on('h.id', '=', $table . '.heading_id');
			$query->where('h.id', '=', $heading_id);
			$query->where_open();
			$query->where($table . '.user_id', 'in', \Model\User::forge()->user_default_template());
			if ($user_id !== null) {
				$query->or_where($table . '.user_id', '=', $user_id);
			}
			$query->where_close();
			$query->order_by($table . '.pos', 'asc');
			return $query;
		}, 'id');
		if ($data !== null) {
			foreach ($data as $id => $row) {
				if (array_key_exists($row->parent_id, $data)) {
					unset($data[$row->parent_id]);
				}
				if ($row->status_id == self::S_UNUSED) {
					unset($data[$row->id]);
				}
			}
		}
		return $data;
	}

	public function up_order($post)
	{
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if (($content = self::find_by_pk($post['id'])) !== null) {

			//clone all records for this step and user
			if (!in_array($post['user_id'], \Model\User::user_default_template())) {

				$option = \Model\Option::find_by_pk($content->option_id);
				$db = \DB::select()->from(static::$_table_name)->where('status_id', self::S_SYSTEM)->where('id', '<>', $content->id);
				$db->where_open();
				$db->where('option_id', $content->option_id);
				if ($option->parent_id > 0) {
					$db->or_where('option_id', $option->parent_id);
				}
				$db->where_close();
				$contents = $db->as_assoc()->execute();
				if ($contents->count() > 0) {
					foreach ($contents->as_array() as $ct) {
						$check = \DB::select('*')->from(static::$_table_name)->where('parent_id', '=', $ct['id'])
							->where_open()
							->where('status_id', '=', self::S_CUSTOM)
							->or_where('status_id', '=', self::S_UNUSED)
							->where_close()
							->where('user_id', '=', $post['user_id'])
							->execute();
						if (count($check) <= 0) {
							$custom_ct = self::forge()->set($ct);
							$custom_ct->id = null;
							$custom_ct->user_id = $post['user_id'];
							$custom_ct->status_id = self::S_CUSTOM;
							$custom_ct->parent_id = $ct['id'];
							$custom_ct->save(false);
						}
					}
				}
			}

			if (in_array($post['user_id'], \Model\User::user_default_template()) || $content->status_id == self::S_CUSTOM) {
				//save record 'up' for this moved position, only for system template user
				$content->pos = $post['pos'] - 1;
				$content->save(false);
			} else {
				//create clone record from this row content with new position
				$custom = self::forge()->set($content->to_array());
				$custom->id = null;
				$custom->parent_id = $content->id;
				$custom->pos = $post['pos'] - 1;
				$custom->status_id = self::S_CUSTOM;
				$custom->user_id = $post['user_id'];
				$custom->save(false);
				$content = $custom;
			}

			//update pos replacement
			$rows = self::find(function ($query) use ($content, $post) {
				$option = \Model\Option::find_by_pk($content->option_id);
				$query->where('pos', '=', $content->pos);
				$query->where('user_id', '=', $post['user_id']);
				$query->where('id', '<>', $content->id);
				$query->where_open();
				$query->where('option_id', '=', $content->option_id);
				if ($option->parent_id > 0) {
					$query->or_where('option_id', '=', $option->parent_id);
				}
				$query->where_close();
				$query->order_by('id', 'desc');
				return $query;
			});

			if ($rows !== null) {
				$data = current($rows);
				$ct2 = self::find_by_pk($data->id);
				if (in_array($post['user_id'], \Model\User::user_default_template()) || $ct2->status_id != self::S_SYSTEM) {
					//update the pos for other record for system template or if status_id already custom or unused
					$ct2->pos = $post['pos'];
					$ct2->save(false);
				}
			}

			$response['hash'] = '#href-option-' . $post['option'];
		}
		return $response;
	}

	public function down_order($post)
	{
		$response = array('status' => 'OK', 'msg' => 'Success', 'hash' => '#');
		if (($content = self::find_by_pk($post['id'])) !== null) {

			//clone all records for this step and user
			if (!in_array($post['user_id'], \Model\User::user_default_template())) {
				$option = \Model\Option::find_by_pk($content->option_id);
				$db = \DB::select()->from(static::$_table_name)->where('status_id', self::S_SYSTEM)->where('id', '<>', $content->id);
				$db->where_open();
				$db->where('option_id', $content->option_id);
				if ($option->parent_id > 0) {
					$db->or_where('option_id', $option->parent_id);
				}
				$db->where_close();
				$contents = $db->as_assoc()->execute();
				if ($contents->count() > 0) {
					foreach ($contents->as_array() as $ct) {
						$check = \DB::select('*')->from(static::$_table_name)->where('parent_id', '=', $ct['id'])
							->where_open()
							->where('status_id', '=', self::S_CUSTOM)
							->or_where('status_id', '=', self::S_UNUSED)
							->where_close()
							->where('user_id', '=', $post['user_id'])
							->execute();
						if (count($check) <= 0) {
							$custom_ct = self::forge()->set($ct);
							$custom_ct->id = null;
							$custom_ct->user_id = $post['user_id'];
							$custom_ct->status_id = self::S_CUSTOM;
							$custom_ct->parent_id = $ct['id'];
							$custom_ct->save(false);
						}
					}
				}
			}

			if (in_array($post['user_id'], \Model\User::user_default_template()) || $content->status_id != self::S_SYSTEM) {
				//save record 'up' for this moved position, only for system template user
				$content->pos = $post['pos'] + 1;
				$content->save(false);
			} else {
				//create clone record from this row content with new position
				$custom = self::forge()->set($content->to_array());
				$custom->id = null;
				$custom->parent_id = $content->id;
				$custom->pos = $post['pos'] + 1;
				$custom->status_id = self::S_CUSTOM;
				$custom->user_id = $post['user_id'];
				$custom->save(false);
				$content = $custom;
			}

			//update pos replacement
			$rows = self::find(array(
				'where' => array(
					'pos' => $content->pos,
					'user_id' => $post['user_id'],
					'option_id' => $content->option_id,
					array('id', '<>', $content->id)
				),
				'order_by' => array('id' => 'desc')
			));

			if ($rows !== null) {
				$data = current($rows);
				$ct2 = self::find_by_pk($data->id);
				if (in_array($post['user_id'], \Model\User::user_default_template()) || $ct2->status_id == self::S_CUSTOM) {
					//update the pos for other record for system template or if status_id already custom
					$ct2->pos = $post['pos'];
					$ct2->save(false);
				}
			}
			$response['hash'] = '#href-option-' . $post['option'];
		}
		return $response;
	}

	public function is_allow_revert($contentid)
	{
		$content = self::find_by_pk($contentid);
		if ($content === null) {
			return true;
		}
		$parent = self::find_by_pk($content->parent_id);
		if ($parent === null) {
			return true;
		}
		if (strcmp($content->title, $parent->title) === 0 && strcmp($content->content, $parent->content) === 0) {
			return true;
		}
		return false;
	}

	static function comp($a, $b)
	{
		if ($a['option_id'] == $b['option_id']) {
			return $a['pos'] - $b['pos'];
		}
		return $a['option_id'] - $b['option_id'];
	}


	protected function post_update($result)
	{
		if (in_array($this->user_id, \Model\User::user_default_template())) {
			$rows = self::find(array(
				'where' => array(
					'option_id' => $this->option_id,
					array('pos', '>=', $this->pos),
					array('id', '<>', $this->id)
				)
			));
			if ($rows !== null) {
				foreach ($rows as $row) {
					$row->pos = $row->pos + 1;
					$row->save(false);
				}
			}
		}
		return $result;
	}

}
