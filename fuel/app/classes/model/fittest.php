<?php
namespace Model;

class Fittest extends \Model\Base {

	protected static $_table_name = 'fit_tests';
	public static $_defaults =	array('id' => null, 'heading_id' => null, 'content' => '', 'created_at' => '', 'updated_at' => '');
	protected static $_rules = array('heading_id' => 'required', 'content' => 'required');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$aColumns = array('heading', 'created_at', 'updated_at', 'preview');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT " . self::$_table_name . ".id, h.title AS heading, content, DATE_FORMAT(" . self::$_table_name . ".created_at, '" . self::NICE_DATE_FORMAT . "') AS created_at, DATE_FORMAT(" . self::$_table_name . ".updated_at, '" . self::NICE_DATE_FORMAT . "') AS updated_at FROM " . self::$_table_name . " INNER JOIN " . \Model\Heading::get_table_name() . " h ON h.id = " . self::$_table_name . ".heading_id) AS tmp ";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'WHERE (';
			$sql .= ' heading LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR created_at LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR updated_at LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id ASC';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$action = \Html::anchor('#', 'Preview', array('data-id' => $row['id'], 'class' => 'button small btn-action fittest-view'));
			$action .= \Html::anchor('#', 'Delete', array('data-id' => $row['id'], 'class' => 'button small btn-action alert fittest-delete'));
			$row['preview'] = $action;
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

}