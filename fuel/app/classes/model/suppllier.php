<?php
namespace Model;

class Supplier extends \Model_Crud {
	const S_NEW = 0; // New not validated
	const S_ACTIVE = 1;
	const S_PASSWORD = 2; // Needs new password
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Account closed, info archived

	protected static $_table_name = 'suppliers';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	public static $_defaults =	array('id' => null, 'name' => '', 'site_url' => '', 'created_at' => '', 'updated_at' => '',
		'created_by' => 0, 'updated_by' => 0, 'status_id' => 0);
	protected $_status = array(0 => 'New', 'Active', 'Password', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');

	public function get_status_array() {
		return $this->_status;
	}
}
