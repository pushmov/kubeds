<?php
namespace Model;

class Product extends \Model\Base {

	const S_DISABLED = 0;
	const S_ACTIVE = 1;

	protected static $_table_name = 'products';

	public static $_defaults =	array('id' => null, 'sup_id' => null, 'part_num' => '', 'description' => '',
		'created_at' => '', 'updated_at' => '', 'created_by' => 0, 'updated_by' => 0,'status_id' => self::S_ACTIVE);
	protected static $_rules = array('part_num' => 'required|is_unique', 'description' => 'required', 'sup_id' => 'is_valid_supplier');
	protected static $_labels = array('part_num' => 'part number', 'description' => 'description', 'sup_id' => 'supplier');
	protected $_status = array(self::S_DISABLED => 'Disabled', self::S_ACTIVE => 'Active');

	public static function get_table_name() {
		return self::$_table_name;
	}

	public function _validation_is_unique($val) {
		$id = \Validation::active()->input('id');
		$row = self::find_one_by(array(
			'sup_id' => \Validation::active()->input('sup_id'),
			'part_num' => $val
		));
		if (($row !== null) && ($row->id != $id)) {
			\Validation::active()->set_message('is_unique', 'Part number \'' . $val . '\' already exists for this supplier');
			return false;
		}
		return true;
	}

	public function _validation_is_valid_supplier($val) {
		$rows = \Model\Supplier::find_by_pk($val);
		if ($rows === null) {
			\Validation::active()->set_message('is_valid_supplier', 'Invalid supplier data');
			return false;
		}
		return true;
	}

	public function datatable($params) {
		$data['data'] = array();
		$data['draw'] = isset($params['draw']) ? (int)$params['draw'] : 1;

		$tb_alias = self::$_table_name;
		$tb_sup = \Model\Supplier::forge()->get_table_name();

		$status = "(CASE ";
		foreach($this->_status as $val => $name){
			$status .= " WHEN {$tb_alias}.status_id = '{$val}' THEN '{$name}'";
		}
		$status .= " END)";

		$aColumns = array('part_num', 'supplier_name', 'description', 'status', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT {$tb_alias}.id, {$tb_alias}.part_num, {$tb_alias}.description, {$status} AS status, {$tb_sup}.name AS supplier_name, {$tb_sup}.site_url AS supplier_site FROM {$tb_alias} INNER JOIN {$tb_sup} ON {$tb_sup}.id = {$tb_alias}.sup_id) AS tmp ";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if ( isset($params['supplier']) && $params['supplier'] != '' ) {
			//filter supplier only
			$sql .= 'WHERE supplier_name LIKE \'%' . $params['supplier'] . '%\' ';
			if ( isset($params['search']['value']) && $params['search']['value'] != '' ) {
				$sql .= 'AND ';
			}
		}
		elseif ( isset($params['search']['value']) && $params['search']['value'] != '' ) {
			$sql .= 'WHERE ';
		}

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= '(';
			$sql .= ' supplier_name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR part_num LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR description LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}
		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY id';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			$sup = ($row['supplier_site'] == '') ? $row['supplier_name'] : \Html::anchor($row['supplier_site'], $row['supplier_name'], array('target' => '_blank'));
			$action = html_tag('button', array('data-id' => $row['id'], 'data-action' => 'view', 'class' => 'button small'), 'View');
			$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'update', 'class' => 'button small warning'), 'Edit');
			//$action .= html_tag('button', array('data-id' => $row['id'], 'data-action' => 'delete', 'class' => 'button small alert'), 'Delete');
			$row['action'] = $action;
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		// Total rows
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function view($id) {
		$product = self::find( function($query) use ($id) {
			return $query
				->select(self::$_table_name . '.*', array('a1.first_name', 'created_first_name'), array('a1.last_name', 'created_last_name'), array('a2.last_name', 'updated_last_name'), array('a2.first_name', 'updated_first_name'), array('sup.name', 'supplier_name'), array('sup.site_url', 'supplier_site'))
				->join(array(\Model\Admin::get_table_name(), 'a1'))
				->on('a1.id', '=', self::$_table_name . '.created_by')
				->join(array(\Model\Admin::get_table_name(), 'a2'))
				->on('a2.id', '=', self::$_table_name . '.updated_by')
				->join(array(\Model\Supplier::get_table_name(), 'sup'))
				->on('sup.id', '=', self::$_table_name . '.sup_id')
				->where(self::$_table_name . '.id', '=', $id);
		});
		if ($product !== null) {
			return current($product)->to_array();
		}
		return self::$_defaults;
	}

	public function all_json_data() {
		$data = array();
		$products = self::find_by_status_id(self::S_ACTIVE);
		if ($products !== null) {
			foreach ($products as $p) {
				$data[] = array('name' => $p->part_num . ' - ' . $p->description, 'id' => $p->id);
			}
		}
		return $data;
	}

	public function get_approved_products($group_id) {
		$group = \Model\Product\Group::find_by_pk($group_id);
		$data = \Model\Product::find( function($query) use ($group_id, $group) {
			$query->select('a.id', 'a.parent_id', 'a.status_id', 'part_num', array('s.name', 'manufacturer'), 'description', 'a.comments');
			$query->join(array('product_approveds', 'a'), 'inner');
			$query->on('a.product_id', '=', 'products.id');
			$query->join(array('product_groups', 'g'), 'inner');
			$query->on('g.id', '=', 'a.group_id');
			$query->join(array('suppliers', 's'), 'inner');
			$query->on('s.id', '=', 'products.sup_id');
			$query->where('g.id', '=', $group_id);
			if ($group->parent_id > 0) {
				$query->or_where('g.id', '=', $group->parent_id);
			}
			$query->order_by('products.part_num', 'asc');
		}, 'id' );
		if ($data !== null) {
			foreach ($data as $id => $row) {
				if ($row['status_id'] == \Model\Product\Approved::S_UNUSED) {
					unset($data[$id]);
				}
				if ($row['parent_id'] > 0) {
					unset($data[$row['parent_id']]);
				}
			}
		}
		return $data;
	}

}
