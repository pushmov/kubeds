<?php
return array(
	'_root_' 	=> 'home',  // The default route
	'_404_' 	=> 'error404',    // The main 404 route
	'admin' 	=> 'admin/dashboard',
	'admin/users/(:num)' => 'admin/users/load/$1',
	'admin/(:everything)' => 'admin/$1',
	'login' => 'login',
	'login/(:everything)' => 'login/$1',
	'pages/load' => 'pages/load',
	'(:step)/(:option)' => 'pages/option',

	/*
	'logout' => 'login/logout',
	'login/recover/:token' => 'login/recover/$1',
	'body-shop/sops' => 'bodyshop/sop',
	'body-shop/products' => 'bodyshop/approved_products',
	'body-shop/msds' => 'bodyshop/msds',
	'body-shop/support' => 'mypage/techsupport',
	'paint-shop' => 'paintshop/index',
	'paint-shop/sops' => 'paintshop/sop',
	'paint-shop/products' => 'paintshop/approved_products',
	'paint-shop/test' => 'paintshop/fit_test',
	'paint-shop/support' => 'mypage/techsupport',
	'detail-shop' => 'detailshop/index',
	'detail-shop/sops' => 'detailshop/sop',
	'detail-shop/products' => 'detailshop/approved_products',
	'detail-shop/support' => 'mypage/techsupport',
*/
);