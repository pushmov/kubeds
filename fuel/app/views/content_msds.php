<div class="grid-x page-content grid-padding-x small-up-1 medium-up-4">
<?php if ( !empty($contents) ) : ?>
	<?php foreach ($contents as $row) : ?>
	<div class="cell msds"><?=\Html::anchor($row['url'], $row['description'], array('class' => 'button'))?></div>
	<?php endforeach; ?>
<?php endif; ?>
</div>