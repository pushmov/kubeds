<div class="grid-x page-content grid-padding-x small-up-1 medium-up-4">
<?php if ( !empty($contents) ) : ?>
	<?php foreach ($contents as $row) : ?>
	<div class="cell msds text-center padding-2">
		<?=\Html::anchor($row['url'], \Asset::img(\Model\Support::TECH_SUPPORT_PATH . $row['logo']), array('target' => '_blank'));?>
		<p><?=\Html::anchor($row['url'], $row['name'], array('target' => '_blank'));?></p>
	</div>
	<?php endforeach; ?>
<?php endif; ?>
</div>