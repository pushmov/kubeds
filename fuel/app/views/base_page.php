<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
		<?= \Asset::css($styles, array(), null, false); ?>
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
		<?= \Asset::js($scripts, array(), null, false); ?>
		<script>var baseUrl = '<?=Uri::base(false);?>';</script>
	</head>
	<body class="grid-container<?=$backg;?>">
		<header id="header" class="grid-x">
			<div class="small-4 cell"><?=\Html::anchor('/', \Asset::img('kube-logo.png', array('alt' => 'Kube DS')));?></div>
		</header>
		<div id="main_content">
			<?=$content;?>
		</div>
		<footer id="footer" class="grid-x">
			<div class="small-12 medium-4 cell">
				&copy;Color Compass Corporation
			</div>
			<div class="small-12 medium-4 cell text-center">
			<?php if ($logged_in): echo \Html::anchor('/login/logout', 'Logout'); endif; ?>
			</div>
			<div class="small-12 medium-4 cell text-right">
				<?= \Html::anchor('http://www.colorcompass.com', \Asset::img('ccc-logo.png', array('alt' => 'Color Compass Corporation')));?>
			</div>

		</footer>
	</body>
	<script>$(document).foundation();</script>
</html>