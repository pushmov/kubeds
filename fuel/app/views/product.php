<div>
	<h3>Approved Product List | <?=current($content)['title']?></h3>
</div>
<table id="dataproduct" class="show clickable cell-border grid-x-border dataTable">
	<thead>
		<tr>
			<th>PART #</th>
			<th>MANUFACTURER</th>
			<th>DESCRIPTION</th>
			<th>COMMENTS</th>
		</tr>
	</thead>
	<tbody>
		<?php if (!empty($content)) : ?>
			<?php foreach (current($content)['approved'] as $row) :?>
			<tr>
				<td><?=$row['part_num']?></td>
				<td></td>
				<td><?=$row['description']?></td>
				<td></td>
			</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#dataproduct').DataTable({
		iDisplayLength: 50,
		responsive: true
	});
</script>