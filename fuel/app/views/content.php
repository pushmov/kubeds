<div class="grid-x page-title">
    <div class="small-12 medium-6 cell">
        <h2><?= $title; ?></h2>
    </div>
    <div class="small-12 medium-6 cell page-navigation text-center">
        <?= \Html::anchor('home', 'Home', array('class' => 'button')); ?>
        <?php foreach ($buttons as $button) : ?>
            <?php if ($button['status_id'] != \Model\Option::S_UNUSED) : ?>
                <?= \Html::anchor(\Uri::create($route . '/' . ($button['parent_id'] == 0 ? $button['id'] : $button['parent_id'])), $button['title'], array('class' => 'button ' . \Model\Option::forge()->get_button_class($button['type_id']))); ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>

<div id="page_content" class="grid-x page-content">
    <?php if (!empty($contents) && $option_type != \Model\Option::T_PRODUCT) : ?>
        <?php if ($option_type == \Model\Option::T_SOP) : ?>
            <ol class="grid-x sop-menu">
                <?php foreach ($contents as $row) : ?>
                    <?php if ($row['status_id'] != \Model\Base::S_UNUSED) : ?>
                        <li>
                            <?= \Html::anchor('', $row['title'], array('class' => 'process', 'id' => $row['id'], 'type' => $option_type)); ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ol>
        <?php else : ?>
            <div class="grid-x">
                <ol class="menu cell">
                    <?php foreach ($contents as $row) : ?>
                        <?php if ($row['status_id'] != \Model\Base::S_UNUSED) : ?>
                            <li><?= \Html::anchor('', $row['title'], array('class' => 'process', 'id' => $row['id'], 'type' => $option_type)); ?></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ol>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($option_type == \Model\Option::T_PRODUCT) : ?>
        <table id="dataproduct" class="show clickable cell-border grid-x-border dataTable" width="100%">
            <thead>
                <tr>
                    <th>PART #</th>
                    <th>MANUFACTURER</th>
                    <th>DESCRIPTION</th>
                    <th>COMMENTS</th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($contents[$option_id])) : ?>
                    <?php foreach ($contents[$option_id] as $row) : ?>
                        <tr>
                            <td><small><?= $row['part_num'] ?></small></td>
                            <td><small><?= $row['manufacturer']; ?></small></td>
                            <td><small><?= $row['description'] ?></small></td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<?php if ($option_type == \Model\Option::T_MSDS || $option_type == \Model\Option::T_SUPPORT) : ?>
    <?= $sub_content; ?>
<?php endif; ?>
<script>
    $('.process').on('click', function (e) {
        e.preventDefault();
        $.get(baseUrl + 'pages/load.html', {'id': $(this).prop('id'), 'type': $(this).prop('type')}, function (html) {
            $('#page_content').html(html);
        });
    });
</script>

<?php if ($admin == true && $content_id > 0) : ?>
    <script type="text/javascript">
        $('.menu.cell li a[id="<?= $content_id ?>"]').trigger('click');
        $('a.process[id="<?= $content_id ?>"]').trigger('click');
    </script>
<?php endif; ?>

<?php if ($option_type == \Model\Option::T_PRODUCT) : ?>
    <script type="text/javascript">
        $('#dataproduct').DataTable({
            iDisplayLength: 50,
            responsive: true
        });
    </script>
<?php endif; ?>