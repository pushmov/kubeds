<div class="grid-x small-up-1 medium-up-2 large-up-3 text-center">
	<?php foreach($data as $row): ?>
		<div class="cell with-arrow">
			<?=\Asset::img($row['img']);?><br>
			<?=$row['pos'] . ' ' . $row['title'];?><br>
			<?php if (isset($row['options'])) foreach($row['options'] as $option) echo $option . '<br>'; ?>
			<span class="fi-arrow-down"></span>
		</div>
	<?php endforeach;?>
</div>
