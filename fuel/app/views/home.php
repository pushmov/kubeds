
<div class="grid-x">
	<!--- Desktop View -->
	<div class="main-container dektop-container">
		
			<div class="box-left">
				<div class="box-1">
					<div class="box-image"><?=\Asset::img($data[1]['img'])?></div>
					<div class="box-caption"><?=$data[1]['label'];?></div>
				</div>
				<div class="arrow-1">
					<div class="option-buttons">
						<?php if (isset($data[1]['options']) && !empty($data[1]['options'])) :?>
						<div><?=join('', $data[1]['options']);?></div>
						<?php endif; ?>
					</div>
				</div>
				<div class="arrow-2">
					<div class="option-buttons">
						<?php if (isset($data[9]['options']) && !empty($data[9]['options'])) :?>
						<div><?=join('', $data[9]['options']);?></div>
						<?php endif; ?>
					</div>
				</div>
				<div class="box-8">
					<div class="box-image"><?=\Asset::img($data[9]['img']);?></div>
					<div class="box-caption"><?=$data[9]['label']?></div>
				</div>
			</div>
			<div class="box-2">
				<div class="box-image"><?=\Asset::img($data[2]['img']);?></div>
				<div class="box-caption"><?=$data[2]['label'];?></div>
				<?php if (isset($data[2]['options']) && !empty($data[2]['options'])) :?>
				<?=join('', $data[2]['options']);?>
				<?php endif; ?>
			</div>
			<div class="middle-arrow">
				<div class="arrow-3"></div>
				<div class="arrow-4"></div>
			</div>
			<div class="box-3">
				<div class="box-caption"><?=$data[3]['label'];?></div>
				<div class="box-3-image"><?=\Asset::img($data[3]['img']);?></div>
				<div class="box-3-center">
					<div class="arrow-5"></div>
					<div class="buttons">
						<?php if (isset($data[3]['options']) && !empty($data[3]['options'])) :?>
						<?=join('<br>', $data[3]['options']);?>
						<?php endif; ?>
					</div>
					<div class="arrow-6"></div>
				</div>
				<div class="box-3-image2"><?=\Asset::img($data[3]['img']);?></div>
			</div>
			<div class="box-4">
				<div class="box-caption"><?=$data[4]['label']?></div>
				<div class="button-box">
					<?php if (isset($data[4]['options']) && !empty($data[4]['options'])) :?>
					<?=join('', $data[4]['options']);?>
					<?php endif; ?>
				</div>
			</div>
			<div class="arrow-7"></div>
			<div class="box-bottom">
				<div class="box-7">
					<div class="box-image"><?=\Asset::img($data[7]['img']);?></div>
					<div class="box-caption"><?=$data[7]['label'];?></div>
					<div class="button-box">
						<?php if (isset($data[7]['options']) && !empty($data[7]['options'])) :?>
						<?=join('', $data[7]['options']);?>
						<?php endif; ?>
					</div>
				</div>
				<div class="arrow-8"></div>
				<div class="box-6">
					<div class="box-image"><?=\Asset::img($data[6]['img']);?></div>
					<div class="box-caption"><?=$data[6]['label'];?></div>
					<div class="button-box">
						<?php if (isset($data[6]['options']) && !empty($data[6]['options'])):?>
						<?=join('', $data[6]['options']);?>
						<?php endif; ?>
					</div>
				</div>
				<div class="arrow-9"></div>
				<div class="box-5">
					<div class="box-image"><?=\Asset::img($data[5]['img']);?></div>
					<div class="box-caption"><?=$data[5]['label'];?></div>
					<div class="button-box">
						<?php if (isset($data[5]['options']) && !empty($data[5]['options'])):?>
						<?=join('', $data[5]['options']);?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="background-container"></div>
	</div>
	
	<!--- /Desktop View -->

	<!--- Mobile View -->
	<div class="mobile-container">
		<?php foreach ($data as $pos => $row) :?>
		<?php if ($pos > 7) break; ?>
		<div class="mobile-box">
			<div class="box-caption"><?=$row['label'];?></div>
			<?php if (isset($row['options']) && count($row['options']) > 0) :?>
			<div class="button-box">
				<?=join('', $row['options']);?>
			</div>
			<?php elseif(isset($row['options']) && count($row['options']) > 0) : ?>
				<?=join('', $row['options']);?>
			<?php endif; ?>

			<?php if ($pos < 7) :?>
			<div class="down-arrow"></div>
			<?php endif; ?>
		</div>
		<?php endforeach; ?>
	</div>
	<!--- /Mobile View -->
</div>

<style type="text/css">
.background-container:before {
    background-image: url('<?=\Asset::get_file($logo, 'img');?>');
}
</style>