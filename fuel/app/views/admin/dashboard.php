<div class="grid-x padding-top-0">
	<h2 class="small-10 cell">SOPs</h2>
	<p class="small-2 cell button" id="add_user">Add Account</p>
</div>
	<table id="datalist" class="show clickable">
		<thead>
			<tr>
				<th>Account #</th>
				<th>Sales Person</th>
				<th>Customer</th>
				<th>Division</th>
				<th>Template Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($users as $user): ?>
				<tr id="id_<?=$user['id'];?>" title="Click to edit <?=$user['customer'];?>">
					<td><?=$user['account'];?></td>
					<td><?=$user['sales_person'];?></td>
					<td><?=$user['customer'];?></td>
					<td><?=$user['division'];?></td>
					<td><?=$user['name'];?></td>
					<td><?=\Html::anchor('admin/users/' . $user['id'], 'Edit', array('class' => 'user-profile button small'));?> &nbsp; <?=\Html::anchor('#', 'Delete', array('class' => 'user-delete button small alert', 'id' => $user['id']));?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<div class="reveal tiny" id="dialog" data-reveal data-options="closeOnClick:false;"></div>
<script>
	$(document).ready(function() {
		$('.user-delete').on('click', function(e){
			e.preventDefault();
			$.get(baseUrl + 'admin/users/delete.html', {id: $(this).attr('id')}, function(html){
				$('#dialog').html(html).foundation('open');
			});
		});

		$('#dialog').on('click', '#delete_user', function(){
			var form = $(this).closest('form');
			$.post(baseUrl + 'admin/users/delete.json', form.serialize(), function(data){
				if (data.status === 'OK') {
					window.location.reload();
				}
			});
		});
		$('#datalist').DataTable({
			iDisplayLength: 50,
			responsive: true
		});
		$('#add_user').on('click', function() {
			$.get(baseUrl + 'admin/users/update.html', {id: 0}, function(html){
				$('#dialog').html(html).foundation('open');
			});
		});

		$('#dialog').on('click', '#update', function(){
			var form = $(this).closest('form');
			form.find('small.error').remove();
	    	form.find('input').removeClass('validation-error');
	    	form.find('label').removeClass('error');
			$.post(baseUrl + 'admin/users/save.json', form.serialize(), function(data){
				if (data.status === 'OK') {
					window.location.reload();
				} else {
					$.each(data.msg, function(k,v){
			            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
			            $('label[for="'+k+'"]').addClass('error');
			        });
				}
			});
		});
		$('.user-profile').on('click', function() {
            $('#loading_div').foundation('open');
        })
	});
</script>
