<div class="grid-x">
	<div class="small-12 medium-12 cell">
		<?=$pages;?>
	</div>
    <input type="hidden" name="userid" id="userid" value="<?= \Model\User::U_DEFAULT; ?>">
    <input type="hidden" name="template_id" id="template_id" value="<?= $pages->template_id; ?>">
</div>
<script>
    $(document).on('load', function() {
        $('#loading_div').foundation('open');
    });
    $(window).on('beforeunload', function() {
        $('#loading_div').foundation('open');
    });
    $(document).on('ready', function(){
        $('#loading_div').foundation('close');
    });
    $('.back').on('click', function(e){
        window.location.href = '/admin/templates';
    });
</script>
