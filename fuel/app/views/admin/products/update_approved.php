<div class="modal-header">
	<h5>Product Group</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'product_form', 'name' => 'product_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="sup_id">Title <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" name="data[title]" id="title" value="<?=$data['title']?>">
		</div>
	</div>

	<div class="grid-x cell" id="btn">
		<button type="button" class="button success" id="save_group" name="save">Save</button>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
	</div>
	<?=\Form::close();?>
</div>