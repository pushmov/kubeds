<div class="modal-header">
	<h5>Confirm Delete</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'product_form', 'name' => 'product_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
		<div class="grid-x">
			<div class="small-12 cell">
				<p>Are you sure want to delete this product data?</p>
			</div>
		</div>
		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button alert" id="delete_product" name="delete">Delete</button>
				<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>