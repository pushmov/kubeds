<div class="grid-x padding-top-0">
	<h2 class="small-10 cell">Products</h2>
	<p class="small-2 cell button" id="add_product">Add Product</p>
</div>
<div class="grid-x padding-top-0">
	<div class="small-12 medium-4 end cell">
		<label>Filter by Supplier:</label>
		<?=\Form::select('data[filter_supplier]', null, \Model\Supplier::forge()->filter_dropdown(), array('id' => 'filter_supplier'));?>
	</div>
</div>
<table id="dataproduct" class="show clickable cell-border grid-x-border dataTable">
	<thead>
		<tr>
			<th>Part Number</th>
			<th>Supplier</th>
			<th>Description</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<div class="reveal tiny" id="dialog" data-reveal data-options="closeOnClick:false;"></div>
<script>
	$(document).ready(function() {
		var dt = $('#dataproduct').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: baseUrl + '/admin/products/all.json',
				data: function(d) {
					d.supplier = $('#filter_supplier').val()
				}
			},
			"aoColumnDefs": [
				{ 'bSortable': false, 'aTargets': [ 4 ] }
			],
		});

		$('#filter_supplier').on('change', function(){
			dt.ajax.reload();
		});
		$('#add_product').on('click', function() {
			$.get(baseUrl + '/admin/products/update.html', {'id': 0}, function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});
		$('#dataproduct').on('click', 'button', function() {
			var action = $(this).data('action');
			$.get(baseUrl + '/admin/products/' + action + '.html', {'id': $(this).data('id')}, function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});
		$('#dialog').on('click', '#save_product', function(e) {
			var action = $(this).prop('id');
			$.post(baseUrl + '/admin/products/update.json', $('#product_form').serialize(), function(json) {
				$('#dialog .callout').remove();
				if (json.status == 'OK') {
					$('#btn').before('<div class="callout success">Saved</div>');
					//$('#dialog').foundation('close');
					dt.ajax.reload();
				} else {
					$('#btn').before('<div class="callout alert">' + json.msg + '</div>');
				}
			});
		});
	});
</script>