<div class="modal-header">
	<h5>Add Approved Product</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'product_form', 'name' => 'product_form', 'method' => 'post', 'action' => ''), array('data[group_id]' => $data['group_id'], 'data[id]' => null))?>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="sup_id">Product Number<span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" name="data[product_name]" id="product_name" value="">
			<input type="hidden" name="data[product_id]" id="product_id" value="<?=$data['product_id']?>">
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="sup_id">Comment</label>
		</div>
		<div class="small-9 cell error">
			<textarea name="data[comments]" cols="20" id="comments"><?=$data['comments']?></textarea>
		</div>
	</div>

	<div class="grid-x cell" id="btn">
		<button type="button" class="button success" id="save_product" name="save">Save</button>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
	</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
$('#product_name').easyAutocomplete({
	url: baseUrl + 'admin/products/autocomplete.json',
	getValue: function(element){
		return element.name;
	},
	list: {
		match: {enabled: true},
		onChooseEvent: function() {
			var value = $("#product_name").getSelectedItemData().id;
			$('#product_id').val(value);
		}
	}
});
</script>