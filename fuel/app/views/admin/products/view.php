<div class="modal-header">
	<h5>View Product</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="sup_id">Supplier :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[sup_id]" id="sup_id" value="<?=$data['supplier_name'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="sup_id">Supplier Site:</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[sup_id]" id="sup_id" value="<?=$data['supplier_site'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="part_num">Part Number :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[part_num]" id="part_num" value="<?=$data['part_num'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="description">Description :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[description]" id="description" value="<?=$data['description'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="status_id">Status :</label>
		</div>
		<div class="small-9 cell">
			<?=\Form::select('data[status_id]', $data['status_id'], \Model\Product::forge()->get_status_array(), array('id' => 'status_id', 'disabled' => 'disabled'))?>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_at">Created At :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_at]" id="created_at" value="<?=$data['created_at'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_at">Created By :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_by]" id="created_by" value="<?=$data['created_first_name'].' '.$data['created_last_name'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_at">Updated At :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_at]" id="updated_at" value="<?=$data['updated_at'];?>" disabled="disabled"/>
		</div>
	</div>

	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_at">Updated By :</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_by]" id="updated_by" value="<?=$data['updated_first_name'].' '.$data['updated_last_name'];?>" disabled="disabled"/>
		</div>
	</div>
	
	<div class="grid-x cell" id="btn">
		<div class="float-left">
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</div>
</div>