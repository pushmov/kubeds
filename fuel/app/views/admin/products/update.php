<div class="modal-header">
	<h5><?=$title?></h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'product_form', 'name' => 'product_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="part_num">Part Number <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" name="data[part_num]" id="part_num" class="" value="<?=$data['part_num']?>">
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="sup_id">Supplier <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" name="sup_autocomplete" id="sup_id" class="" value="<?=$data['supplier_name']?>" autocomplete="off">
			<input type="hidden" name="data[sup_id]" id="sup_id_val" value="<?=$data['sup_id']?>">
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="part_num">Description <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" name="data[description]" id="description" class="" value="<?=$data['description']?>">
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="status_id">Status <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<?=\Form::select('data[status_id]', $data['status_id'], \Model\Product::forge()->get_status_array(), array('id' => 'status_id'))?>
		</div>
	</div>

	<div class="grid-x cell" id="btn">
		<button type="button" class="button success" id="save_product" name="save">Save</button>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
	</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$('#sup_id').easyAutocomplete({
		url: baseUrl + 'admin/suppliers/autocomplete.json',
		getValue: function(element){
			return element.name;
		},
		list: {
			match: {enabled: true},
			onChooseEvent: function() {
				var value = $("#sup_id").getSelectedItemData().id;
				$('#sup_id_val').val(value);
			}
		}
	});
</script>