<div class="modal-header">
	<h5>Reset Password</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<form id="user_form" name="user_form" method="post" action="">
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="password">Password <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
                <input type="password" name="data[password]">
			</div>
		</div>
		<div class="grid-x cell" id="btn">
			<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
			<button type="button" class="button success" id="reset_pwd" name="reset_pwd">Save</button>
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</form>
</div>