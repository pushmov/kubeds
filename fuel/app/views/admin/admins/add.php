<div class="modal-header">
	<h5>Add Admin</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<form id="user_form" name="user_form" method="post" action="">
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="email">Email <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<input type="email" name="data[email]" value="" id="email" >
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="passwrd">Password <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<input type="password" name="data[passwrd]" value="" id="passwrd" >
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="first_name">First Name</label>
			</div>
			<div class="small-9 cell">
				<input type="text" name="data[first_name]" value="" id="first_name" >
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="last_name">Last Name</label>
			</div>
			<div class="small-9 cell">
				<input type="text" name="data[last_name]" value="" id="last_name" >
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="status_id">Role <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
                <?=\Form::select('data[role_id]', null, \Model\Admin::forge()->get_role(), array('id' => 'role_id'));?>
			</div>
		</div>
		<div class="grid-x cell" id="btn">
			<input type="hidden" name="data[id]" id="id" value="">
			<button type="button" class="button success" id="update" name="update">Save</button>
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</form>
</div>