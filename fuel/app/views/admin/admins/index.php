<div class="grid-x padding-top-0">
	<h2 class="small-10 cell">Admins</h2>
	<p class="small-2 cell button" id="add_user">Add Admin</p>
</div>
	<table id="datalist" class="show clickable">
		<thead>
			<tr>
				<th>Email</th>
				<th>Name</th>
				<th>Role</th>
				<th>Last Login</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($admins as $admin): ?>
				<tr id="<?php echo $admin['id'];?>" title="Click to edit">
					<td><?=$admin['email']?></td>
					<td><?=$admin['name']?></td>
					<td><?=$admin['role']?></td>
					<td><?=$admin['date'];?></td>
					<td><?=$admin['action']?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<div class="reveal tiny" id="dialog" data-reveal data-options="closeOnClick:false;"></div>
<script type="text/javascript">
	$('#datalist').DataTable({
		iDisplayLength: 50,
		responsive: true
	});

	$('.admin-edit').on('click', function(e){
		e.preventDefault();
		$.get(baseUrl + 'admin/admins/edit.html', {id : $(this).attr('id')}, function(html){
			$('#dialog').html(html);
			$('#dialog').foundation('open');
		});
	});

	$('#dialog').on('click', '#update', function(e){
		e.preventDefault();
		var form = $(this).closest('form');
		form.find('small.error').remove();
	    form.find('input').removeClass('validation-error');
	    form.find('label').removeClass('error');
		$('#dialog .callout').remove();
		$.post(baseUrl + 'admin/admins/edit.json', form.serialize(), function(data){
			if (data.status == 'OK') {
				$('#btn').before('<div class="callout success">Saved</div>');
			} else {
				$.each(data.msg, function(k,v){
		            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
		            $('label[for="'+k+'"]').addClass('error');
		        });
			}
		});
	});

	$('#dialog').on('click', '#reset', function(){
		$.get(baseUrl + 'admin/admins/reset.html', {id: $(this).attr('data-id')}, function(html){
			$('#dialog').html(html);
		});
	});

	$('#dialog').on('click', '#reset_pwd', function(){
		var form = $(this).closest('form');
		$('#dialog .callout').remove();
		$.post(baseUrl + 'admin/admins/reset.json', form.serialize(), function(data){
			if (data.status == 'OK') {
				$('#btn').before('<div class="callout success">Saved</div>');
			} else {
				$('#btn').before('<div class="callout alert">Error</div>');
			}
		});
	});

	$('#add_user').on('click', function(e){
		e.preventDefault();
		$.get(baseUrl + 'admin/admins/add.html', function(html){
			$('#dialog').html(html);
			$('#dialog').foundation('open');
		});
	});

</script>
