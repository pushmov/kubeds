<div class="modal-header">
	<h5>Edit Admin</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<form id="user_form" name="user_form" method="post" action="">
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="status_id">Status <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
                <?=\Form::select('data[status_id]', $data['status_id'], \Model\Admin::forge()->get_status(), array('id' => 'status_id'));?>
			</div>
		</div>
		<div class="grid-x cell" id="btn">
			<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
			<button type="button" class="button success" id="update" name="update">Save</button>
			<button type="button" class="button warning float-right" id="reset" data-id="<?=$data['id']?>" name="reset">Reset Password</button>
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</form>
</div>