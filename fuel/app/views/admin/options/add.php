<div class="modal-header">
	<h5>Add Option</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'options_form', 'name' => 'options_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id'], 'data[step_id]' => $step))?>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="type_id">Option Type <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<?=\Form::select('data[type_id]', \Model\Option::T_PROCESS, \Model\Option::forge()->get_type_array(), array('id' => 'type_id'));?>
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="title">Title <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<input type="text" class="" name="data[title]" id="title" value="<?=$data['title'];?>"/>
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="external_url">External URL</label>
			</div>
			<div class="small-9 cell">
				<input type="text" class="" name="data[external_url]" id="external_url" value="<?=$data['external_url'];?>"/>
			</div>
		</div>

		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button success" id="add_options" name="save">Save</button><button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$('#add_options').on('click', function(){
		var form = $(this).closest('form');
		form.find('small.error').remove();
      	form.find('input').removeClass('validation-error');
      	form.find('label').removeClass('error');
		var data = form.serializeArray();
		data.push({name: 'data[user_id]', value: $('#userid').val()});
		$.post(baseUrl + 'admin/options/add.json', $.param(data), function(data){
			if (data.status === 'OK') {
				window.location.hash = data.hash;
				window.location.reload();
			} else {
				$.each(data.msg, function(k,v){
		            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
		            $('label[for="'+k+'"]').addClass('error');
		        });
			}
		});
	});
</script>