<form id="user_form" name="user_form" method="post" action="">
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="account">User ID <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[account]" id="account" value="<?=$data['account'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="email">Email <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[email]" id="email" value="<?=$data['email'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="bus_name">Company <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell">
			<?=\Form::select('data[bus_name]', $data['bus_name'], \Model\User::forge()->companies(), array('id' => 'bus_name', 'disabled' => 'disabled'));?>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="status_id">Status</label>
		</div>
		<div class="small-9 cell">
			<?= \Form::select('data[status_id]', $data['status_id'], \Model\User::forge()->get_status_array(), array('id' => 'status_id', 'disabled' => 'disabled')); ?>
		</div>
	</div>
	<div class="grid-x cell" id="btn">
		<div class="float-left">
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</div>
</form>
