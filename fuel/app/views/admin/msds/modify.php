<div class="modal-header">
	<h5><?=$title?></h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'msds_form', 'name' => 'msds_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id'], 'data[option]' => $data['option']))?>
		

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="part_num">Part Number <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[part_num]" id="part_num" value="<?=$data['part_num'];?>"/>
			</div>
		</div>

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="supplier_name">Supplier <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="supplier_name" id="supplier_name" value="<?=$data['supplier_name'];?>" disabled="disabled"/>
				<input type="hidden" name="data[sup_id]" id="supplier_value" value="<?=$data['sup_id']?>">
			</div>
		</div>

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="description">Display Title <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[description]" id="description" value="<?=$data['description'];?>"/>
			</div>
		</div>

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="url">Target Url <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[url]" id="url" value="<?=$data['url'];?>"/>
			</div>
		</div>

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="status_id">Status <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<?=\Form::select('data[status_id]', $data['status_id'], \Model\Msds::forge()->get_status_array(), array('id' => 'status_id'));?>
			</div>
		</div>
		
		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button" id="save_msds" name="save">Save</button>
				<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#part_num').easyAutocomplete({
			url: baseUrl + 'admin/product/autocomplete.json',
			getValue: function(element){
				return element.name;
			},
			list: {
				match: {enabled: true},
				onClickEvent: function(){
					var supid = $('#part_num').getSelectedItemData().sup_id;
					var supname = $('#part_num').getSelectedItemData().sup_name;
					$('#supplier_value').val(supid);
					$('#supplier_name').val(supname).prop('disabled', true);
				},
				onKeyEnterEvent: function(){
					var supid = $('#part_num').getSelectedItemData().sup_id;
					var supname = $('#part_num').getSelectedItemData().sup_name;
					$('#supplier_value').val(supid);
					$('#supplier_name').val(supname).prop('disabled', true);
				}
			}
		});
	});
</script>