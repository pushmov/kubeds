<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Kube Administration</title>
    <?= \Asset::css('app.css'); ?>
    <?= \Asset::css('style.css'); ?>
    <?= \Asset::css('loader.css'); ?>
    <?= \Asset::css('foundation-icons.css') ?>
    <?= \Asset::css($styles, array(), null, false); ?>
    <?= \Asset::js('jquery-3.2.1.min.js');?>
    <?= \Asset::js('foundation.min.js'); ?>
    <script>var baseUrl = '<?=\Uri::base(false);?>';</script>
    <?= \Asset::js($scripts, array(), null, false); ?>
</head>
<body>
<div class="grid-container">
    <header id="header" class="grid-x">
        <div class="small-12 medium-4 cell">
            <?= \Html::anchor('/', \Asset::img('kube-logo.png', array('alt' => 'Kube DS'))); ?>
        </div>
        <div class="small-12 medium-4 cell">
        </div>
        <div class="small-12 medium-4 cell">
        </div>
    </header>
    <?php if ($logged_in): ?>
        <div class="grid-x">
            <ul class="menu cell">
                <?= $menu; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?= $content; ?>
    <footer id="footer" class="grid-x">
        <div class="small-12 medium-6 cell">
            &copy;Color Compass Corporation
        </div>
        <div class="small-12 medium-6 cell text-right">
            <?php if ($logged_in): echo \Html::anchor('/admin/login/logout', 'Logout', array('class' => 'button')); endif; ?>
        </div>
    </footer>
</div>
<div id="loading_div" class="reveal" data-reveal data-close-on-click="false" data-close-on-esc="false"></div>
<script>
    $(document).foundation();
    $('#documents').on('click', function(e){
        e.preventDefault();
        window.open('/kcfinder/browse.php', 'kcfinder_single', 'location=yes,height=570,width=800,scrollbars=yes,status=yes');
    });
</script>
</body>
</html>