<div class="grid-x cell">
	<h5 class="small-12 cell">Select Module:</h5>
</div>
<div class="grid-x cell">
	<div class="large-10 cell">
		<select class="select2" id="select_modules">
			<option data-href="" value="" selected="selected">Select Module</option>
			<optgroup label="Users & Companies">
				<option data-href="/admin/user/index.html" value="users">Users</option>
				<option data-href="/admin/user/admin.html" value="admins">Admins</option>
				<option data-href="/admin/company/index.html" value="companies">Companies</option>
			</optgroup>
			<optgroup label="Headings, MSDS, Tech Support, Procedures & Processes">
				<option data-href="/admin/heading/index.html" value="headings">Headings</option>
				<option data-href="/admin/procedure/index.html" value="procedures">Procedures</option>
				<option data-href="/admin/process/index.html" value="processes">Processes</option>
				<option data-href="/admin/msds/index.html" value="msds">MSDS</option>
				<option data-href="/admin/support/index.html" value="support">Technical Support</option>
			</optgroup>
			<optgroup label="Products & Suppliers">
				<option data-href="/admin/product/index.html" value="products">Products</option>
				<option data-href="/admin/product/group/index.html" value="product_groups">Product Groups</option>
				<option data-href="/admin/product/approved/index.html" value="product_approveds">Approved Products</option>
				<option data-href="/admin/supplier/index.html" value="suppliers">Suppliers</option>
			</optgroup>
		</select>
  	</div>
  	<div class="large-2 cell">
  		<button class="button btn-show" type="button" id="show">Show</button>
  	</div>
</div>
<div class="grid-x cell admin-content" id="content"></div>
<div class="reveal large" id="modal_modules" data-reveal data-options="closeOnClick:false;"></div>