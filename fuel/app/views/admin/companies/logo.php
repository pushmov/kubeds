<div class="modal-header">
	<h5>Company Logo</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'company_form', 'name' => 'company_form', 'method' => 'post', 'action' => ''), array('data[id]' => $id))?>
		<div class="grid-x">
			<div class="small-12 text-center cell" id="c_logo">
				<?=$logo?>
				<div class="text-center" id="upload_logo"></div>
			</div>
		</div>
		<br>
		<div class="grid-x cell" id="btn">
			<div class="text-center">
				<button type="button" class="button success" id="btn_upload" name="btn_close">Change Logo</button>
				<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$('#upload_logo').uploadFile({
		url: baseUrl + 'admin/companies/logo.json',
		fileName:'company_logo',
		formData: {id: $('[name="data[id]"]').val()},
		returnType: 'json',
		allowedTypes: 'jpg,jpeg,png',
		acceptFiles: 'image/',
		dragdropWidth: '100%',
		statusBarWidth: '100%',
		onSelect: function(){
			$('#btn_upload').prop('disabled', true);
		},
		onSuccess:function(files,data,xhr,pd) {
			$('#btn_upload').prop('disabled', false);
		    if (data.img) {
		    	$('#c_logo > img').attr('src', data.img +'?'+ new Date().getTime());
		    }
		}
	});

	$('#btn_upload').on('click', function(){
		$('#upload_logo').find('input').trigger('click');
	});
</script>