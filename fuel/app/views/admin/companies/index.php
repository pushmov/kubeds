<div class="grid-x padding-top-0">
	<h2 class="small-10 cell">Companies</h2>
	<p class="small-2 cell button" id="add_company">Add Company</p>
</div>
<table id="datacompany" class="show clickable cell-border grid-x-border dataTable">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<div class="reveal tiny" id="dialog" data-reveal data-options="closeOnClick:false;"></div>
<script>
	$(document).ready(function() {
		var dt = $('#datacompany').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: baseUrl + '/admin/companies/all.json'
			},
			"aoColumnDefs": [
				{ 'bSortable': false, 'aTargets': [ 3 ] }
			],
		});
		$('#add_company').on('click', function() {
			$.get(baseUrl + '/admin/companies/update.html', {'id': 0}, function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});
		$('#datacompany').on('click', 'button', function() {
			var action = $(this).data('action');
			$.get(baseUrl + '/admin/companies/' + action + '.html', {'id': $(this).data('id')}, function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});
		$('#dialog').on('click', '#btn_save', function(e) {
			var action = $(this).prop('id');
			$.post(baseUrl + '/admin/companies/update.json', $('#company_form').serialize(), function(json) {
				$('#dialog .callout').remove();
				if (json.status == 'OK') {
					$('#btn').before('<div class="callout success">Saved</div>');
					dt.ajax.reload();
				} else {
					$('#btn').before('<div class="callout alert">' + json.msg + '</div>');
				}
			});
		});
	});
</script>