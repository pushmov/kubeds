<div class="modal-header">
	<h5>View Company</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'company_form', 'name' => 'company_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="name">Name <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[name]" id="name" value="<?=$data['name'];?>" disabled="disabled" />
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="status_id">Status <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<?=\Form::select('data[status_id]', $data['status_id'], \Model\Template::forge()->get_status_array(), array('id' => 'status_id', 'disabled' => 'disabled'));?>
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="created_by">Created by <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[created_by]" id="created_by" value="<?=$data['created_first_name'] . ' ' . $data['created_last_name'];?>" disabled="disabled" />
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="created_at">Created at <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[created_at]" id="created_at" value="<?=$data['created_at'];?>" disabled="disabled" />
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="updated_by">Updated by <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[updated_by]" id="updated_by" value="<?=$data['updated_first_name'] . ' ' . $data['updated_last_name'];?>" disabled="disabled" />
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="updated_at">Updated at <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="data[updated_at]" id="updated_at" value="<?=$data['updated_at'];?>" disabled="disabled" />
			</div>
		</div>
		
		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>