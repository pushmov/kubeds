<div class="modal-header">
	<h5>URL</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<div class="grid-x">
		<code><?=\Uri::base(false).$data['file_name']?></code>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
	</div>
</div>