<?php if ( !empty($options) ) : ?>
<div class="grid-x">
	<?php if (in_array($data->id, \Model\User::user_default_template())) : ?>
	<div class="small-3 cell">
		<label for="template_id">Clone Template <span class="astrict">*</span></label>
	</div>
	<div class="small-9 cell">
		<?=\Form::select('data[template_id]', ($user !== null) ? $user->template_id : null, $options, array('id' => 'template_id'))?>
	</div>
	<?php else : ?>
	<div class="small-3 cell">
		<label for="template_id">Select Template <span class="astrict">*</span></label>
	</div>
	<div class="small-9 cell">
		<?=\Form::select('data[template_id]', ($user !== null) ? $user->template_id : null, $options, array('id' => 'template_id'))?>
	</div>
	<?php endif; ?>
</div>
<?php endif; ?>