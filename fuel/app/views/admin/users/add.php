<div class="modal-header">
	<h5>Add Account</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<form id="user_form" name="user_form" method="post" action="">
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="account">Account ID <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<input type="text" class="" name="data[account]" id="account" value="<?=$data['account'];?>" placeholder="Account ID"/>
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="customer">Company <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
				<input class="" name="data[customer]" id="customer" value="" placeholder="Account Company" type="text">
			</div>
		</div>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="email">Email <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
                <input type="text" class="" name="data[email]" id="email" value="<?=$data['email'];?>" placeholder="Account Email"/>
			</div>
		</div>

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="passwrd">Password <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell">
                <input type="password" class="" name="passwrd" id="passwrd" placeholder="Account Password" />
			</div>
		</div>
        <div class="grid-x">
            <div class="small-3 cell">
                <label for="template" id="user_template">Use Template</label>
            </div>
            <div class="small-9 cell">
                <?= \Form::select('data[template_id]', $data['template_id'], $cboTemplates); ?>
            </div>
        </div>
		<div class="grid-x cell" id="btn">
			<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
			<button type="button" class="button success" id="update" name="update">Save</button>
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
		</div>
	</form>
</div>
<script>
	$(document).ready(function() {
		$('#new_company').on('click', function(e){
			e.preventDefault();
			$('#company_select').addClass('hide');
			$('#company_input').removeClass('hide');
		});

		$('#company').on('keyup, change', function(){
			$('#company_val').val(0);
		});

		$('#customer').easyAutocomplete({
			url: baseUrl + 'admin/companies/autocomplete.json',
			getValue: function(element){
				return element.name;
			},
			list: {
				match: {enabled: true},
				onClickEvent: function(){
					$('#customer').removeClass('validation-error').parent().find('small.error').remove();
					loadTemplate();
				},
				onKeyEnterEvent: function(){
					$('#customer').removeClass('validation-error').parent().find('small.error').remove();
					loadTemplate();
				}
			}
		});
	});

	function loadTemplate() {
		$.get(baseUrl + 'admin/users/template.html', {name: $('#name').val()}, function(html){
			$('#user_template').html(html);
		});
	}
	//# sourceURL=dialoguser.php
</script>