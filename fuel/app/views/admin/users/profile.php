<div class="grid-x grid-padding-x padding-top-0">
    <h3>Profile</h3>
    <form id="user_form" name="user_form" method="post" action="" class="cell">
        <input type="hidden" id="userid" name="data[id]" value="<?= $data['id'] ?>">
        <div class="grid-x">
            <div class="small-3 cell">
                <label for="account"><?=$data['account_label'];?></label>
            </div>
            <div class="small-9 cell">
                <input type="text" class="" name="data[account]" id="account" value="<?= $data['account']; ?>" readonly/>
            </div>
        </div>
        <div class="grid-x">
            <div class="small-3 cell">
                <label for="email">Email</label>
            </div>
            <div class="small-9 cell">
                <input type="text" class="" name="data[email]" id="email" value="<?= $data['email']; ?>" readonly/>
            </div>
        </div>
        <?php if ($data['template_id'] == 1): ?>
            <div class="grid-x">
                <div class="small-3 cell">
                    <label for="customer">Company</label>
                </div>
                <div class="small-9 cell">
                    <input type="text" class="" name="data[customer]" value="<?= $template['name'] ?>" readonly>
                </div>
            </div>
        <?php else: ?>
            <div class="grid-x">
                <div class="small-3 cell">
                    <label for="customer">Company</label>
                </div>
                <div class="small-9 cell">
                    <input type="text" class="" name="data[customer]" id="customer" value="<?= $template['name'] ?>" readonly>
                </div>
            </div>
            <div id="user_template">
                <div class="grid-x">
                    <div class="small-3 cell">
                        <label for="status_id">Template</label>
                    </div>
                    <div class="small-9 cell">
                        <?= \Form::select('data[template_id]', $data['template_id'], \Model\User::forge()->user_template(), array('id' => 'template_id', 'disabled' => 'disabled')); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        
        <div class="grid-x cell" id="btn">
            <button type="button" class="button success" id="btn_edit" name="btn_edit">Edit</button>
            <button type="button" class="button warning hide" id="btn_save" name="btn_save">Update</button>
            <button type="button" class="button" id="btn_passwrd" name="btn_passwrd">Reset Password</button>
            <?php if (!array_key_exists($data['id'], \Model\User::forge()->user_template())) :?>
            <button type="button" class="button alert" id="btn_delete" name="btn_delete">Delete</button>
            <?php endif; ?>
        </div>
    </form>

    <form id="form_thumbnail" name="thumbnail_form" method="post" action="" class="cell">
        <div class="grid-x">
            <div class="small-12 cell">
                <h3>Company Logo</h3>
                <div class="product-update">
                    <button type="button" id="add_thumbnail" class="button small">Add Company Logo</button>
                    <button type="button" id="delete_thumbnail" class="button small alert hide">Delete</button>
                </div>

                <?php if (!empty($logos)) :?>
                <div class="thumbnail">
                    <?php foreach ($logos as $logo) :?>
                    <div class="item">
                        <?=\Asset::img( \Uri::base(false) . $logo['asset_file_name'])?>
                        <div>
                            <input type="checkbox" name="thumbnails[]" value="<?=$logo['id']?>" class="float-right delete-thumbnail">
                            <button type="button" data-id="<?=$logo['id']?>" class="button warning tiny float-left show-url">Show URL</button>
                        </div>
                        
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var hash = '';
    if (window.location.hash) {
        hash = window.location.hash;
    }

    $('#add_thumbnail').on('click', function(e){
        window.KCFinder = {};
        window.KCFinder.callBack = function(url) {
            var filename = url.replace(/.*(\/|\\)/, '');
            var data = {};
            data.hash = hash;
            data.user_id = $('#userid').val();
            data.file_name = url;
            $.post(baseUrl + 'admin/users/logo.json', data, function(d){
                if (d.status === 'OK') {
                    window.location.hash = d.hash;
                    window.location.reload();
                }
            });
            window.KCFinder = null;
        };
        window.open('/kcfinder/browse.php', 'kcfinder_single', 'location=yes,height=570,width=800,scrollbars=yes,status=yes');
    });

    $('#delete_thumbnail').on('click', function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        $.post(baseUrl + 'admin/users/delete_logo.json', form.serialize(), function(data){
            if (data.status === 'OK') {
                window.location.hash = data.hash;
                window.location.reload();
            }
        });
    });
    $('.thumbnail .item').find('input[type="checkbox"]').prop('checked', false);
    $('.thumbnail .item').on('click', function(){
        $(this).toggleClass('selected');
        if ($(this).hasClass('selected')) {
            $(this).find('input[type="checkbox"]').prop('checked', true);
        } else {
            $(this).find('input[type="checkbox"]').prop('checked', false);
        }
        if ($('.thumbnail .item.selected').length > 0) {
            $('#delete_thumbnail').removeClass('hide');
        } else {
            $('#delete_thumbnail').addClass('hide');
        }
    });

    $('#btn_save').on('click', function () {
        var form = $(this).closest('form');
        form.find('.callout').remove();
        form.find('small.error').remove();
        form.find('input').removeClass('validation-error');
        form.find('label').removeClass('error');
        $.post(baseUrl + 'admin/users/save.json', form.serialize(), function (data) {
            if (data.status === 'OK') {
                $('#btn').before('<div class="callout success">' + data.msg + '</div>');
                $('#btn_save').addClass('hide');
            } else {
                $.each(data.msg, function (k, v) {
                    $('#' + k).addClass('validation-error').after('<small class="error">' + v + '</small>');
                    $('label[for="' + k + '"]').addClass('error');
                });
            }
            form.find('input').each(function(){
                $(this).prop('readonly', true);
            });
            $('#btn_edit').removeClass('hide');
        });
    });

    $('#customer').easyAutocomplete({
        url: baseUrl + 'admin/companies/autocomplete.json',
        getValue: function (element) {
            return element.name;
        },
        list: {
            match: {enabled: true},
            onClickEvent: function () {
                $('#customer').removeClass('validation-error').parent().find('small.error').remove();
                $('label[for="customer"]').removeClass('error');
                loadTemplate();
            },
            onKeyEnterEvent: function () {
                $('#customer').removeClass('validation-error').parent().find('small.error').remove();
                $('label[for="customer"]').removeClass('error');
                loadTemplate();
            }
        }
    });

    function loadTemplate() {
        $.get(baseUrl + 'admin/users/template.html', {name: $('#name').val(), userid: $('#userid').val()}, function (html) {
            $('#user_template').html(html);
            
        });
    }

    $('#btn_passwrd').on('click', function () {
        $.get(baseUrl + 'admin/users/password.html', {name: $('#name').val(), userid: $('#userid').val()}, function (html) {
            $('#modal_modules').html(html);
            $('#modal_modules').foundation('open');
        });
    });
    $('#reset_password').on('click', function () {
        $.post(baseUrl + 'admin/users/reset.json', form.serialize(), function (data) {
            if (data.status === 'OK') {
                $('#btn').before('<div class="callout success">' + data.msg + '</div>')
            } else {
                $.each(data.msg, function (k, v) {
                    $('#' + k).addClass('validation-error').after('<small class="error">' + v + '</small>');
                    $('label[for="' + k + '"]').addClass('error');
                });
            }
        });
    });
    $('#customer').on('change, keyup', function () {
        if ($(this).val() === '') {
            $('#user_template').html('');
        }
    });

    $('.show-url').on('click', function(e){
        $.get(baseUrl + 'admin/users/logo.html', {id: $(this).data('id')}, function(html){
            $('#modal_modules').html(html);
            $('#modal_modules').foundation('open');
        });
    });

    $('#btn_delete').on('click', function(e){
        $.get(baseUrl + 'admin/users/delete.html', {id: $('#userid').val()}, function(html){
            $('#modal_modules').html(html);
            $('#modal_modules').foundation('open');
        });
    });

    $(document).ready(function(){
        $('#modal_modules').on('click', '#delete_user', function(e){
            var form = $(this).closest('form');
            $.post(baseUrl + 'admin/users/delete.json', form.serialize(), function(json){
                if (json.status == 'OK') {
                    //window.location.href = baseUrl + 'admin';
                } else {
                    $('#modal_modules').foundation('close');
                }
            });
        });
    });

    $('#btn_edit').on('click', function(){
        $('#user_form input').each(function(){
            $(this).removeAttr('readonly');
        });
        $('#btn_save').removeClass('hide');
        $(this).addClass('hide');
    });
</script>