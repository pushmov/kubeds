<div class="grid-x grid-padding-x padding-top-0">
	<h3 class="small-12 medium-6">Pages</h3>
	<div class="small-12 medium-6 text-right sops-btn">
		<button type="button" class="button back">Back</button>
		<button type="button" id="view_book" class="button">View Book</button>
		<button type="button" href="<?=$print_href?>" class="button warning" id="print">Print</button>
	</div>
	
	<ul class="accordion cell" data-accordion>
		<?php foreach ($steps as $row) : ?>
			<li class="accordion-item accordion-step" data-accordion-item>
				<a href="#" data-href="href-step-<?= $row['id'] ?>" class="accordion-title accordion-title-step"><?= $row['title']; ?></a>
				<div class="accordion-content" data-tab-content>
					<?php if ($row['status_id'] == \Model\Step::S_UNUSED) : ?>
						<button disabled="disabled" class="button tiny edit-page" data-id="<?= $row['id'] ?>">Edit</button><button class="button tiny alert step-enable" data-id="<?= $row['id'] ?>" title="Click to Enable">Disabled</button>
					<?php else : ?>
						<button class="button tiny edit-page" data-id="<?= $row['id'] ?>">Edit</button><button class="button tiny success step-disable" data-id="<?= $row['id'] ?>" title="Click to Disable">Enabled</button>
						<?php if ($row['status_id'] == \Model\Step::S_CUSTOM) : ?>
							<button class="button tiny warning step-reset" data-id="<?= $row['id'] ?>" title="Click to Revert">Revert</button>
						<?php endif; ?>
					<?php endif; ?>

					<button <?= ($row['status_id'] == \Model\Step::S_UNUSED) ? 'disabled="disabled"' : '' ?> class="button tiny warning" title="Click to Add an Option" id="add_option" data-id="<?= $row['id'] ?>">Add</button>
					<button type="button" class="button tiny float-right icons" data-id="<?= $row['id'] ?>">Icon</button>
					<ul id="accordion-options" class="accordion" data-accordion="accordionOptions">
						<?php foreach ($options as $row2) : ?>
							<?php if ($row2['step_id'] == $row['id'] || $row2['step_id'] == $row['parent_id']) : ?>
								<li class="accordion-item accordion-option <?= ($row['status_id'] == \Model\Step::S_UNUSED) ? 'disabled' : '' ?>" data-accordion-item>
									<a href="#" data-href="href-option-<?= $row2['id'] ?>" class="accordion-title"><?= $row2['title']; ?>
										<div class="accordion-title-direction">
											<?php if ($row2['pos'] > 1) : ?>
											<span title="Up" data-id="<?= $row2['id'] ?>" data-direction="up.json" data-step="<?= $row2['step_id'] ?>" class="option-move fi-arrow-up" data-pos="<?= $row2['pos'] ?>"></span>
											<?php endif; ?>
											<span title="Down" data-id="<?= $row2['id'] ?>" data-direction="down.json" class="option-move fi-arrow-down" data-step="<?= $row2['step_id'] ?>" data-pos="<?= $row2['pos'] ?>"></span>
										</div>
									</a>

									<div class="accordion-content" data-tab-content>
										<?php if ($row['status_id'] != \Model\Step::S_UNUSED) : ?>

											<?php if ($row2['status_id'] == \Model\Option::S_UNUSED) : ?>
												<button disabled="disabled" class="button tiny edit-option" data-id="<?= $row2['id'] ?>">Edit</button><button class="button tiny alert option-enable" title="Click to Enable" data-id="<?= $row2['id'] ?>">Disabled</button>
											<?php else : ?>
												<?php if ($row2['type_id'] <= \Model\Option::T_SOP) : ?>
												<button class="button tiny edit-option" data-id="<?= $row2['id'] ?>">Edit</button><?php endif; ?><button class="button tiny success option-disable" title="Click to Disable" data-id="<?= $row2['id'] ?>">Enabled</button>
												<?php if ($row2['status_id'] == \Model\Option::S_CUSTOM && \Model\Option::forge()->is_allow_revert($row2['id'])) : ?>
													<button class="button tiny warning option-reset" data-id="<?= $row2['id'] ?>" title="Click to Revert">Revert</button>
												<?php endif; ?>
											<?php endif; ?>

											<?php if ($row2['type_id'] == \Model\Option::T_PRODUCT) : ?>
											<div id="product_upload_<?= $row2['id'] ?>" class="product-update" data-option="<?= $row2['id'] ?>"></div>
											<script type="text/javascript">
												$('#product_upload_<?= $row2['id'] ?>').uploadFile({
													url: baseUrl + 'admin/user/product/update.json',
													fileName:'document',
													uploadStr: 'Update',
													autoSubmit: true,
													allowedTypes: 'xls,csv,xlsx',
													returnType: 'json',
													maxFileCount: 1,
													showStatusAfterSuccess: false,
													showError: false,
													showProgress: false,
													formData: {user_id: $('#userid').val(), option_id: $('#product_upload_<?= $row2['id'] ?>').attr('data-option')},
													onSuccess: function(files, response, xhr, pd){
														if (response.status === 'OK') {
															window.location.hash = response.hash;
															window.location.reload();
														}
													}
												});
											</script>
											<?php endif; ?>
											
											<div class="grid-x-content">
												<?php $alt = \Str::alternator('odd', 'even'); ?>
												<?php foreach ($content as $row3) : ?>
													<?php if ($row3['option_id'] == $row2['id'] || $row3['option_id'] == $row2['parent_id']) : ?>
														<div class="grid-x <?= $alt(); ?>">
															<div class="small-8 cell">
																<?= $row3['title']; ?>
															</div>
															<div class="small-3 cell">
																<?php if ($row2['status_id'] == \Model\Option::S_UNUSED) : ?>
																	<button class="button tiny btn-disabled" disabled="disabled">Edit</button>
																	<button class="button tiny btn-disabled" disabled="disabled">Disabled</button>
																<?php else : ?>
																	<button <?= ($row3['status_id'] == \Model\Content::S_UNUSED) ? 'disabled="disabled"' : '' ?> class="button tiny edit-content" data-id="<?= $row3['id'] ?>" data-option="<?= $row2['id'] ?>">Edit</button>
																	<?php if ($row3['status_id'] == \Model\Content::S_UNUSED) : ?>
																		<button class="button tiny alert content-enable" title="Click to Enable" data-id="<?= $row3['id'] ?>">Disabled</button>
																	<?php else : ?>
																		<button class="button tiny success content-disable" title="Click to Disable" data-id="<?= $row3['id'] ?>">Enabled</button>
																		<?php if ($row3['status_id'] == \Model\Content::S_CUSTOM && !\Model\Content::forge()->is_allow_revert($row3['id'])) : ?>
																			<button class="button tiny warning content-reset" data-id="<?= $row3['id'] ?>" data-option="<?= $row2['id'] ?>" title="Click to Revert" >Revert</button>
																		<?php endif; ?>
																<?php endif; ?>
																<?php endif; ?>
															</div>
															<div class="small-1 cell text-right padded-right">
																<?php if ($row3['pos'] > 1) : ?>
																	<a href="#" data-id="<?= $row3['id'] ?>" data-direction="up.json" data-option="<?= $row2['id'] ?>" data-pos="<?= $row3['pos'] ?>" class="move up"><span class="fi-arrow-up"></span></a>
																<?php endif; ?>
																<a href="#" data-id="<?= $row3['id'] ?>" data-direction="down.json" data-option="<?= $row2['id'] ?>" data-pos="<?= $row3['pos'] ?>" class="move down"><span class="fi-arrow-down"></span></a>
															</div>
														</div>
													<?php endif; ?>
												<?php endforeach; ?>
											</div>

											<!--approved product content-->
											<?php if ($row2['type_id'] == \Model\Option::T_PRODUCT) : ?>
												<div class="grid-x-content">
													<div class="grid-x"><small><strong>Total record : <?= (isset($approved[$row2['id']])) ? count($approved[$row2['id']]) : '0' ?></strong></small></div>
													<div class="grid-x <?= $alt(); ?>">
														<div class="small-4 cell"><small><strong>Manufacturer</strong></small></div>
														<div class="small-3 cell"><small><strong>Part Num</strong></small></div>
														<div class="small-5 cell"><small><strong>Description</strong></small></div>
													</div>
													<?php if (isset($approved[$row2['id']])) : ?>
														<?php $i = 1; ?>
														<?php foreach ($approved[$row2['id']] as $product) : ?>
															<div class="grid-x <?= $alt(); ?>">
																<div class="small-4 cell"><small><?= $product['manufacturer'] ?></small></div>
																<div class="small-3 cell"><small><?= $product['part_num'] ?></small></div>
																<div class="small-5 cell"><small><?= $product['description'] ?></small></div>
															</div>
															<?php $i++; ?>
														<?php endforeach; ?>
													<?php else : ?>
														<div class="grid-x <?= $alt(); ?> text-center"><small>No product records found</small></div>
													<?php endif; ?>
												</div>
												
											<?php endif; ?>
											<!--end of approved product content-->

											<!-- msds list content -->
											<?php if ($row2['type_id'] == \Model\Option::T_MSDS) : ?>
												<ul id="accordion-msds" class="accordion" data-accordion="accordionMsds">
														<li class="accordion-item accordion-msds-item" data-accordion-item>
																<div class="grid-x <?= $alt(); ?>">
																	<div class="small-1 cell"></div>
																	<div class="small-4 cell">Part #</div>
																	<div class="small-5 cell">Description</div>
																</div>
															</li>													
													<?php foreach ($msdss as $msds) : ?>
													<li class="accordion-item accordion-msds-item" data-accordion-item>
														<div class="grid-x <?= $alt(); ?>">
															<div class="small-1 cell"><?php echo \Form::checkbox('data[msds]', $msds['id'], ($msds['status_id'] == \Model\Base::S_UNUSED ? false : true), array('data-id' => $msds['id'], 'class' => 'msds-item', 'data-option' => $row2['id']));?></div>
															<div class="small-4 cell"><?= $msds['part_num'] ?></div>
															<div class="small-5 cell"><?= $msds['description'] ?></div>
														</div>
													</li>
													<?php endforeach; ?>
												</ul>
											<?php endif; ?>
											<!-- end of msds content -->

											<!-- support list content -->
											<?php if ($row2['type_id'] == \Model\Option::T_SUPPORT) : ?>
												<ul id="accordion-support" class="accordion" data-accordion="accordionSupport">
													<?php foreach ($all_supports as $support) : ?>
													<li class="accordion-item accordion-support-item" data-accordion-item>
														<div class="grid-x <?= $alt(); ?>">
															<div class="small-1 cell"><?php echo \Form::checkbox('data[support]', $support['id'], \Model\Support::forge()->checked($support['id'], $row2['id']), array('class' => 'support-item', 'data-option' => $row2['id'], 'data-id' => $support['id']));?></div>
															<div class="small-11 cell"><?= $support['name'] ?></div>
														</div>
													</li>
													<?php endforeach; ?>
												</ul>
											<?php endif; ?>
											<!-- end support list content -->

										<?php endif; ?>
									</div>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
<div class="reveal tiny" id="modal_modules" data-reveal data-options="closeOnClick:false;"></div>
<div class="reveal large" id="content_dialog" data-reveal data-options="closeOnClick:false;"></div>

<script type="text/javascript">
	$(function(){
		var hash = window.location.hash.replace('#', '');
		if (hash != '') {
			// Cache targeted panel
			$target = $('[data-href="' + hash + '"]');
			// Make sure panel is not already active
			if (!$target.parents('li').hasClass('is-active')) {
				// Trigger a click on item to change panel
				$target.trigger('click');
			}
			if (!$target.closest('.accordion-step').hasClass('is-active')) {
				// Trigger a click on item to change panel
				$target.closest('.accordion-step').find('a.accordion-title-step').trigger('click');
			}

			if (!$target.closest('.accordion-option').hasClass('is-active')) {
				// Trigger a click on item to change panel
				$target.closest('.accordion-option').find('a:first').trigger('click');
			}
		}
	});

	$('.grid-x-content .grid-x:last-child').find('a.down').remove();
	$('#accordion-options .accordion-option:last-child .option-move.fi-arrow-down').remove();

	$('.accordion-content').on('click', '.step-reset', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/steps/revert.json', {id: $(this).attr('data-id')}, function(data){
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.option-reset', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/options/revert.json', {id: $(this).attr('data-id')}, function(data){
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.content-reset', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/contents/revert.json', {id: $(this).attr('data-id'), option_id: $(this).attr('data-option')}, function(data){
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.step-enable', function(){
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/steps/enable.json', {id: $(this).attr('data-id')}, function(data){
			btn.removeClass('step-enable').addClass('step-disable');
			btn.removeClass('alert').addClass('success');
			btn.attr('title', 'Click to Disable');
			btn.html('Enabled');
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.step-disable', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/steps/disable.json', {id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(data){
			btn.removeClass('step-disable').addClass('step-enable');
			btn.removeClass('success').addClass('alert');
			btn.attr('title', 'Click to Enable');
			btn.html('Disabled');
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.edit-page').on('click', function(){
		$.get(baseUrl + 'admin/steps/edit.html', {id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('.edit-option').on('click', function(){
		$.get(baseUrl + 'admin/options/edit.html', {id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('.accordion-content').on('click', '.option-enable', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/options/enable.json', {id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(data){
			btn.removeClass('option-enable').addClass('option-disable');
			btn.removeClass('alert').addClass('success');
			btn.attr('title', 'Click to Disable');
			btn.html('Enabled');
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.option-disable', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/options/disable.json', {id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(data){
			btn.removeClass('option-disable').addClass('option-enable');
			btn.removeClass('success').addClass('alert');
			btn.attr('title', 'Click to Enable');
			btn.html('Disabled');
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.edit-content').on('click', function(){
		$.get(baseUrl + 'admin/contents/edit.html', {id: $(this).attr('data-id'), user_id: $('#userid').val(), option_id: $(this).attr('data-option')}, function(html){
			$('#content_dialog').html(html).foundation('open');
		});
	});

	$('.accordion-content').on('click', '.content-enable', function(){
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/contents/enable.json', {id: $(this).attr('data-id')}, function(data){
			btn.removeClass('content-enable').addClass('content-disable');
			btn.removeClass('alert').addClass('success');
			btn.attr('title', 'Click to Disable');
			btn.html('Enabled');
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.content-disable', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var btn = $(this);
		btn.html('Loading');
		$.post(baseUrl + 'admin/contents/disable.json', {id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(data){
			btn.removeClass('content-disable').addClass('content-enable');
			btn.removeClass('success').addClass('alert');
			btn.attr('title', 'Click to Enable');
			btn.html('Disabled');
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '#add_option', function(){
		$.get(baseUrl + 'admin/options/add.html', {step_id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('.accordion-content').on('click', '#add_page', function(){
		$.get(baseUrl + 'admin/contents/edit.html', {option_id: $(this).attr('data-id'), user_id: $('#userid').val()}, function(html){
			$('#content_dialog').html(html).foundation('open');
		});
	});

	$('.accordion-content').on('click', '.move', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var data = $(this).data();
		data.user_id = $('#userid').val();
		$.post(baseUrl + 'admin/contents/' + $(this).data('direction'), data, function(data){
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('change', '.approved-product', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var checkbox = $(this);
		checkbox.prop('disabled', true);
		var data = $(this).data();
		data.checked = $(this).is(':checked') ? 1 : 0;
		data.userid = $('#userid').val();
		$.post(baseUrl + 'admin/product/approved/toggle.json',$(this).data(), function(data){
			checkbox.prop('disabled', false);
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '.group-enabled', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		//alert('disable action');
		console.log('disable action');
	});

	$('.accordion-content').on('click', '.group-disabled', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		//alert('enable action');
		console.log('enable action');
	});

	$('.accordion-content').on('click', '.group-edit', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$.get(baseUrl + 'admin/product/approved/edit.html', {id: $(this).data('id')}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('#modal_modules').on('click', '#save_group', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var form = $(this).closest('form');
		form.find('small.error').remove();
	    form.find('input').removeClass('validation-error');
	   	form.find('label').removeClass('error');
		var data = form.serializeArray();
		data.push({name: 'data[user_id]', value: $('#userid').val()});
		$.post(baseUrl + 'admin/product/approved/edit.json', $.param(data), function(data){
			if (data.status === 'OK') {
				window.location.hash = data.hash;
				window.location.reload();
			} else {
				$.each(data.msg, function(k,v){
			        $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
			        $('label[for="'+k+'"]').addClass('error');
			    });
			}
		});
	});

	$('.accordion-content').on('click', '.product-add', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$.get(baseUrl + 'admin/product/approved/add.html', {group: $(this).data('group')}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('#modal_modules').on('click', '#save_product', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var form = $(this).closest('form');
		form.find('small.error').remove();
	    form.find('input').removeClass('validation-error');
	    form.find('label').removeClass('error');
	    var data = form.serializeArray();
	    data.push({name: 'data[user_id]', value: $('#userid').val()});
		$.post(baseUrl + 'admin/product/approved/add.json', $.param(data), function(data){
			if (data.status === 'OK') {
				window.location.hash = data.hash;
				window.location.reload();
			} else {
				$.each(data.msg, function(k,v){
			        $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
			        $('label[for="'+k+'"]').addClass('error');
			    });
			}
		});
	});

	$('.accordion-content').on('change', '.msds-item', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var chckbox = $(this);
		chckbox.prop('disabled', true);
		var data = {};
		data.checked = $(this).is(':checked') ? 1 : 0;
		data.id = $(this).data('id');
		data.option = $(this).data('option');
		$.post( baseUrl + 'admin/msds/toggle.json', data, function(data){
			chckbox.prop('disabled', false);
			window.location.hash = data.hash;
			window.location.reload();
		} );
	});

	$('.accordion-content').on('change', '.support-item', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var chckbox = $(this);
		chckbox.prop('disabled', true);
		var data = {};
		data.checked = $(this).is(':checked') ? 1 : 0;
		data.id = $(this).data('id');
		data.option = $(this).data('option');
		$.post(baseUrl + 'admin/support/toggle.json', data, function(data){
			chckbox.prop('disabled', false);
			window.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.accordion-content').on('click', '#add_msds, .edit-msds', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$.get(baseUrl + 'admin/msds/update.html', {id: $(this).data('id'), 'option': $(this).data('option')}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('.accordion-content').on('click', '#add_support, .edit-support', function(e){

		e.preventDefault();
		e.stopImmediatePropagation();
		$.get(baseUrl + 'admin/support/update.html', {id: $(this).data('id'), 'option': $(this).data('option')}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	$('#modal_modules').on('click', '#save_support', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var form = $(this).closest('form');
		form.find('small.error').remove();
	    form.find('input').removeClass('validation-error');
	   	form.find('label').removeClass('error');
		$.ajax({
	   		url : baseUrl + 'admin/support/update.json',
	   		type: 'POST',
	   		data: new FormData(form[0]),
	   		cache: false,
	   		contentType: false,
	   		processData: false,
	   		dataType: 'json',
	   		success: function(data){
	   			if (data.status === 'OK') {
					window.location.hash = data.hash;
					window.location.reload();
				} else {
					$.each(data.msg, function(k,v){
				        $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
				        $('label[for="'+k+'"]').addClass('error');
				    });
				}
	   		}
	   	});
	});

	$('#modal_modules').on('click', '#save_msds', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var form = $(this).closest('form');
		form.find('small.error').remove();
	    form.find('input').removeClass('validation-error');
	   	form.find('label').removeClass('error');
	   	$.post(baseUrl + 'admin/msds/update.json', form.serialize(), function(data){
	   		if (data.status === 'OK') {
				window.location.hash = data.hash;
				window.location.reload();
			} else {
				$.each(data.msg, function(k,v){
			        $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
			        $('label[for="'+k+'"]').addClass('error');
			    });
			}
	   	});
	});

	$('.option-move').on('click', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var data = $(this).data();
		data.user_id = $('#userid').val();
		$.post(baseUrl + 'admin/options/' + $(this).data('direction'), data, function(data){
			document.location.hash = data.hash;
			window.location.reload();
		});
	});

	$('.icons').on('click', function(){
		$.get(baseUrl + 'admin/steps/icons.html', {id: $(this).data('id')}, function(html){
			$('#modal_modules').html(html).foundation('open');
		});
	});

	

	$('.menu.cell').remove();
    //# sourceMappingURL=pages.php
    
    $('#view_book').on('click', function(e){
    	var _window = '';
    	e.preventDefault();
    	var data = {
    		userid: $('#userid').val(),
    		template_id: $('#template_id').val()
    	};
    	if (_window == '') {
    		_window = window.open(baseUrl + 'admin/users/view_book?' + $.param(data), '_blank');
    	} else {
    		var currUrl = _window.location.href;
    		_window.location.href=currUrl;
    	}
    });

    $('#print').on('click', function(e){
    	e.preventDefault();
    	window.location.href = $(this).attr('href');
    });
</script>