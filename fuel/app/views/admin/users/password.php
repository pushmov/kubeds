<div class="modal-header">
    <h5>Reset Password</h5>
    <button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
    <?= \Form::open(array('id' => 'password_form', 'name' => 'password_form', 'method' => 'post', 'action' => '')) ?>
    <div class="grid-x">
        <div class="small-12 cell">
            <p>New Password</p>
            <input type="text" name="password" id="password" value="" />
        </div>
    </div>
    <div class="grid-x cell" id="password_btn">
        <div class="float-left">
            <button type="button" class="button small success" id="reset_password" name="delete">Reset</button><button type="button" class="button small" id="btn_close" name="btn_close" data-close>Cancel</button>
        </div>
    </div>
    <?= \Form::close(); ?>
</div>
<script>
    $('#reset_password').on('click', function () {
        $('.callout').remove();
        $.post(baseUrl + 'admin/users/password.json', {id: $('#userid').val(), passwrd: $('#password').val()}, function (json) {
            if (json.status == 'OK') {
                $('#password_btn').before('<div class="callout success">Password Reset</div>');
            } else {
                $('#password_btn').before('<div class="callout warning">' + json.msg + '</div>');
            }
        })
    })
</script>