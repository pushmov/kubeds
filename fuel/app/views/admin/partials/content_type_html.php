<div class="small-2 cell">
	<label for="content">Content <span class="astrict">*</span></label>
</div>
<div class="small-10 cell">
	<textarea id="content_edit" name="data[content]"><?=$data['content']?></textarea>
</div>
<script type="text/javascript">
    tinymce.remove();
    tinymce.init({
        convert_urls: false,
        relative_urls: false,
        remove_script_host: false,
        selector: '#content_edit',
        height: 500,
        menubar: false,
        theme: 'modern',
        plugins: 'preview autolink visualblocks visualchars fullscreen image link media template charmap hr nonbreaking anchor advlist lists imagetools contextmenu colorpicker code',
        toolbar1: 'template | formatselect fontsizeselect | bold italic strikethrough | link charmap media image | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code fullscreen',
        image_advtab: true,
        templates: [
            { title: 'Two Column', content: '<div class="grid-x"><div class="small-6 cell">Col 1</div><div class="small-6 cell">Col 2</div></div>' },
            { title: 'Three Column', content: '<div class="grid-x"><div class="small-4 cell">col 1</div><div class="small-4 cell">col 2</div><div class="small-4 cell">col 3</div></div>' }
        ],
        content_css: [
            '/assets/css/app.css',
            '/assets/css/style.css',
            '/assets/foundation-icons/foundation-icons.css',
            '/assets/css/styles2.css'
        ],
        file_browser_callback: function(field, url, type, win) {
            tinyMCE.activeEditor.windowManager.open({
                file: '/kcfinder/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                title: 'KCFinder',
                width: 700,
                height: 500,
                inline: true,
                close_previous: false
            }, {
                window: win,
                input: field
            });
            return false;
        },
        video_template_callback: function(data){
            var path = data.source1.replace(/^.+\.\//,'');
            if (/[^.]+$/.exec(data.source1)[0] === 'pdf') {
                return ' <object width="' + data.width + '" height="' + data.height + '" data="' + path + '"  type="application/pdf"></object>';
            } else {
                return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + path + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
            }
        }
    });
    tinymce.execCommand('mceFocus',false,'content_edit');
</script>