<div class="small-3 cell">
	<label for="content">Content <span class="astrict">*</span></label>
</div>
<div class="small-9 cell">
	<input type="file" name="data[content]" id="content" value="" >
	<?php if ( $data['content'] != '' ) : ?>
	<?=\Html::anchor('#', 'View image', array('id' => 'view_image', 'data-id' => $data['id']));?>
	<?php endif; ?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		//ajax upload plugin
		$('#view_image').on('click', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			$('#modal_modules').foundation('close');
			$.get(baseUrl + 'admin/process/image.html', {id: $(this).attr('data-id')}, function(html){
				$('#modal_modules').html(html).foundation('open');
			});
		});
	});
</script>