<div class="grid-x padding-top-0">
	<h2 class="small-10 cell">Suppliers</h2>
	<p class="small-2 cell button" id="add_supplier">Add Supplier</p>
</div>
<table id="datasupplier" class="show clickable cell-border grid-x-border dataTable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Site URL</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<div class="reveal tiny" id="dialog" data-reveal data-options="closeOnClick:false;"></div>
<script>
	$(document).ready(function() {
		var dt = $('#datasupplier').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: baseUrl + '/admin/suppliers/all.json'
			},
			"aoColumnDefs": [
				{ 'bSortable': false, 'aTargets': [ 3 ] }
			],
		});
		$('#add_supplier').on('click', function() {
			$.get(baseUrl + '/admin/suppliers/update.html', {'id': 0}, function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});
		$('#datasupplier').on('click', 'button', function() {
			var action = $(this).data('action');
			$.get(baseUrl + '/admin/suppliers/' + action + '.html', {'id': $(this).data('id')}, function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});
		$('#dialog').on('click', '#btn_save', function(e) {
			var action = $(this).prop('id');
			$.post(baseUrl + '/admin/suppliers/update.json', $('#supplier_form').serialize(), function(json) {
				$('#dialog .callout').remove();
				if (json.status == 'OK') {
					$('#btn').before('<div class="callout success">Saved</div>');
					dt.ajax.reload();
				} else {
					$('#btn').before('<div class="callout alert">' + json.msg + '</div>');
				}
			});
		});
	});
</script>