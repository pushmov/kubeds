<div class="modal-header">
	<h5>Confirm Delete</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'supplier_form', 'name' => 'supplier_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
	<div class="callout alert">
		<h4>Are you sure want to delete supplier <?=$data['name'];?>?</h4>
		</div>
		<button type="button" class="button alert" id="delete_supplier" name="delete">Delete</button>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
	<?=\Form::close();?>
</div>