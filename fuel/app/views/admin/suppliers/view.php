<div class="modal-header">
	<h5>View Supplier</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="name">Name</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[name]" id="name" value="<?=$data['name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="site_url">Site URL</label>
		</div>
		<div class="small-9 cell error">
			<input type="text" class="" name="data[site_url]" id="site_url" value="<?=$data['site_url'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="status_id">Status</label>
		</div>
		<div class="small-9 cell">
			<?=\Form::select('data[status_id]', $data['status_id'], \Model\Supplier::forge()->get_status_array(), array('id' => 'status_id', 'disabled' => 'disabled'))?>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_at">Created At</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_at]" id="created_at" value="<?=date_create($data['created_at'])->format('M d, Y');?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_by">Created By</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_by]" id="created_by" value="<?=$data['created_first_name'].' '.$data['created_last_name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_at">Updated At</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_at]" id="updated_at" value="<?=date_create($data['updated_at'])->format('M d, Y');?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_by">Updated By</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_by]" id="updated_by" value="<?=$data['updated_first_name'].' '.$data['updated_last_name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x cell" id="btn">
		<div class="float-left">
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</div>
</div>