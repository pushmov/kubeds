<div class="modal-header">
	<h5><?=$title?></h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'supplier_form', 'name' => 'supplier_form', 'method' => 'post', 'action' => ''));?>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="name">Name <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" class="" name="data[name]" id="name" value="<?=$data['name'];?>"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="site_url">Site URL</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[site_url]" id="site_url" value="<?=$data['site_url'];?>"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="status_id">Status <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell">
			<?=\Form::select('data[status_id]', $data['status_id'], \Model\Supplier::forge()->get_status_array(), array('id' => 'status_id'));?>
		</div>
	</div>
	<div class="grid-x cell" id="btn">
		<input type="hidden" name="data[id]" id="id" value="<?=$data['id'];?>"/>
		<button type="button" class="button success" id="btn_save" name="save">Save</button>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
	</div>
	<?=\Form::close();?>
</div>