<div class="grid-x">
	<div class="small-12 medium-4 cell">
		<?=$profile;?>
	</div>
	<div class="small-12 medium-8 cell">
		<?=$pages;?>
	</div>
</div>
<script>
    $(document).on('load', function() {
        $('#loading_div').foundation('open');
    });
    $(window).on('beforeunload', function() {
        $('#loading_div').foundation('open');
    });
    $(document).on('ready', function(){
        $('#loading_div').foundation('close');
    });
    $('.back').on('click', function(e){
        window.location.href = '/admin/users';
    });
</script>
