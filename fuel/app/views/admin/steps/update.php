<div class="modal-header">
	<h5><?=$title?></h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'heading_form', 'name' => 'heading_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id'], 'data[user_id]' => $user_id))?>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="title">Title <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[title]" id="title" value="<?=$data['title'];?>"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_at">Created At</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_at]" id="created_at" value="<?=date_create($data['created_at'])->format(VIEW_DT);?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_by">Created By</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_by]" id="created_by" value="<?=$data['created_name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_at">Updated At</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_at]" id="updated_at" value="<?=date_create($data['updated_at'])->format(VIEW_DT);?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_by">Updated By</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_by]" id="updated_by" value="<?=$data['updated_name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x cell" id="btn">
		<div class="float-left">
			<button type="button" class="button success" id="save_step" name="save">Save</button><button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
		</div>
	</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$('#save_step').on('click', function(){
		var form = $(this).closest('form');
		form.find('small.error').remove();
		form.find('input').removeClass('validation-error');
		form.find('label').removeClass('error');
		var data = form.serializeArray();
		data.push({name: 'data[user_id]', value: $('#userid').val()});
		$.post(baseUrl + 'admin/steps/edit.json', $.param(data), function(data){
			if (data.status === 'OK') {
				window.location.hash = data.hash;
				window.location.reload();
			} else {
				$.each(data.msg, function(k,v){
					$('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
					$('label[for="'+k+'"]').addClass('error');
				});
			}
		});
	});
</script>