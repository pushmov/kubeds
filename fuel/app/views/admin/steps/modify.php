
<div class="large-12 cell heading-notif">
	<div data-alert class="alert-box success radius hide"></div>
</div>
<div class="large-12 cell">
	<button type="button" id="btn_add_heading" class="button small">Add Item</button>
</div>
<div class="large-6 cell">
	<div class="panel callout panel-active">
		<h5>Active Heading</h5>
  		<small>Drag and drop to re-order. Move item into 'Unused Heading' area to remove</small>
  		<ul id="sortable_active" class="list-unstyled sortable-connect sortable-active">
  			<?php foreach($active as $row) : ?>
  			<li data-key="<?=$row->id?>" data-pos="<?=$row->pos?>">
  				<h6><strong><?=$row->title?></strong></h6>
  				<small><?=\Model\Heading::forge()->get_status_array($row->status_id);?></small>
  				<?php if ($row->status_id == \Model\Heading::S_CUSTOM) : ?>
  				<a href="#" data-id="<?=$row->id?>" class="button small warning float-right heading-revert">Revert</a>
  				<?php endif; ?>
  				<a href="#" data-id="<?=$row->id?>" class="button small float-right heading-edit">Edit</a>
  				<?php if ( !empty($row->child) ) : ?>
  					<?php foreach ($row->child as $child) : ?>
	  				<div class="child">
	  					<h6><strong><?=$child->title?></strong></h6>
		  				<small><?=\Model\Heading::forge()->get_status_array($child->status_id);?></small>
		  				<a href="#" data-id="<?=$child->id?>" class="button small alert float-right heading-delete">Deactivate</a>
		  				<!--a href="#" data-id="<?=$row->id?>" class="button small float-right heading-edit">Edit</a-->
	  				</div>
	  				<?php endforeach; ?>
  				<?php endif; ?>
  			</li>
  			<?php endforeach; ?>
  		</ul>
  		<button class="button small" type="button" id="save_pos" disabled="disabled">Save</button>
  		<button class="button small warning" type="button" id="cancel_order">Reset Order</button>
	</div>
</div>
<div class="large-6 cell">
	<div class="panel panel-unused">
		<h5>Unused Heading</h5>
		<small>Drag and drop item into 'Active Heading' area to activate it.</small>
		<ul id="sortable_unused" class="list-unstyled sortable-connect sortable-unused">
			<?php foreach($unused as $row) : ?>
  			<li data-key="<?=$row->id?>" data-pos="<?=$row->pos?>" class="<?=(\Model\Heading::forge()->parent_level($row->id) > 2) ? 'disable-drop' : ''?>">
  				<?php if ($row->parent_id > 0 && (\Model\Heading::forge()->parent_level($row->id) > 2)) : ?>
  				<h6><strong><?=\Model\Heading::forge()->find_top_parent($row->id)->title.' - '.$row->title?></strong></h6>
  				<?php else : ?>
  				<h6><strong><?=$row->title?></strong></h6>
  				<?php endif; ?>
  				<small><?=\Model\Heading::forge()->get_status_array($row->status_id);?></small>
  				<!--a href="#" data-id="<?=$row->id?>" class="button small alert float-right heading-delete">Delete</a-->
  				<a href="#" data-id="<?=$row->id?>" class="button small float-right heading-activate">Activate</a>
  			</li>
  			<?php endforeach; ?>
  		</ul>
	</div>
</div>
<script type="text/javascript">
	var items;
	$( function() {
	    $('#sortable_active').sortable({
	    	connectWith: '.sortable-connect',
	    	placeholder: 'heading-placeholder',
	    	start: function(e, ui) {
	    		$('#save_pos').prop('disabled', true);
	    	},
	    	update: function(e, ui) {
	    		do_order();
	    		$('#save_pos').prop('disabled', false);
	    	},
	    	receive: function(e, ui) {
	    		if ($(ui.item).hasClass('disable-drop')) {
	    			alert('this system template cannot be dragged. Use activate instead');
	    			$(ui.sender).sortable('cancel');
	    		} else {
	    			activate_item($(ui.item.get(0)).attr('data-key'));
	    		}

	    		//do_order();
	    		//save_items_to_server();
	    		$('#save_pos').prop('disabled', true);
	    	},
	    	stop: function(e, ui) {
	    		$('#save_pos').prop('disabled', false);
	    	}
	    });

	    $('#save_pos').on('click', function(e){
	    	e.preventDefault();
	    	e.stopImmediatePropagation();
	    	save_items_to_server();
	    });

	    $('.heading-revert').on('click', function(e){
	    	e.preventDefault();
	    	e.stopImmediatePropagation();
	    	$.get(baseUrl + 'admin/heading/revert.html', {id: $(this).attr('data-id')}, function(html){
	    		$('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
	    	});
	    });

	    $('#modal_modules').on('click', '#revert_heading', function(e){
	    	e.preventDefault();
	    	e.stopImmediatePropagation();
	    	var form = $(this).closest('form');
	    	$.post(baseUrl + 'admin/heading/revert.json', form.serialize(), function(json){
	    		if (json.status === 'OK') {
	    			$('.alert-box').html(json.msg + '<a href="#" class="close">&times;</a>').removeClass('hide').show();
	    		}
				$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
			    	$('#content').find('#heading_content').html(html);
			    	$('#modal_modules').removeClass('small').addClass('large').html(html).foundation('close');
			    });

	    	});
	    });

	    function save_items_to_server() {
	    	$.post(baseUrl + 'admin/heading/modify.json', {data: items}, function(json){
	    		$('#save_pos').prop('disabled', true);
	    		if (json.status === 'OK') {
	    			$('.alert-box').html(json.msg + '<a href="#" class="close">&times;</a>').removeClass('hide').show();
	    		}
	    		$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
			    	$('#content').find('#heading_content').html(html);
			    });
	    	});
	    }

	    function do_order() {
	    	items = {};
	      	$('#sortable_active li').each(function(e){
	      		var x = $(this).attr('data-key');
	      		items[x] = ($(this).index() + 1);
	      	});
	      	items.user_id = $('#company').val();
	    }

	    function activate_item(id) {
	    	$('#save_pos').prop('disabled', true);
	    	$.post(baseUrl + 'admin/heading/activate.json', {id: id, user_id: $('#company').val()}, function(json){
	    		if (json.status === 'OK') {
	    			$('.alert-box').html(json.msg + '<a href="#" class="close">&times;</a>').removeClass('hide').show();
	    		}
				$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
			    	$('#content').find('#heading_content').html(html);
			    });
	    	});
	    }

	    function deactivate_item(id) {
	    	$('#save_pos').prop('disabled', true);
	    	$.post(baseUrl + 'admin/heading/deactivate.json', {id: id, user_id: $('#company').val()}, function(json){
	    		if (json.status === 'OK') {
	    			$('.alert-box').html(json.msg + '<a href="#" class="close">&times;</a>').removeClass('hide').show();
	    		}
				$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
			    	$('#content').find('#heading_content').html(html);
			    });
	    	});
	    }

	    $('#sortable_unused').sortable({
	    	connectWith: '.sortable-connect',
	    	start: function(){
	    		$('#save_pos').prop('disabled', true);
	    	},
	    	receive: function(e, ui) {
	    		deactivate_item($(ui.item.get(0)).attr('data-key'));
	    		//do_order();
	    		//save_items_to_server();
	    		$('#save_pos').prop('disabled', true);
	    	},
	    	stop: function(e, ui) {
	    		$('#save_pos').prop('disabled', false);
	    	}
	    });

	    $('#cancel_order').on('click', function(){
	    	$('#sortable_active').sortable( 'cancel' );
	    	$('#save_pos').prop('disabled', true);
	    });

	    $('.heading-activate').on('click', function(e){
	    	e.preventDefault();
	    	e.stopImmediatePropagation();
	    	$.get(baseUrl + 'admin/heading/activate.html', {id: $(this).attr('data-id')}, function(html){
	    		$('#modal_modules').removeClass('large').addClass('small').html(html).foundation('open');
	    	});
	    });

	    $('#modal_modules').on('click', '#activate_headings', function(e){
	    	e.preventDefault();
	    	e.stopImmediatePropagation();
	    	var form = $(this).closest('form');
	    	activate_item(form.find('[name="data[id]"]').val());
	    	$('#modal_modules').removeClass('small').addClass('large').foundation('close');
	    });

	    $('#modal_modules').one('click', '#save_headings', function(e){
	    	e.preventDefault();
	    	e.stopImmediatePropagation();
	    	var btn = $(this);
	    	btn.prop('disabled', true);
	    	var form = $(this).closest('form');
	    	form.find('small.error').remove();
	    	form.find('input').removeClass('validation-error');
	    	form.find('label').removeClass('error');
	    	var data = form.serializeArray();
	    	data.push({name: 'data[user_id]', value: $('#company').val()});
	    	$.post(baseUrl + 'admin/heading/save.json', $.param(data), function(data){
	    		btn.prop('disabled', false);
	        	if (data.status === 'FAIL') {
	        		$.each(data.msg, function(k,v){
	        			$('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
	            		$('label[for="'+k+'"]').addClass('error');
	          		});
	        	} else {
	          		$('#modal_modules').foundation('close');
	          		$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
			    		$('#content').find('#heading_content').html(html);
			    		do_order();
			    		save_items_to_server();
			  		});
	        	}
	      	});
	    });

	    $('#modal_modules').on('click', '#delete_headings', function(e){
		    e.preventDefault();
		    e.stopImmediatePropagation();
		    var form = $(this).closest('form');
		    var data = form.serializeArray();
		    data.push({name: 'user_id', value: $('#company').val()});
		    data.push({name: 'id', value: $('#modal_modules [name="data[id]"]').val()});
		    $.post(baseUrl + 'admin/heading/deactivate.json', $.param(data), function(data){
			    if (data.status === 'OK') {
			        $('#modal_modules').removeClass('small').addClass('large').foundation('close');
	          		$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
			    		$('#content').find('#heading_content').html(html);
			  		});
			    }
		    });
    	});
	} );

	$(document).ready(function(){
		var a = $('#sortable_active');
		var b = $('#sortable_unused');
		if ( a.height() > b.height() ) {
			b.height(a.height());
		}

		if ( b.height() > a.height() ) {
			a.height(b.height());
		}

		
	});
</script>