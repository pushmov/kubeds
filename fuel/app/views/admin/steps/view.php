<div class="modal-header">
	<h5>View Heading</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="user_id">Company</label>
		</div>
		<div class="small-9 cell">
			<?=\Form::select('data[user_id]', $data['user_id'], \Model\User::forge()->companies(), array('id' => 'user_id', 'disabled' => 'disabled'));?>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="route">URL Route</label>
		</div>
		<div class="small-9 cell error">
			<input type="text" class="" name="data[route]" id="route" value="<?=$data['route'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="title">Title</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[title]" id="title" value="<?=$data['title'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="pos">Position</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[pos]" id="pos" value="<?=$data['pos'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_at">Created At</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_at]" id="created_at" value="<?=date_create($data['created_at'])->format('M d, Y');?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="created_by">Created By</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[created_by]" id="created_by" value="<?=$data['created_name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_at">Updated At</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_at]" id="updated_at" value="<?=date_create($data['updated_at'])->format('M d, Y');?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="updated_by">Updated By</label>
		</div>
		<div class="small-9 cell">
			<input type="text" class="" name="data[updated_by]" id="updated_by" value="<?=$data['updated_name'];?>" disabled="disabled"/>
		</div>
	</div>
	<div class="grid-x cell" id="btn">
		<div class="float-left">
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Close</button>
		</div>
	</div>
</div>