<div class="small-12 cell">
	<h5>Select User:</h5>
</div>
<div class="grid-x cell">
	<div class="large-10 cell"><?=\Form::select('data[company]', null, \Model\User::forge()->companies(), array('id' => 'company', 'class' => 'select2'));?></div>
	<div class="large-2 cell">
		<button class="button btn-show" type="button" id="refresh">Refresh</button>
	</div>
</div>

<div class="grid-x cell heading-content" id="heading_content"></div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
		function load_page(id){
			$.get(baseUrl + 'admin/heading/modify.html', {id: $('#company').val()}, function(html){
		    	$('#content').find('#heading_content').html(html);
		    });
		}
		$('#content').on('change', '#company', function(){
	    	load_page();
	    });
		load_page();
		$('#refresh').on('click', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			load_page();
		});
	});
</script>