<div class="modal-header">
	<h5>Confirm Activation</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'heading_form', 'name' => 'heading_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
		<div class="grid-x">
			<div class="small-12 cell">
				<p>Are you sure want to activate this item?</p>
			</div>
		</div>
		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button alert" id="activate_headings" name="delete">Yes</button>
				<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>