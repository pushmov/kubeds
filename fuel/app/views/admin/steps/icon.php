<div class="modal-header">
	<h5>Update Icon</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<div class="text-center">
		<div class="bpaddding">
			<?php if ($data['icon'] != '') : ?>
				<?=\Asset::img($data['icon'], array('id' => 'icon'));?>
			<?php endif; ?>
		</div>
		<div class="buttons">
			<button class="button success" id="change_icon" data-id="<?=$data['id']?>">Change Icon</button><button class="button" data-close type="button">Close</button>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#change_icon').on('click', function(){
		var btn = $(this);
		window.KCFinder = {};
		window.KCFinder.callBack = function(url) {
			var filename = url.replace(/.*(\/|\\)/, '');
			$.post(baseUrl + 'admin/steps/icons.json', {id: btn.attr('data-id'), icons: filename, userid: $('#userid').val()}, function(json){
				if (json.status === 'OK') {
					$('.buttons').prepend('<div class="callout success"> '+json.msg+' </div>');
					$('#icon').attr('src', baseUrl + '/' + url)
				}
			});
			window.KCFinder = null;
		};
		window.open('/kcfinder/browse.php', 'kcfinder_single', 'location=yes,height=570,width=800,scrollbars=yes,status=yes');
	});
</script>