<div class="modal-header">
	<h5><?=$title?></h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'content_form', 'name' => 'content_form', 'method' => 'get', 'action' => '', 'target' => '_blank'), array('data[id]' => $data['id'], 'data[option_id]' => $data['option_id'], 'data[content_type]' => \Model\Content::TYPE_HTML, 'data[opt]' => $data['opt']))?>
		<div class="grid-x">
			<div class="small-2 cell">
				<label for="title">Title <span class="astrict">*</span></label>
			</div>
			<div class="small-10 cell">
				<input type="text" class="" name="data[title]" id="title" value="<?=$data['title'];?>"/>
			</div>
		</div>

		<div class="grid-x" id="content_type_field"><?=$data['content_type_field'];?></div>

		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button success" id="save_content" name="save">Save</button><?php if ($data['id'] != '') :?><button type="button" class="button warning" id="preview" data-option="<?=$data['option_id']?>" data-content="<?=$data['id'];?>">Preview</button><?php endif; ?><button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$('#save_content').on('click', function(){
		var form = $(this).closest('form');
		var data = form.serializeArray();
		data.push({name: 'data[content]', value: tinyMCE.activeEditor.getContent()});
	    data.push({name: 'data[user_id]', value: $('#userid').val()});
		$.post(baseUrl + 'admin/contents/edit.json', $.param(data), function(data){
			if (data.status === 'OK') {
				window.location.hash = data.hash;
				window.location.reload();
			} else {
				$.each(data.msg, function(k,v){
		            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
		            $('label[for="'+k+'"]').addClass('error');
		        });
			}
		});
	});
	var _window = '';
	
	$('#preview').on('click', function(e){
		var btn = $(this);
		var form = $(this).closest('form');
		var formdata = form.serializeArray();
		//var _window = '';
		formdata.push({name: 'data[content]', value: tinyMCE.activeEditor.getContent()});
	    formdata.push({name: 'data[user_id]', value: $('#userid').val()});
		$.post(baseUrl + 'admin/contents/edit.json', $.param(formdata), function(data){
			if (data.status === 'OK') {
				var data = {
					user_id: $('#userid').val(),
					option_id: btn.attr('data-option'),
					content_id: data.id
				};
				if (_window == '') {
					_window = window.open(baseUrl + 'admin/contents/preview?' + $.param(data), '_blank');
				} else {
					var currUrl = _window.location.href;
					_window.location.href=currUrl;
				}

			} else {
				$.each(data.msg, function(k,v){
		            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
		            $('label[for="'+k+'"]').addClass('error');
		        });
			}
		});
		
	});
</script>