<div class="modal-header">
	<h5><?=$title?></h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'support_form', 'name' => 'support_form', 'method' => 'post', 'action' => '', 'enctype' => 'multipart/form-data'), array('data[id]' => $data['id'], 'data[option]' => $data['option'], 'data[user_id]' => $data['user_id'], 'data[template_id]' => $data['template_id']))?>
		<div class="grid-x">
			<div class="small-3 cell">
				<label for="support_name">Support Name <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<input type="text" class="" name="support_name" id="support_name" value="<?=$data['support_name'];?>"/>
				<input type="hidden" name="data[sup_id]" id="sup_id" value="<?=$data['sup_id']?>">
			</div>
		</div>

		<div class="grid-x">
			<div class="small-3 cell">
				<label for="logo">Logo <span class="astrict">*</span></label>
			</div>
			<div class="small-9 cell error">
				<?php if ($data['logo_label'] != '') : ?>
				<div class="img">
				<?=\Asset::img(\Model\Support::TECH_SUPPORT_PATH . $data['logo_label']);?>
				</div>
				<?php endif; ?>
				<input type="file" class="" name="support_file" id="support_file" value="" />
			</div>
		</div>

		<div class="grid-x cell" id="btn">
			<div class="float-left">
				<button type="button" class="button" id="save_support" name="save">Save</button>
				<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
			</div>
		</div>
	<?=\Form::close();?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#support_name').easyAutocomplete({
			url: baseUrl + 'admin/supplier/autocomplete.json',
			getValue: function(element){
				return element.name;
			},
			list: {
				match: {enabled: true},
				onClickEvent: function(){
					var supid = $('#support_name').getSelectedItemData().id;
					var supname = $('#support_name').getSelectedItemData().name;
					$('#sup_id').val(supid);
					$('#support_name').val(supname);
				},
				onKeyEnterEvent: function(){
					var supid = $('#support_name').getSelectedItemData().id;
					var supname = $('#support_name').getSelectedItemData().name;
					$('#sup_id').val(supid);
					$('#support_name').val(supname);
				}
			}
		});
	});
</script>