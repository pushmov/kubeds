<div class="modal-header">
	<h5>Add Template</h5>
	<button class="close-button" data-close type="button"> <span>&times;</span></button>
</div>
<div class="modal-body">
	<?=\Form::open(array('id' => 'company_form', 'name' => 'company_form', 'method' => 'post', 'action' => ''), array('data[id]' => $data['id']))?>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="name">Name <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell error">
			<input type="text" class="" name="data[name]" id="name" value="<?=$data['name'];?>"/>
		</div>
	</div>
	<div class="grid-x">
		<div class="small-3 cell">
			<label for="clone">Clone Template <span class="astrict">*</span></label>
		</div>
		<div class="small-9 cell">
			<?=\Form::select('data[clone]', '', \Model\Template::forge()->dropdown(true, 'Select One'), array('id' => 'clone'));?>
		</div>
	</div>
	<div class="grid-x cell" id="btn">
		<button type="button" class="button success" id="btn_save" name="save">Save</button>
		<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
	</div>
	<?=\Form::close();?>
</div>