<div class="grid-x padding-top-0">
	<h2 class="small-10 cell">Templates</h2>
	<p class="small-2 cell button" id="add_user">Add Template</p>
</div>
	<table id="datalist" class="show clickable">
		<thead>
			<tr>
				<th>Name</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($users as $user): ?>
				<tr id="id_<?=$user['id'];?>">
					<td><?=$user['name'];?></td>
					<td><?=\Model\Template::forge()->status_array($user['status_id']);?></td>
					<td><?=\Html::anchor('admin/templates/load/' . $user['id'], 'Edit', array('class' => 'user-profile button small', 'id' => $user['id'], 'title' => "Click to edit {$user['name']}"));?> &nbsp; <?=\Html::anchor('#', 'Delete', array('class' => 'user-delete button small alert', 'id' => $user['id'], 'title' => "Click to delete {$user['name']}"));?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<div class="reveal tiny" id="dialog" data-reveal data-options="closeOnClick:false;"></div>
<script>
	$(document).ready(function() {
		$('.user-delete').on('click', function(e){
			e.preventDefault();
			$.get(baseUrl + 'admin/templates/delete.html', {id: $(this).attr('id')}, function(html){
				$('#dialog').html(html).foundation('open');
			});
		});

		$('#dialog').on('click', '#delete_user', function(){
			var form = $(this).closest('form');
			$.post(baseUrl + 'admin/users/delete.json', form.serialize(), function(data){
				if (data.status === 'OK') {
					window.location.reload();
				}
			});
		});
		$('#datalist').DataTable({
			iDisplayLength: 50,
			responsive: true
		});
		$('#add_user').on('click', function() {
			$.get(baseUrl + 'admin/templates/update.html', {id: 0}, function(html){
				$('#dialog').html(html).foundation('open');
			});
		});
		$('#dialog').on('click', '#update', function(){
			var form = $(this).closest('form');
			form.find('small.error').remove();
	    	form.find('input').removeClass('validation-error');
	    	form.find('label').removeClass('error');
			$.post(baseUrl + 'admin/templates/save.json', form.serialize(), function(data){
				if (data.status === 'OK') {
					window.location.reload();
				} else {
					$.each(data.msg, function(k,v){
			            $('#'+k).addClass('validation-error').after('<small class="error">' + v + '</small>');
			            $('label[for="'+k+'"]').addClass('error');
			        });
				}
			});
		});
		$('.user-profile').on('click', function() {
            $('#loading_div').foundation('open');
			/*
            $.get(baseUrl + 'admin/templates/update.html', {id: $(this).attr('id')}, function(html){
				$('#dialog').html(html).foundation('open');
			});
			*/
        });
        $('#dialog').on('click', '#btn_save', function(e) {
			var action = $(this).prop('id');
			$.post(baseUrl + '/admin/templates/update.json', $('#company_form').serialize(), function(json) {
				$('#dialog .callout').remove();
				if (json.status == 'OK') {
					$('#btn').before('<div class="callout success">Saved</div>');
					window.location.reload();
				} else {
					$('#btn').before('<div class="callout alert">' + json.msg + '</div>');
				}
			});
		});

		$('#dialog').on('click', '#delete_company', function(e) {
			var action = $(this).prop('id');
			$.post(baseUrl + '/admin/templates/delete.json', $('#company_form').serialize(), function(json) {
				$('#dialog .callout').remove();
				if (json.status == 'OK') {
					$('#btn').before('<div class="callout success">Deleted</div>');
					window.location.reload();
				} else {
					$('#btn').before('<div class="callout alert">' + json.msg + '</div>');
				}
			});
		});
/*
		$('.user-profile').on('click', function(e){
			e.preventDefault();
			var url = $(this).attr('href');
			$('#dialog').html('').foundation('open');
			$.get(baseUrl + 'admin/templates/edit.html', {id: $(this).attr('id')}, function(html){
				$('#dialog').html(html).foundation('open');
				setTimeout(function(){
					window.location.href = url;
				}, 1000);
			});
		});
		*/
	});
</script>
