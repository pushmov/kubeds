<div class="grid-x grid-padding-x align-center">
	<div class="cell medium-8 large-6 log-in-form">
		<form id="form" method="POST">
			<h5 class="text-center">Log in with your email or account ID</h5>
			<label>Account #
				<input name="account" type="text" placeholder="K00000">
			</label>
			<label>Email
				<input name="email" type="text" placeholder="somebody@example.com">
			</label>
			<label>Password
				<input name="password" type="password" placeholder="Password">
			</label>
			<input name="remember" id="remember" type="checkbox"><label for="remember">Remember Me</label>
			<p id="subp"><a id="submit" type="submit" class="button expanded">Log In</a></p>
			<p class="text-center"><?=\Html::anchor('login/forgot', 'Forgot your password?');?></p>
		</form>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#submit').on('click', function() {
			$('.callout').remove();
			$.post(baseUrl + 'login/submit.json', $('#form').serialize(), function(json) {
				if (json.status == 'OK') {
					window.location = baseUrl + 'home';
				} else {
					$('#subp').before('<div class="callout alert">' + json.msg + '</div>');
				}
			}).fail(function(response) {
				alert('Error: ' + response.responseText);
			});
		});
	});
</script>